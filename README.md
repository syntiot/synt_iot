# SyntIoT

Public release of the platform source code, anonymized for the review process

### Structure of the main table of the dataset collected with the platform (networkflow_ts)

* row ID
* device ID
* device port number
* remote host IP
* remote host port number
* protocol
* number of bytes received
* number of bytes sent
* initiator of the connection (packet sender)
* timestamp of the first packet in the current 5-second measurement window
* interceptor ID
* remote host ID
* is it a DNS request packet (flag)
* timestamp of the packet interception
* is it a packet belonging to a state (flag)
* is it a packet belonging to a transition (flag)
* state ID
* transition ID
* timestamp of the last record update
* is the FSM info set (flag)

### Deployment instructions

Below we present an exemplary protocol that could be followed to deploy the SyntIoT platform for a research project leveraging all of platform components.

## Required hardware

List of hardware required to deploy both the Synthetic IoT devices and the synthetic IoT backend.

1. A (preferably) single-board computer capable of running a GNU/Linux distribution with a Python interpreter. The number of available network interfaces (or devices) will correspond to the number of separate Synthetic devices that can be emulated.
2. A computer with Python 2 interpreter, running in the same network as the Synthetic IoT devices to host the Interceptor module.
3. A router enabling the internet connection for both the emulated IoT devices and the commercial ones. For the latter, it should offer wired connection capabilities required by certain IoT hubs, as in the case of Philips Hue Bridge.
4. A computer with Python 3 interpreter and a public IP address, so that it can be used to host the Collector module of the platform.
5. A computer with Python 3 interpreter and a public IP address to server as the Synthetic IoT vendor backend. This could be the same machine as the Collector module. However, since the Synthetic vendor backend is intended to support not only the vendor's own server's / API but also support-party / CDN as well as third-party endpoints, each of these require another machine with a public IP address and ideally also different associated domain (as that is the case for the commercial deployments).
6. Commercial, off-the-shelf IoT devices, as per the needs of a particular research project.

## Software setup

We have divided the procedure of software setup per platform modules.

### Synthetic IoT device

We use the example of Raspberry Pi as a hardware platform hosting the Synthetic device.
1. Update the software running on the device.
2. Install required software dependencies.
3. Create a virtual Python environment for the project.
4. Clone the source code repository of Synthetic IoT device module.
5. Use the declarative specification file to customize the desired behavior of the Synthetic user and IoT device.
6. Run the module.

### Interceptor module

The setup of this module boils down to the following steps:
1. Create a virtual Python environment for the project.
2. Clone the source code repository of Interceptor module.
3. Run the module.
4. Control the status of traffic interception (5-second intervals of the uploads of traffic traces of commercial and Synthetic IoT devices are reported; similarly, the process of intercepting records of the internal state of the Synthetic IoT devices are included in the status information available to the user).

### Collector module

1. Install the required, operating-system wide software dependencies.
2. Create a virtual Python environment for the project.
3. Install the required software dependencies.
4. Clone the source code repository of the Collector module.
5. Run the module.
6. Once the Interceptor module is run, information about the devices emitting network traffic inside Interceptor's network will arrive into the database of the Collector module. Select the devices which are intended to be monitored by the Interceptor module and enable interception of their network traces by switching the respective flag in the Interceptor user's record.
7. Reload the module for the changes to take effect.
8. Create a periodic task for the collected records about the network traffic to be inserted into the Collector's database in an offline manner (in order not to overload the host machine - these operations might be run in time periods of decreased demand for computing resources, as well as in order to assure real-time response of the Collector's module to the Interceptor module, which is needed for controlling the integrity if the data collection process).

### Synthetic backend

1. Install the required, operating-system wide software dependencies.
2. Create a virtual Python environment for the project.
3. Install the required software dependencies.
4. Clone the source code repository of the Backend module.
5. Customize parameters of the module if desired.
6. Run the module.
