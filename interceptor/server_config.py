import sys


if '--test' in sys.argv:
    BASE_URL = 'YOUR_URL'
else:
    BASE_URL = 'YOUR_URL'

NEW_USER_URL = BASE_URL + '/generate_user_key'

SUBMIT_URL = BASE_URL + '/submit_data/{user_key}'

UTC_OFFSET_URL = BASE_URL + '/submit_utc_offset/{user_key}/{offset_seconds}'

CHECK_CONSENT_URL = BASE_URL + '/has_signed_consent_form/{user_key}'

INIT_URL = BASE_URL + '/setup?started_from_app=yes'

GT_UPLOAD_URL = BASE_URL + '/behavior_simulation/submit_gtdata/{user_key}/{device_id}'
