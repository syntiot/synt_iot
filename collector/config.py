"""Flask config class."""
import os

class Config:
    """Set Flask configuration vars."""
    # General config
    # Basic config variables
    SECRET_KEY = os.environ.get('FLASK_SECRET_KEY')
    SESSION_COOKIE_NAME = os.environ.get('FLASK_SESSION_COOKIE_NAME')
    CELERY_BROKER = 'amqp://guest:guest@localhost:5672//'

class ProdConfig(Config):
    FLASK_DEBUG = False
    TESTING = False
    # Database
    SQLALCHEMY_DATABASE_URI = os.environ.get('FLASK_SQLALCHEMY_PROD_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get("FLASK_SQLALCHEMY_PROD_TRACK_MODIFICATIONS")    

class DevConfig(Config):
    FLASK_DEBUG = True
    TESTING = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('FLASK_SQLALCHEMY_DEV_DATABASE_URI')
    SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get("FLASK_SQLALCHEMY_DEV_TRACK_MODIFICATIONS")
