
DNS_DICT_NAME = 'dns_dict'
DNS_DICT_TSD_NAME = 'dns_dict_timestamped'
FLOW_DICT_NAME = 'flow_dict'
FLOW_DICT_TSD_NAME = 'flow_dict_timestamped'
DEVICE_DICT_NAME = 'device_dict'
DEVICE_DICT_TSD_NAME = 'device_dict_timestamped'
USER_AGENT_DICT_NAME = 'ua_dict'
USER_AGENT_DICT_TSD_NAME = 'ua_dict_timestamped'
DHCP_DICT_NAME = 'dhcp_dict'
DHCP_DICT_TSD_NAME = 'dhcp_dict_timestamped'
DHCP_RESOLVER_DICT_NAME = 'dhcp_resolver_dict'
DHCP_RESOLVER_DICT_TSD_NAME = 'dhcp_resolver_dict_timestamped'
NETDISCO_DICT_NAME = 'netdisco_dict'
NETDISCO_DICT_TSD_NAME = 'netdisco_dict_timestamped'
TLS_DICT_LIST = 'tls_dict_list'
TLS_DICT_LIST_TSD = 'tls_dict_list_timestamped'


# Based on the findings Ren et al. / IMC'19 "Information exposure from consumer IoT devices: A multidimensional, network-informed measurement approach" paper, table 4:
# (measurements done in the US and UK only)
TOP_10_COMPANIES_CONTACTED_IMC19 = ['Amazon', 'Google', 'Akamai', 'Microsoft', 'Netflix', 'Kingsoft', '21Vianet', 'Alibaba', 'Beijing Huaxiay', 'AT&T']
VENDORS_OF_IMC19_TESTED_IOT_DEVICES = ['Amazon', 'Amcrest', 'Xiaomi', 'Insteon', 'Philips', 'Lightify', 'TP-Link', 'Google', 'Samsung']

# Make sure to map Ring (video doorbells), Blink (security cameras) to Amazon
# Nest to Google
# Wink to i.am.+ (owned by will.i.am)
# smartthings.com to Samsung
# TP-Link - Chineese company
# We-mo - part of Belkin (same as Linksys)

VENDOR_IDENTIFYING_KEYWORDS = {} # e.g. product names, names of subsidiaries and acquired companies
VENDOR_IDENTIFYING_KEYWORDS['Amazon'] = ['Amazon', 'Ring', 'Blink']
VENDOR_IDENTIFYING_KEYWORDS['Google'] = ['Google', 'Nest']
VENDOR_IDENTIFYING_KEYWORDS['Samsung'] = ['Samsung', 'Smartthings']
VENDOR_IDENTIFYING_KEYWORDS['Belkin'] = ['Belkin', 'We-mo', 'Linksys']
VENDOR_IDENTIFYING_KEYWORDS['Philips'] = ['Philips', 'Hue']
VENDOR_IDENTIFYING_KEYWORDS['Lightify'] = ['Lightify', 'Osram']
VENDOR_IDENTIFYING_KEYWORDS['TP-Link'] = ['TP-Link']
