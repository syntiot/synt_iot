#!/usr/bin/python

import sys
import logging

logging.basicConfig(stream=sys.stderr)

sys.path.insert(0, '~/cloned_repository_dir_path')

from iot_study import create_app
application = create_app()
