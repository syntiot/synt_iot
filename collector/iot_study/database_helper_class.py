import mysql.connector
from mysql.connector import Error
from mysql.connector import errorcode

import os
import json
import sys
import six

class DatabaseHelper:
    """
       Establish a connection to the DB and offer helper methods
       in an Object-Oriented Programming fashion
    """
    db_host = 'localhost'
    db_user = ''
    db_password = ''
    db_name = ''
    db_credentials_file_name = 'db_credentials.json'
    SUCCESSFUL_INSERT_RETURN_VALUE = "Successfully completed the insert operation"
    FAILED_INSERT_RETURN_VALUE = "Failed the insert operation"
    SUCCESSFUL_UPDATE_RETURN_VALUE = "Successfully completed the pdate query"
    FAILED_UPDATE_RETURN_VALUE = "Failed the update operation"

    def __init__(self):
        DatabaseHelper.load_db_credentials(self)
        self.connection = mysql.connector.connect(host=self.db_host,
                                                  database=self.db_name,
                                                  user=self.db_user,
                                                  password=self.db_password)
        self.cursor = self.connection.cursor()
        self.insert_cursor = self.connection.cursor(dictionary=True)

    def insert(self, query, paramTuple):
        try:
            self.insert_cursor.execute(query, paramTuple)
            self.connection.commit()
            return self.insert_cursor.lastrowid
        except:
            print(sys.exc_info())
            self.connection.rollback()
            return self.FAILED_INSERT_RETURN_VALUE

    def insert_many(self, query, paramArrayOfTuples):
        try:
            self.insert_cursor.executemany(query, paramArrayOfTuples)
            self.connection.commit()
            return self.insert_cursor.rowcount
        except mysql.connector.Error as error:
            print(sys.exc_info())
            print("Failed to insert multiple records into MySQL table with this error: {}".format(error))
            self.connection.rollback()
            return self.FAILED_INSERT_RETURN_VALUE

    def query(self, query, paramTuple):
        self.cursor.execute(query, paramTuple)
        return self.cursor.fetchall()

    def simple_update(self, query):
        try:
            self.cursor.execute(query)
            self.conection.commit()
            return self.cursor.lastrowid
        except:
            print(sys.exc_info())
            self.connection.rollback()
            return self.FAILED_UPDATE_RETURN_VALUE

    def update(self, query, paramsTuple):
        try:
            self.insert_cursor.execute(query, paramsTuple)
            self.connection.commit()
            return self.cursor.lastrowid
        except:
            print(sys.exc_info())
            self.connection.rollback()
            return self.FAILED_UPDATE_RETURN_VALUE

    def update_many(self, query, paramsListOfTuples):
        try:
            self.cursor.executemany(query, paramsListOfTuples)
            self.connection.commit()
            return self.cursor.rowcount

        except:
            print(sys.exc_info())
            print("[database_helper_class: update_many] SWITCHING TO PER-RECORD UPDATE due to an exception in the BATCH! This might take some more time...")
            numberOfRecordsThatHaveNotBeenUpdated = 0
            numberOfUpdatedRecords = 0
            totalNumberOfRecordsToBeUpdated = len(paramsListOfTuples)
            for paramTuple in paramsListOfTuples:
                try:
                    self.cursor.execute(query, paramTuple)
                    numberOfUpdatedRecords += 1
                except:
                    print("[database_helper_class: update_many] Error for query: " + str(query) + " and paramTuple: " + str(paramTuple) + ", rolling back and skipping update of this record! However, we move on to the next one")
                    print(sys.exc_info())
                    numberOfRecordsThatHaveNotBeenUpdated += 1
            self.connection.commit()
            radioOfUpdatedRecords = round( (numberOfUpdatedRecords/totalNumberOfRecordsToBeUpdated) * 100, 2 )
            ratioOfErroneousRecords = round( (numberOfRecordsThatHaveNotBeenUpdated/totalNumberOfRecordsToBeUpdated) * 100, 2 )
            print("[database_helper_class: update_many] Successfully updated " + str(radioOfUpdatedRecords) + " % of records (" + str(numberOfUpdatedRecords) + "/" + str(totalNumberOfRecordsToBeUpdated) + ")")
            print("[database_helper_class: update_many] Ratio of records with errors: " + str(ratioOfErroneousRecords) + "% (" + str(numberOfRecordsThatHaveNotBeenUpdated) + "/" + str(totalNumberOfRecordsToBeUpdated) + ")")
            return self.cursor.rowcount

    def __del__(self):
        self.connection.close()

    def load_db_credentials(self):
        thisSourceFilesFullPath = os.path.dirname(os.path.abspath(__file__))
        osSeparatorCharacter = os.sep
        fullPathElements = thisSourceFilesFullPath.split(osSeparatorCharacter)
        fullPathElements.remove('')
        twoLevelsUpFilePath = os.sep.join(fullPathElements[:-2])
        twoLevelsUpFilePath = os.sep + twoLevelsUpFilePath
        credentialsJSONFilePath = os.path.join(twoLevelsUpFilePath, 'inspector-backend-db-credentials', self.db_credentials_file_name)
        with open(credentialsJSONFilePath) as cfHandle:
            try:
                credentialsJSON = json.load(cfHandle)
            except Exception as e:
                print("Exception while reading the credentials file: " + str(e) + "\nReturning!")
                return
            self.db_host = credentialsJSON['host']
            self.db_user = credentialsJSON['user_name']
            self.db_password = credentialsJSON['password']
            self.db_name = credentialsJSON['database_name']
