from flask import current_app as app
from flask import request, make_response

import logging
from datetime import datetime
import os
import json
import pytz
import hashlib

logger = logging.getLogger(__name__)

@app.route('/behavior_simulation/submit_gtdata/<user_key>/<device_id>', methods = ['POST'])
def receive_ground_truth_data_from_synth_app(user_key, device_id):
    listOfFSMstatusDictsAsString = request.get_json()
    listOfFSMstatusDicts = []
    numberOfFSMstatusRecordsReceived = -1
    numberOfFSMstatusRecordsWithCorrectChecksum = 0
    try:
        listOfFSMstatusDicts = json.loads(listOfFSMstatusDictsAsString)
        numberOfFSMstatusRecordsReceived = len(listOfFSMstatusDicts)
    except:
        logger.error("Exception while parsing listOfFSMstatusDictsAsString (" + str(listOfFSMstatusDictsAsString) + "):\n" + str(e))
        return make_response("Exception while parsing 'listOfFSMstatusDictsAsString', sorry!", 400)        
    listOfCorrectlyReceivedFSMstatusDicts = []
    for statusDictString in listOfFSMstatusDicts:
        statusDict = statusDictString
        receivedHashValue = statusDict.pop("augmentedFSMstatusSHA512")
        tempSTRING = ""
        LIST_OF_SORTED_DICT_ITEMS = sorted(statusDict.items())
        for key, value in LIST_OF_SORTED_DICT_ITEMS:
            tempSTRING += str(key) + str(value)
        receivedDictFrozenString = tempSTRING.encode('utf-8')
        computedHashValue = hashlib.sha512(receivedDictFrozenString).hexdigest()
        if receivedHashValue != computedHashValue:
            logger.error("\n\nDATA INTEGRITY ERROR for dict " + str(statusDict) + "\nSkipping it!")
        else:
            listOfCorrectlyReceivedFSMstatusDicts.append(statusDict)
            numberOfFSMstatusRecordsWithCorrectChecksum += 1
    logger.error("Received FSM status records: correct checksum in " + str(numberOfFSMstatusRecordsWithCorrectChecksum) + " of " + str(numberOfFSMstatusRecordsReceived))
    the_timezone = pytz.timezone('Europe/Amsterdam')
    ts_dumping_JSON_to_disk = datetime.now(the_timezone).strftime('%Y-%m-%d_%H-%M-%S.%f')
    diskDumpJSONfileName = 'inspIntercGroundTruth_' + ts_dumping_JSON_to_disk + '_file.json'
    diskDumpJSONfileDir = 'inspAugmentedGroundTruthData'
    diskDumpJSONfilePath = os.path.join(app.root_path, diskDumpJSONfileDir, diskDumpJSONfileName)
    with open(diskDumpJSONfilePath, 'w') as jsonFileHandle:
        try:
            json.dump(listOfFSMstatusDicts, jsonFileHandle)
        except Exception as e:
            logger.error("Exception while saving JSON to disk for offline processing:\n" + str(e))
            return make_response("Exception while saving JSON to disk for offline processing", 400)
    logger.error("Successful reception of Ground truth data done.")
    responseExpectedByClient = {"gt_reception_status": "received_and_saved"}
    return make_response(responseExpectedByClient, 200)

@app.route('/behavior_simulation/submit_gtdata', methods= ['POST', 'GET'])
def stub_for_unaugmented_ground_truth_request():
    logger.error("Got the unaugmented GT request, just returning OK")
    responseExpectedByClient = {"gt_reception_status": "STUB_REQUEST_received_and_saved"}
    return make_response(responseExpectedByClient, 200)
