from flask import current_app as app

from .models import *
import cons_defs
import logging

from flask import request, make_response

import uuid
import hashlib
import os
import pytz

logger = logging.getLogger(__name__)

from . import redis_queue
from .flask_bg_worker_rq import redis_queue_connection
from rq.job import Job

TIMEZONE_NAME_STRING = 'Europe/Amsterdam' # Random, anonymized for review

@app.route('/submit_data/<user_key>', methods = ['POST'])
def submit_data(user_key):
    from datetime import datetime
    from flask import jsonify
    from flask import request, make_response
    import json
    import time
    
    if request.method != 'POST':
        logger.error('request method is not POST :(')
        return make_response('Malformed request', 400)
    if request.method == 'POST':
        currentUserDBrecord = StudyParticipant.query.filter_by(user_key=user_key).first()
        if currentUserDBrecord == None:
            logger.error('userInQuestion == None :(')
            return make_response('Unknown user!', 400)
 
        submitDataProcessingStartTime = time.time()
        dataDict = request.get_json()
        dataDict['user_key'] = user_key
        the_timezone = pytz.timezone(TIMEZONE_NAME_STRING)
        ts_dumping_JSON_to_disk = datetime.now(the_timezone).strftime('%Y-%m-%d_%H-%M-%S.%f')
        diskDumpJSONfileName = 'inspDumpFrom_' + ts_dumping_JSON_to_disk + '_file.json'
        diskDumpJSONfileDir = 'inspClientsCollectedData'
        diskDumpJSONfilePath = os.path.join(app.root_path, diskDumpJSONfileDir, diskDumpJSONfileName)
        with open(diskDumpJSONfilePath, 'w') as jsonFileHandle:
            try:
                json.dump(dataDict, jsonFileHandle)
            except Exception as e:
                logger.error("Exception while saving JSON to disk for offline processing:\n" + str(e))
                return make_response("Exception while saving JSON to disk for offline processing", 400)
        dnsDictTimestamped = json.loads(dataDict['dns_dict_timestamped'])
        flowDictTimestamped = json.loads(dataDict['flow_dict_timestamped'])
        deviceDictTimestamped = json.loads(dataDict['device_dict_timestamped'])
        uaDictTimestamped = json.loads(dataDict['ua_dict_timestamped'])
        dhcpDictTimestamped = json.loads(dataDict['dhcp_dict_timestamped'])
        dhcpResolverDictTimestamped = json.loads(dataDict['dhcp_resolver_dict_timestamped'])
        tlsDictListTimestamped = json.loads(dataDict['tls_dict_list_timestamped'])
        netdiscoDictTimestamped = json.loads(dataDict['netdisco_dict_timestamped'])
        
        roundingPrecision = 3
        logDictSizesTSonly = 'dnsTS: ' + str(len(dnsDictTimestamped)) + ' | flowTS: ' + str(len(flowDictTimestamped)) + ' | devTS: ' + str(len(deviceDictTimestamped)) + ' | uaTS: ' + str(len(uaDictTimestamped)) + ' | dhcpTS: ' + str(len(dhcpDictTimestamped)) + ' | dhcpResTS: ' + str(len(dhcpResolverDictTimestamped)) + ' | tlsDTS: ' + str(len(tlsDictListTimestamped)) + ' | tlsTS: ' + str(len(tlsDictListTimestamped)) + ' | netdTS: ' + str(len(netdiscoDictTimestamped))
        logger.error('[submit_data] ' + logDictSizesTSonly )
        d1_deviceID = '"randomDeviceID"'
        d2_deviceID = '"randomDeviceID"'
        d3_deviceID = '"randomDeviceID"'
        d4_deviceID = '"randomDeviceID"'
        d5_deviceID = '"randomDeviceID"'
        response_dict_here = '{"status" : "success", "inspected_devices" : [' + d1_deviceID + ', ' + d2_deviceID + ', ' +  d3_deviceID + ', ' + d4_deviceID + ", " + d5_deviceID + ']}'
        return make_response(response_dict_here, 200)


@app.route("/generate_user_key", methods = ['GET'])
def generate_user_key():
    """ Generating user's key in the set timezone """
    the_timezone = pytz.timezone(TIMEZONE_NAME_STRING)
    from datetime import datetime
    if request.method != 'GET':
        return make_response('Malformed request', 400)
    user_key = uuid.uuid4()
    key_creation_ts = datetime.now(the_timezone)
    user_key = str(user_key).replace('-', '')
    new_participant = StudyParticipant(user_key=str(user_key), key_creation_timestamp=key_creation_ts, signed_consent_form=False)
    db.session.add(new_participant)
    db.session.commit()
    response = make_response(user_key, 200)
    response.headers["Content-Type"] = 'application/json'
    return response

@app.route('/submit_utc_offset/<user_key>/<offset_seconds>', methods = ['GET'])
def submit_utc_offset(user_key, offset_seconds):
    from flask import make_response
    offset_seconds_as_int = int(offset_seconds)
    userInQuestion = StudyParticipant.query.filter_by(user_key=user_key).first()
    if userInQuestion != None:
        userInQuestion.utc_offset_seconds = offset_seconds_as_int
        db.session.commit()
        response = make_response("SUCCESS", 200)
    else:
        logger.error("[WARNING!] the USER_KEY_DOESNT_EXIST. This means that you should delete the '~/princeton-iot-inspector' directory given you're trying to start using the inspector as a NEW USER !")
        response = make_response("USER_KEY_DOESNT_EXIST", 400)
    return response

@app.route('/has_signed_consent_form/<user_key>', methods = ['GET'])
def signed_consent_form(user_key):
    userInQuestion = StudyParticipant.query.filter_by(user_key=user_key).first()
    if userInQuestion != None:
        hasSignedConsentForm = userInQuestion.signed_consent_form
        if hasSignedConsentForm == 0:
            response = make_response("NO_CONSENT_RECORDED", 200)
        else:
            response = make_response("CONSENT_GIVEN", 200)
    else:
        response = make_response("Unknown user!", 400)
        
    return response
