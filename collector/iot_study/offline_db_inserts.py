import os
import logging
import json
import time
from datetime import datetime
import traceback
import sys
import pytz
import pathlib

from database_helper_class import *

db = DatabaseHelper()

logger = logging.getLogger(__name__)
hdlr = logging.FileHandler('execution_logs/offline_DB_inserts.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.WARNING)

NAME_OF_NETWORK_FLOW_FILES_DIR = 'inspClientsCollectedData'
NAME_OF_FSM_STATUS_FILES_DIR = 'inspAugmentedGroundTruthData'
NAME_OF_SORTED_FSM_STATUS_RECORDS_DIR = 'inspSortedFSMrecords'
NAME_OF_NO_USER_KEY_RECORDS_DIR = "noUserKeyInDBrecords"

inspectorsPacketInfoJSONdirectory = os.fsencode(NAME_OF_NETWORK_FLOW_FILES_DIR)
FSM_status_data_dir = os.fsencode(NAME_OF_FSM_STATUS_FILES_DIR)
FSM_sorted_records_dir = os.fsencode(NAME_OF_SORTED_FSM_STATUS_RECORDS_DIR)
LIST_OF_CURRENT_NETWORK_FLOW_JSON_FILES = None
LIST_OF_CURRENT_FSM_INFO_JSON_FILES = None

ROW_ID_FIELD_IDX = 0
USER_KEY_FIELD_IDX = 1 # 0-based index for a tuple containing DB query results
DEVICE_ID_FIELD_IDX = 3 # Based on models.py, the index/order no of the column
DNS_DOMAIN_FIELD_IDX = 1
DEVICE_IP_FIELD_IDX = 4
DEVICE_OUI_FIELD_IDX = 5

DEFAULT_SELECT_QUERY_COMPARISON_OPERATOR = "="

INSERT_QRY_NETFLOWS_PARTICIPANTID_IDX_IN_TUPLE = 0
INSERT_QRY_NETFLOWS_DEVICEID_IDX_IN_TUPLE = 1
INSERT_QRY_NETFLOWS_FLOWTS_IDX_IN_TUPLE = 12

NETWORK_FLOWS_TSD_TABLE_NAME = 'networkflow_ts'

TIMEZONE_NAME_STRING = 'Europe/Amsterdam' # Random, anonymized for the review

''' Methods providing simple way of interaction with the DB while using the database_helper_class '''

#########################################################################################################################
####                                                                                                                 ####
#### ---- Wrapper functions for the DatabaseHelper class responsible for direct communication with the MySQL DB ---- ####
####                                                                                                                 ####
#########################################################################################################################

def execute_select_query(fieldsToSelectAsString, tableNameString, listOfFieldsToCheckWithWhereClause, parametersTuple, useUserDefinedComparisonOperatorsList=False, userDefinedComparisonOperatorsList=[]):
    '''
        If the user decides to 'useUserDefinedComparisonOperatorsList',
        a comparison operator for EACH AND EVERY requested comparison operation
        MUST be defined in 'userDefinedComparisonOperatorsList'.
    '''
    selectQueryString = ''
    selectQueryString += "SELECT" + " "
    selectQueryString += fieldsToSelectAsString + " "
    selectQueryString += "FROM" + " "
    selectQueryString += tableNameString
    if parametersTuple:
        numberOfTupleElements = len(parametersTuple)
        numberOfWhereArgs = len(listOfFieldsToCheckWithWhereClause)
        if numberOfTupleElements != numberOfWhereArgs:
            errorMsg = ''
            traceback.print_stack() # Will go to stderr, i.e. console running the script
            errorMsg += "[execute_select_query] \nERROR! Number of names of fields to be checked with WHERE statement (" + str(numberOfWhereArgs) + ") is different than the number of values passed as parameters in the tuple (" + str(numberOfTupleElements) + ")!"
            errorMsg += "\nparametersTuple: " + str(parametersTuple) + " | listOfFieldsToCheckWithWhereClause: " + str(listOfFieldsToCheckWithWhereClause) + " Returning None..."
            logger.error(errorMsg)
            return None
        if useUserDefinedComparisonOperatorsList:
            numberOfUserDefinedComparisonOperators = len(userDefinedComparisonOperatorsList)
            if numberOfUserDefinedComparisonOperators != numberOfTupleElements:
                errorMsg = "[execute_select_query] ERROR! numberOfUserDefinedComparisonOperators != numberOfTupleElements ! Quitting..."
                logger.error(errorMsg)
                return
            if numberOfUserDefinedComparisonOperators != numberOfWhereArgs:
                errorMsg = "[execute_select_query] ERROR! numberOfUserDefinedComparisonOperators != numberOfWhereArgs ! Quitting..."
                logger.error(errorMsg)
                return
        COMPARISON_OPERATORS_LIST = []
        if useUserDefinedComparisonOperatorsList:
            COMPARISON_OPERATORS_LIST = userDefinedComparisonOperatorsList
        else:
            COMPARISON_OPERATORS_LIST = DEFAULT_SELECT_QUERY_COMPARISON_OPERATOR * numberOfWhereArgs
        selectQueryString += " "
        selectQueryString += "WHERE"
        for index, paramValue in enumerate(parametersTuple):
            if index == 0:
                selectQueryString += " " + listOfFieldsToCheckWithWhereClause[index] + " " + COMPARISON_OPERATORS_LIST[index] +  " " + "%s"
            else:
                if listOfFieldsToCheckWithWhereClause[index] == '' and COMPARISON_OPERATORS_LIST[index] == '':
                    selectQueryString += " AND " + "%s"
                else:
                    selectQueryString += " AND " + listOfFieldsToCheckWithWhereClause[index] +  " " + COMPARISON_OPERATORS_LIST[index] +  " "  + "%s"
    queryResultList = db.query(selectQueryString, parametersTuple)
    return queryResultList

def execute_insert_query(tableNameString, listOfFieldNamesToBeInsertedAsASingleString, tupleOfValuesToBeInserted):
    numberOfFieldsToBeInsertedInto = len( listOfFieldNamesToBeInsertedAsASingleString.split(',') )
    numberOfFieldsToBeInsertedIntoAsPerTupleSize = len(tupleOfValuesToBeInserted)
    if numberOfFieldsToBeInsertedInto != numberOfFieldsToBeInsertedIntoAsPerTupleSize:
        logger.error("[execute_insert_query] ERROR: numberOfFieldsToBeInsertedInto (" + str(numberOfFieldsToBeInsertedInto) + ") != numberOfFieldsToBeInsertedIntoAsPerTupleSize (" + str(numberOfFieldsToBeInsertedIntoAsPerTupleSize) + ") ! Returning...")
        return  
    insertQueryString = ''
    insertQueryString += "INSERT INTO" + " "
    insertQueryString += tableNameString
    insertQueryString += " " + "("
    insertQueryString += listOfFieldNamesToBeInsertedAsASingleString + ")"
    insertQueryString += " " + "VALUES"
    insertQueryString += " " + "("
    for index, paramValue in enumerate(tupleOfValuesToBeInserted):
        if index == 0:
            insertQueryString += "%s"
        else:
            insertQueryString += ", %s"
    insertQueryString += ")"
    resultOfInsertOp = db.insert(insertQueryString, tupleOfValuesToBeInserted)
    if resultOfInsertOp == db.FAILED_INSERT_RETURN_VALUE:
        return resultOfInsertOp
    else:
        return resultOfInsertOp

def execute_insert_query_multiple_inserts_at_once( tableNameString, listOfFieldNamesToBeInsertedAsASingleString, arrayOfTuplesToBeBatchInsertedAtOnce ):
    """
        Insert a bunch of rows into the table at once using 'cursor.executemany' and a single 'commit' operations per all of them
        in order to improve the performance of DB operations (time-wise)
    """
    numberOfFieldsToBeInsertedInto = len( listOfFieldNamesToBeInsertedAsASingleString.split(',') )
    dummyListUsedOnlyToSetNumberOfParamsInTheQuerysInsertInto = [0] * numberOfFieldsToBeInsertedInto
    numberOfFieldsToBeInsertedIntoAsPerTupleSize = len(arrayOfTuplesToBeBatchInsertedAtOnce[0])
    if numberOfFieldsToBeInsertedInto != numberOfFieldsToBeInsertedIntoAsPerTupleSize:
        logger.error("[execute_insert_query_multiple_inserts_at_once] ERROR: numberOfFieldsToBeInsertedInto (" + str(numberOfFieldsToBeInsertedInto) + ") != numberOfFieldsToBeInsertedIntoAsPerTupleSize (" + str(numberOfFieldsToBeInsertedIntoAsPerTupleSize) + ") ! Returning...")
        return
    insertQueryString = ''
    insertQueryString += "INSERT INTO" + " "
    insertQueryString += tableNameString
    insertQueryString += " " + "("
    insertQueryString += listOfFieldNamesToBeInsertedAsASingleString + ")"
    insertQueryString += " " + "VALUES"
    insertQueryString += " " + "("
    for index, paramValue in enumerate(dummyListUsedOnlyToSetNumberOfParamsInTheQuerysInsertInto):
        if index == 0:
            insertQueryString += "%s"
        else:
            insertQueryString += ", %s"
    insertQueryString += ")"
    resultOfInsertOp = db.insert_many(insertQueryString, arrayOfTuplesToBeBatchInsertedAtOnce)
    if resultOfInsertOp == db.FAILED_INSERT_RETURN_VALUE:
        return resultOfInsertOp
    else:
        return resultOfInsertOp
    
def execute_update_query(tableNameString, listOfNamesOfFieldsToBeUpdated, listOfNewValuesToBeSet, idOfTheRowToBeUpdated):
    """ This wrapper function updates as many column values as you want in one shot """
    numberOfFieldNamesPassedAsArg = len(listOfNamesOfFieldsToBeUpdated)
    numberOfNewValuesPassedAsArg = len(listOfNewValuesToBeSet)
    if numberOfFieldNamesPassedAsArg != numberOfNewValuesPassedAsArg:
        errorMsg = ''
        errorMsg += "ERROR updating a row in function 'execute_update_query': numberOfFieldNamesPassedAsArg != numberOfNewValuesPassedAsArg"
        errorMsg += "\nSpecifically, numberOfFieldNamesPassedAsArg = " + str(numberOfFieldNamesPassedAsArg) + " and numberOfNewValuesPassedAsArg = " + str(numberOfNewValuesPassedAsArg)
        logger.error(errorMsg)
        return db.FAILED_UPDATE_RETURN_VALUE
    updateQueryString = ''
    updateQueryString += "UPDATE" + " "
    updateQueryString += tableNameString
    updateQueryString += " " + "SET"
    for index, newValue in enumerate(listOfNewValuesToBeSet):
        if index == 0:
            updateQueryString += " " + listOfNamesOfFieldsToBeUpdated[index] + " " + "=" + " " + "%s"
        else:
            updateQueryString += ", " + listOfNamesOfFieldsToBeUpdated[index] + " " + "=" + " " + "%s"

    updateQueryString += " " + "WHERE"
    updateQueryString += " " + "id"
    updateQueryString += " " + "=" + " " + "%s"
    paramsList = listOfNewValuesToBeSet
    paramsList.append(idOfTheRowToBeUpdated)
    paramsTuple = tuple(paramsList)
    return db.update(updateQueryString, paramsTuple)

def execute_update_query_for_multiple_records_at_once(tableNameString, listOfNamesOfFieldsToBeUpdated, listOfListsOfNewValuesToBeSetWithUpdatedRowIDappended):
    """
        This wrapper function updates as many column values as you want in one shot
        for multiple records at once (passed as a list of tuples instead of a single tuple)
    """
    numberOfFieldNamesPassedAsArg = len(listOfNamesOfFieldsToBeUpdated)
    numberOfNewValuesPassedAsArg = len(listOfListsOfNewValuesToBeSetWithUpdatedRowIDappended[0]) - 1
    if numberOfFieldNamesPassedAsArg != numberOfNewValuesPassedAsArg:
        errorMsg = ''
        errorMsg += "ERROR updating a row in function 'execute_update_query': numberOfFieldNamesPassedAsArg != numberOfNewValuesPassedAsArg"
        errorMsg += "\nSpecifically, numberOfFieldNamesPassedAsArg = " + str(numberOfFieldNamesPassedAsArg) + " and numberOfNewValuesPassedAsArg = " + str(numberOfNewValuesPassedAsArg)
        logger.error(errorMsg)
        return db.FAILED_UPDATE_RETURN_VALUE
    updateQueryString = ''
    updateQueryString += "UPDATE" + " "
    updateQueryString += tableNameString
    updateQueryString += " " + "SET"
    for index, newValue in enumerate(listOfNamesOfFieldsToBeUpdated):
        if index == 0:
            updateQueryString += " " + listOfNamesOfFieldsToBeUpdated[index] + " " + "=" + " " + "%s"
        else:
            updateQueryString += ", " + listOfNamesOfFieldsToBeUpdated[index] + " " + "=" + " " + "%s"
    updateQueryString += " " + "WHERE"
    updateQueryString += " " + "id"
    updateQueryString += " " + "=" + " " + "%s"
    return db.update_many(updateQueryString, listOfListsOfNewValuesToBeSetWithUpdatedRowIDappended)

###############################################################################
####                                                                       ####
#### ---- Offline insert methods for NetworkFlows and FSM status data ---- ####
####                                                                       ####
###############################################################################

#### ----
#### ---- Inserting the NetworkFlow timestamped data from JSON files saved to the hard drive by the Flask app of the platform's measurements collector module
#### ----

def insert_network_flow_data():
    logger.error("[insert_network_flow_data] ########## NETWORK FLOWS INSERTING PROCESS STARTED ##########")
    networkFlowInsertingStartTime = time.time()
    listOfFilepathsOfSuccessfulyInsertedJSONfiles = []
    submitDataProcessingStartTime = time.time()
    listOfDirContents = LIST_OF_CURRENT_NETWORK_FLOW_JSON_FILES
    numberOfFilesFoundInTheDir = len(listOfDirContents)
    numberOfJSONfilesWithFSMstatusInfoProcessed = 0
    dirItemIndex = 0
    for filenameStr in listOfDirContents:
        filename = os.fsdecode(filenameStr)
        dirItemIndex += 1
        if filename.endswith(".json") or filename.endswith(".JSON"):
            currentJsonFilePath = os.path.join(inspectorsPacketInfoJSONdirectory, filenameStr)
            JSONdumpContents = None
            try:
                with open(currentJsonFilePath) as jsonFileHandle:
                    dataDict = json.load(jsonFileHandle)
            except Exception as e:
                logger.error("[insert_network_flow_data] Failed with exception: "+ str(e) + "\nReturning...")
                return
            ##############################################################################################
            #### Beginning of the DB insertion CODE
            ##############################################################################################
            logger.error("[insert_network_flow_data] Processing file [" + str(dirItemIndex) + " of " + str(numberOfFilesFoundInTheDir) + "]")
            user_key = dataDict['user_key']
            currentUserDBrecord = execute_select_query(fieldsToSelectAsString="*", tableNameString="studyparticipant", listOfFieldsToCheckWithWhereClause=['user_key'], parametersTuple=(user_key,))
            if currentUserDBrecord is None:
                logger.error("[insert_network_flow_data] There is no such user/participant in the DB -- or -- there's an error in the implementation of the offline DB insert code. Quitting...")
                return
            elif len(currentUserDBrecord) > 1:
                logger.error("[insert_network_flow_data] The 'currentUserDBrecord' returns more than 1 result per the given user_key '" + user_key + "'! Quitting...")
                return
            elif not currentUserDBrecord:
                logger.error("[insert_network_flow_data] WARNING! The 'user_key' = '" + user_key + "' was not found in the database, IGNORING THE FILE then...")
                continue
            currentUserDBrecordID = currentUserDBrecord[0][ROW_ID_FIELD_IDX]

            dnsDictTimestamped = json.loads(dataDict['dns_dict_timestamped'])
            flowDictTimestamped = json.loads(dataDict['flow_dict_timestamped'])
            deviceDictTimestamped = json.loads(dataDict['device_dict_timestamped'])
            uaDictTimestamped = json.loads(dataDict['ua_dict_timestamped'])
            dhcpDictTimestamped = json.loads(dataDict['dhcp_dict_timestamped'])
            dhcpResolverDictTimestamped = json.loads(dataDict['dhcp_resolver_dict_timestamped'])
            tlsDictListTimestamped = json.loads(dataDict['tls_dict_list_timestamped'])
            netdiscoDictTimestamped = json.loads(dataDict['netdisco_dict_timestamped'])

            client_version = str(dataDict['client_version'])
            duration = str(dataDict['duration'])
            client_ts = datetime.strptime(dataDict['client_ts'], '%Y-%m-%d %H:%M:%S.%f')
            timeProcessingDNStsd = 0.0
            timeProcessingFlow = 0.0
            timeProcessingFlowTsd = 0.0
            timeDeviceTsd = 0.0
            timeUserAgentTsd = 0.0
            timeDHCPTsd = 0.0
            timeDHCPresolverIPTsd = 0.0
            timeNetdiscoTsd = 0.0
            timeTlsDictTsd = 0.0

            timeQueryingFlowTable = 0.0
            timeInsertingIntoFlowTable = 0.0
            timeQueryingFlowTimestampedTable = 0.0
            timeInsertingIntoFlowTimestampedTable = 0.0
            
            submitDataOverallProcessingTime = 0

            processingStartTime = time.time()
            numberOfDNSentries = len(dnsDictTimestamped)
            for key, value in dnsDictTimestamped.items():
                ip_list_of_tuples_w_ts = value
                if not ip_list_of_tuples_w_ts:
                    continue
                dns_key_tsd = json.loads(key)
                device_id = dns_key_tsd[0]
                domain = dns_key_tsd[1]
                dns_resolver_ip = dns_key_tsd[2]
                port = dns_key_tsd[3]
                testDevice = execute_select_query(fieldsToSelectAsString="id", tableNameString="device", listOfFieldsToCheckWithWhereClause=['device_id'], parametersTuple=(str(device_id),))
                if testDevice is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDevice in >dnsDictTimestamped< processing! Returning...")
                    return
                testDeviceID = None
                if not testDevice:
                    insert_result = execute_insert_query( tableNameString="device", listOfFieldNamesToBeInsertedAsASingleString="device_id,participant,added_on_timestamp", tupleOfValuesToBeInserted=(str(device_id), currentUserDBrecordID, client_ts, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >device< record in processing dnsDictTimestamped :(")
                        return
                    testDeviceID = insert_result
                else:
                    testDeviceID = testDevice[0][ROW_ID_FIELD_IDX]
                testDomain = execute_select_query(fieldsToSelectAsString="id", tableNameString="dnsdomain", listOfFieldsToCheckWithWhereClause=['domain'], parametersTuple=(str(domain),))
                if testDomain is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDomain in >dnsDictTimestamped< processing! Returning...")
                    return
                testDomainID = None
                if not testDomain:
                    insert_result = execute_insert_query( tableNameString="dnsdomain", listOfFieldNamesToBeInsertedAsASingleString="domain", tupleOfValuesToBeInserted=(domain, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >domain< record in processing dnsDictTimestamped :(")
                        return
                    testDomainID = insert_result
                else:
                    testDomainID = testDomain[0][ROW_ID_FIELD_IDX]
                testDNSresolver = execute_select_query( fieldsToSelectAsString="id", tableNameString="dnsresolver", listOfFieldsToCheckWithWhereClause=['resolver_ip'], parametersTuple=(dns_resolver_ip, ) )
                if testDNSresolver is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDNSresolver in >dnsDictTimestamped< processing! Returning...")
                    return
                testDNSresolverID = None
                if not testDNSresolver:
                    insert_result = execute_insert_query( tableNameString="dnsresolver", listOfFieldNamesToBeInsertedAsASingleString="resolver_ip", tupleOfValuesToBeInserted=(dns_resolver_ip, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >dndsresolver< record in processing dnsDictTimestamped :(")
                        return
                    testDNSresolverID = insert_result
                else:
                    testDNSresolverID = testDNSresolver[0][ROW_ID_FIELD_IDX]

                for aTuple in ip_list_of_tuples_w_ts:
                    if not isinstance( aTuple, tuple ) and not isinstance( aTuple, list ):
                        logger.error("[insert_network_flow_data] ################ [for aTuple in ip_list_of_tuples_w_ts:] 'aTuple' in neither a tuple or a list here! :( It is ..::" + str(type(aTuple)) + "::.. :/ Skipping processing it this time. DBG info: aTuple contains this: " + str(aTuple))
                        continue
                    if len(aTuple) < 2:
                        logger.error("[insert_network_flow_data] ################ [if len(aTuple) < 2:] aTuple has less than 2 elements! It's length is: " + str(len(aTuple)) + " | Contents: " + str(aTuple))
                        continue
                    ip = aTuple[0]
                    ts = aTuple[1]
                    ts = datetime.strptime(ts, '%Y-%m-%d %H:%M:%S.%f')
                    testRequest = execute_select_query( fieldsToSelectAsString="id", tableNameString="dnsrequest", listOfFieldsToCheckWithWhereClause=['iot_device', 'domain', 'resolver_ip', 'request_timestamp'], parametersTuple=(testDeviceID,testDomainID,testDNSresolverID,ts, ) )
                    if testRequest is None:
                        logger.error("[insert_network_flow_data] Error executing query for testDNSrequest in >dnsDictTimestamped< processing! Returning...")
                        return
                    testDNSrequestID = None
                    if not testRequest:
                        insert_result = execute_insert_query( tableNameString="dnsrequest", listOfFieldNamesToBeInsertedAsASingleString="iot_device,domain,resolver_ip,request_timestamp", tupleOfValuesToBeInserted=(testDeviceID,testDomainID,testDNSresolverID,ts, ) )
                        if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                            logger.error("[insert_network_flow_data] Error inserting >dnsrequest< record in processing dnsDictTimestamped :(")
                            return
                        testDNSrequestID = insert_result # It's the ID
                    else:
                        testDNSrequestID = testDNSresolver[0][ROW_ID_FIELD_IDX]
                    if testDNSrequestID is None:
                        logger.error("[insert_network_flow_data] ERROR inserting the DNSrequest record in offline DB insert code! :(")
                        return
            
            timeProcessingDNStsd += time.time() - processingStartTime
            secondsSpentProcessingDNSdict = time.time() - processingStartTime
            logger.error("--done with DNS dict in " + str(round(secondsSpentProcessingDNSdict, 2)) + " s-- [" + str(numberOfDNSentries) + " items]")
            
            processingStartTime = time.time()
            flowsTimeSpentQuerying = 0.0
            flowsTimeSpentInserting = 0.0
            
            numberOfFlowItems = len(flowDictTimestamped)
            
            cacheArrayOfFlowParamsTuplesToBeInserted = []
            for key, value in flowDictTimestamped.items():
                addOpOccured = False
                the_timezone = pytz.timezone(TIMEZONE_NAME_STRING)
                entryProcessingStartTimestamp = datetime.now(the_timezone).strftime('%Y-%m-%d %H"%M:%S.%f')
                entryProcessingStartTimestamp = datetime.strptime(entryProcessingStartTimestamp, '%Y-%m-%d %H"%M:%S.%f')
                flow_key = json.loads(key)
                flow_stats = value
                device_id = flow_key[0]
                device_port = flow_key[1]
                remote_ip = flow_key[2]
                remote_port = flow_key[3]
                protocol = flow_key[4]
                flow_ts = datetime.strptime(flow_key[5], '%Y-%m-%d %H:%M:%S.%f')
                inbound_byte_count = flow_stats['inbound_byte_count']
                outbound_byte_count = flow_stats['outbound_byte_count']
                syn_originator = flow_stats['syn_originator']
                isDNSrequest = flow_stats['isDNSrequest']
                if syn_originator is None:
                    syn_originator = 'NA'
                if syn_originator == '':
                    syn_originator = 'NA'
                queryingStartTime = time.time()
                testDevice = execute_select_query(fieldsToSelectAsString="id", tableNameString="device", listOfFieldsToCheckWithWhereClause=['device_id'], parametersTuple=(str(device_id),))
                flowsTimeSpentQuerying += time.time() - queryingStartTime
                if testDevice is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDevice in >flowDictTimestamped< processing! Returning...")
                    return
                testDeviceID = None
                if not testDevice:
                    insertingStartTime = time.time()
                    insert_result = execute_insert_query( tableNameString="device", listOfFieldNamesToBeInsertedAsASingleString="device_id,participant,added_on_timestamp", tupleOfValuesToBeInserted=(str(device_id), currentUserDBrecordID, client_ts, ) )
                    flowsTimeSpentInserting += time.time() - insertingStartTime
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >device< record in processing flowDictTimestamped :(")
                        return
                    testDeviceID = insert_result
                else:
                    testDeviceID = testDevice[0][ROW_ID_FIELD_IDX]
                
                queryingStartTime = time.time()
                testRemoteEntity = execute_select_query(fieldsToSelectAsString="id", tableNameString="remoteentity", listOfFieldsToCheckWithWhereClause=['ip_address'], parametersTuple=(remote_ip,))
                flowsTimeSpentQuerying += time.time() - queryingStartTime
                if testRemoteEntity is None:
                    logger.error("[insert_network_flow_data] Error executing query for testRemoteEntity in >flowDictTimestamped< processing! Returning...")
                    return
                testRemoteEntityID = None
                if not testRemoteEntity:
                    insertingStartTime = time.time()
                    insert_result = execute_insert_query( tableNameString="remoteentity", listOfFieldNamesToBeInsertedAsASingleString="ip_address", tupleOfValuesToBeInserted=(remote_ip, ) )
                    flowsTimeSpentInserting += time.time() - insertingStartTime
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >remoteEntity< record in processing dnsDictTimestamped :(")
                        return
                    testRemoteEntityID = insert_result
                else:
                    testRemoteEntityID = testRemoteEntity[0][ROW_ID_FIELD_IDX]
                queryingStartTime = time.time()
                testNetworkFlow = execute_select_query(fieldsToSelectAsString="id", tableNameString="networkflow_ts", listOfFieldsToCheckWithWhereClause=['participant', 'iot_device', 'flow_timestamp'], parametersTuple=(currentUserDBrecordID, testDeviceID, flow_ts))
                flowsTimeSpentQuerying += time.time() - queryingStartTime
                if testNetworkFlow is None:
                    logger.error("[insert_network_flow_data] Error executing query for testNetworkFlow in >flowDictTimestamped< processing! Returning...")
                    return
                if not testNetworkFlow:
                    flowParamsToBeInsertedTuple = ( currentUserDBrecordID,testDeviceID,device_port,remote_ip,remote_port,protocol,syn_originator,inbound_byte_count,outbound_byte_count,testRemoteEntityID,isDNSrequest,client_ts,flow_ts,entryProcessingStartTimestamp )
                    cacheArrayOfFlowParamsTuplesToBeInserted.append(flowParamsToBeInsertedTuple)

            if cacheArrayOfFlowParamsTuplesToBeInserted:
                tableToBeInsertedIntoNameString = "networkflow_ts"
                listOfFieldsToBeInsertedPerEachParamsArrayAsSingleString = "participant,iot_device,device_port,remote_ip_or_hostname,remote_port,protocol_used,who_initiated_communication,bytes_received,bytes_sent,remote_entity,is_dns_request,timestamp_of_first_packet,flow_timestamp,record_last_updated"
                insertingStartTime = time.time()
                batchInsertResult = execute_insert_query_multiple_inserts_at_once( tableNameString=tableToBeInsertedIntoNameString, listOfFieldNamesToBeInsertedAsASingleString=listOfFieldsToBeInsertedPerEachParamsArrayAsSingleString, arrayOfTuplesToBeBatchInsertedAtOnce=cacheArrayOfFlowParamsTuplesToBeInserted )
                flowsTimeSpentInserting += time.time() - insertingStartTime
                if batchInsertResult == db.FAILED_INSERT_RETURN_VALUE:
                    logger.error("[insert_network_flow_data] Error batch-inserting flow records into >NetworkFlow_ts< after processing flowDictTimestamped :(")
                    return
                elif batchInsertResult == numberOfFlowItems:
                    logger.error("[insert_network_flow_data] Successfully inserted " + str(batchInsertResult) + " records into 'networkflow_ts' table.")
                else:
                    logger.error("[insert_network_flow_data] ERROR! Tried to insert " + str(numberOfFlowItems) + " flow records bu ended up succeeding inserting " + str(batchInsertResult) + " records ! :( Returning...")
                    return
            else:
                logger.error("[insert_network_flow_data] WARNING: no FLOW records found, nothing entered into the DB, moving on.")
            timeProcessingFlowTsd += time.time() - processingStartTime
            secondsProcessingFlowDict = str(round(time.time() - processingStartTime, 2))
            secondsSpentQueryingLogString = str(round(flowsTimeSpentQuerying, 2))
            secondsSpentInsertingLogString = str(round(flowsTimeSpentInserting, 2))
            logger.error("[insert_network_flow_data] --done with flow dict in " + secondsProcessingFlowDict + " s (SEL: " + secondsSpentQueryingLogString + ", INS: " + secondsSpentInsertingLogString + ")-- [" + str(numberOfFlowItems) + " items]")
            
            processingStartTime = time.time()
            numberOfDeviceItems = len(deviceDictTimestamped)
            for key, value in deviceDictTimestamped.items():
                addOpOccurred = False
                device_id = str(key)
                device_ip = value[0]
                device_oui = value[1]
                device_added_ts = value[2]
                device_added_ts = datetime.strptime(device_added_ts, '%Y-%m-%d %H:%M:%S.%f')
                testDevice = execute_select_query(fieldsToSelectAsString="*", tableNameString="device", listOfFieldsToCheckWithWhereClause=['participant', 'device_id'], parametersTuple=(currentUserDBrecordID,str(device_id),))
                if testDevice is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDevice in >deviceDictTimestamped< processing! Returning...")
                    return
                testDeviceID = None
                if not testDevice:
                    insert_result = execute_insert_query( tableNameString="device", listOfFieldNamesToBeInsertedAsASingleString="device_id,device_ip,device_oui,participant,added_on_timestamp,last_updated", tupleOfValuesToBeInserted=(device_id, device_ip, device_oui, currentUserDBrecordID, device_added_ts, device_added_ts, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >device< record in processing deviceDict :(")
                        return
                    testDeviceID = insert_result
                else:
                    testDeviceID = testDevice[0][ROW_ID_FIELD_IDX]
                    if device_ip != "" and testDevice[0][DEVICE_IP_FIELD_IDX] != device_ip:
                        updateResult = execute_update_query(tableNameString="device", listOfNamesOfFieldsToBeUpdated=['device_ip', 'ip_updated_timestamp', 'last_updated'], listOfNewValuesToBeSet=[device_ip, device_added_ts, device_added_ts], idOfTheRowToBeUpdated=testDeviceID)
                        if updateResult == db.FAILED_UPDATE_RETURN_VALUE:
                            logger.error("[insert_network_flow_data] Error UPDATING >device< IP record in processing deviceDict :(")
                            return
                    if device_oui != "" and testDevice[0][DEVICE_OUI_FIELD_IDX] != device_oui:
                        updateResult = execute_update_query(tableNameString="device", listOfNamesOfFieldsToBeUpdated=['device_oui', 'last_updated'], listOfNewValuesToBeSet=[device_oui, device_added_ts], idOfTheRowToBeUpdated=testDeviceID)
                        if updateResult == db.FAILED_UPDATE_RETURN_VALUE:
                            logger.error("[insert_network_flow_data] Error UPDATING >device< OUI record in processing deviceDict :(")
                            return
            timeDeviceTsd += time.time() - processingStartTime
            deviceDictProcessingTimeInSecondsLogString = str(round(time.time() - processingStartTime, 2))
            logger.error("[insert_network_flow_data] --done with device dict in " + str(deviceDictProcessingTimeInSecondsLogString) + " s-- [" + str(numberOfDeviceItems) + " items]")
            #
            processingStartTime = time.time()
            numberOfUserAgentItems = len(uaDictTimestamped)
            for key, value in uaDictTimestamped.items():
                device_id = str(key)
                tempUAarrayOfArrays = value
                testDevice = execute_select_query(fieldsToSelectAsString="id", tableNameString="device", listOfFieldsToCheckWithWhereClause=['participant', 'device_id'], parametersTuple=(currentUserDBrecordID, str(device_id), ))
                if testDevice is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDevice in >uaDictTimestamped< processing! Returning...")
                    return
                testDeviceID = None
                if not testDevice:
                    insert_result = execute_insert_query( tableNameString="device", listOfFieldNamesToBeInsertedAsASingleString="device_id,participant,added_on_timestamp", tupleOfValuesToBeInserted=(str(device_id), currentUserDBrecordID, client_ts, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >device< record in processing uaDictTimestamped :(")
                        return
                    testDeviceID = insert_result
                else:
                    testDeviceID = testDevice[0][ROW_ID_FIELD_IDX]
                for anArray in tempUAarrayOfArrays:
                    device_ua = anArray[0]
                    if device_ua == '':
                        continue
                    device_ua_ts = datetime.strptime(anArray[1], '%Y-%m-%d %H:%M:%S.%f')
                    testUserAgentEntry = execute_select_query(fieldsToSelectAsString="id", tableNameString="useragent", listOfFieldsToCheckWithWhereClause=['device', 'user_agent_string'], parametersTuple=(testDeviceID,device_ua, ))
                    if testUserAgentEntry is None:
                        logger.error("[insert_network_flow_data] Error executing query for testUserAgentEntry in >uaDictTimestamped< processing! Returning...")
                        return
                    testUserAgentEntryID = None
                    if not testUserAgentEntry:
                        insert_result = execute_insert_query( tableNameString="useragent", listOfFieldNamesToBeInsertedAsASingleString="device,user_agent_string,ua_first_seen_ts", tupleOfValuesToBeInserted=(testDeviceID, currentUserDBrecordID, client_ts, ) )
                        if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                            logger.error("[insert_network_flow_data] Error inserting >userAgentEntry< record in processing uaDictTimestamped :(")
                            return
                        testUserAgentEntryID = insert_result
                    else:
                        testUserAgentEntryID = testUserAgentEntry[0][ROW_ID_FIELD_IDX]
                    if testUserAgentEntryID is None:
                        logger.error("[insert_network_flow_data] ERROR inserting the testUserAgentEntry record in offline DB insert code! :(")
                        return                        
            timeUserAgentTsd += time.time() - processingStartTime
            secondsSpentProcessingUserAgentDictLogString = str(round(time.time() - processingStartTime, 2))
            logger.error("[insert_network_flow_data] --done with UserAgent dict in " + str(secondsSpentProcessingUserAgentDictLogString) + " s-- [" + str(numberOfUserAgentItems) + " items]")
            
            processingStartTime = time.time()
            numberOfDHCPnameItems = len(dhcpDictTimestamped)
            for key, value in dhcpDictTimestamped.items():
                device_id = str(key)
                dhcp_hostname, hostname_ts = value
                hostname_ts = datetime.strptime(hostname_ts, '%Y-%m-%d %H:%M:%S.%f')
                testDevice = execute_select_query(fieldsToSelectAsString="id", tableNameString="device", listOfFieldsToCheckWithWhereClause=['participant', 'device_id'], parametersTuple=(currentUserDBrecordID, str(device_id), ))
                if testDevice is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDevice in >dhcpDictTimestamped< processing! Returning...")
                    return
                testDeviceID = None
                if not testDevice:
                    insert_result = execute_insert_query( tableNameString="device", listOfFieldNamesToBeInsertedAsASingleString="device_id,participant,added_on_timestamp", tupleOfValuesToBeInserted=(str(device_id), currentUserDBrecordID, client_ts, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >device< record in processing dhcpDictTimestamped :(")
                        return
                    testDeviceID = insert_result
                else:
                    testDeviceID = testDevice[0][ROW_ID_FIELD_IDX]
                testDHCPname = execute_select_query(fieldsToSelectAsString="id", tableNameString="dhcpname", listOfFieldsToCheckWithWhereClause=['device', 'dhcp_hostname'], parametersTuple=(testDeviceID, dhcp_hostname, ))
                if testDHCPname is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDHCPname in >dhcpDictTimestamped< processing! Returning...")
                    return
                testDHCPnameID = None
                if not testDHCPname:
                    insert_result = execute_insert_query( tableNameString="dhcpname", listOfFieldNamesToBeInsertedAsASingleString="device,dhcp_hostname,hostname_first_seen_ts", tupleOfValuesToBeInserted=(testDeviceID,dhcp_hostname,hostname_ts, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >DHCPname< record in processing dhcpDictTimestamped :(")
                        return
                    testDHCPnameID = insert_result
                else:
                    testDHCPnameID = testDHCPname[0][ROW_ID_FIELD_IDX]
                if testDHCPnameID is None:
                    logger.error("[insert_network_flow_data] ERROR inserting the DHCPname record in offline DB insert code! :(")
                    return
            timeDHCPTsd += time.time() - processingStartTime
            secondsProcessingDHCPdictLogString = str(round(time.time() - processingStartTime, 2))
            logger.error("[insert_network_flow_data] --done with dhcp dict in " + str(secondsProcessingDHCPdictLogString) + " s-- [" + str(numberOfDHCPnameItems) + " items]")
            
            processingStartTime = time.time()
            numberOfDHCPresolverItems = len(dhcpResolverDictTimestamped)
            for key, value in dhcpResolverDictTimestamped.items():
                device_id = str(key)
                resolver_IP, resIP_ts = value
                resIP_ts = datetime.strptime(resIP_ts, '%Y-%m-%d %H:%M:%S.%f')
                testDevice = execute_select_query(fieldsToSelectAsString="id", tableNameString="device", listOfFieldsToCheckWithWhereClause=['participant', 'device_id'], parametersTuple=(currentUserDBrecordID, str(device_id), ))
                if testDevice is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDevice in >dhcpResolverDictTimestamped< processing! Returning...")
                    return
                testDeviceID = None
                if not testDevice:
                    insert_result = execute_insert_query( tableNameString="device", listOfFieldNamesToBeInsertedAsASingleString="device_id,participant,added_on_timestamp", tupleOfValuesToBeInserted=(str(device_id), currentUserDBrecordID, client_ts, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >device< record in processing dhcpResolverDictTimestamped :(")
                        return
                    testDeviceID = insert_result
                else:
                    testDeviceID = testDevice[0][ROW_ID_FIELD_IDX]
                testResolverIP = execute_select_query( fieldsToSelectAsString="id", tableNameString="dhcpresolverip", listOfFieldsToCheckWithWhereClause=['device', 'resolver_ip'], parametersTuple=(testDeviceID, resolver_IP, ) )
                if testResolverIP is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDHCPresolverIP in >dhcpResolverDictTimestamped< processing! Returning...")
                    return
                testDHCPresolverID = None
                if not testResolverIP:
                    insert_result = execute_insert_query( tableNameString="dhcpresolverip", listOfFieldNamesToBeInsertedAsASingleString="device,resolver_ip,resolv_ip_first_seen_ts", tupleOfValuesToBeInserted=(testDeviceID,resolver_IP, resIP_ts, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >dhcpresolverip< record in processing dhcpResolverDictTimestamped :(")
                        return
                    testDHCPresolverID = insert_result
                else:
                    testDHCPresolverID = testResolverIP[0][ROW_ID_FIELD_IDX]
                if testDHCPresolverID is None:
                    logger.error("[insert_network_flow_data] ERROR inserting the DHCPResolverIP record in offline DB insert code! :(")
                    return
            timeDHCPresolverIPTsd += time.time() - processingStartTime
            secondsProcessingDHCPresolverDictLogString = str(round(time.time() - processingStartTime, 2))
            logger.error("[insert_network_flow_data] --done with dhcpResolver dict in " + str(secondsProcessingDHCPresolverDictLogString) + " s-- [" + str(numberOfDHCPresolverItems) + " items]")
            
            processingStartTime = time.time()
            numberOfTLSdictItems = len(tlsDictListTimestamped)
            for tlsDict in tlsDictListTimestamped:
                device_id = tlsDict['device_id']
                timestamp = tlsDict['timestamp']
                client_hello = ''
                clientHelloDictKey = 'client_hello'
                server_hello = ''
                serverHelloDictKey = 'server_hello'
                client_cert = ''
                clientCertDictKey = 'clientCert'
                try:                    
                    if clientHelloDictKey in tlsDict:
                        client_hello = json.loads(tlsDict[clientHelloDictKey])
                    if serverHelloDictKey in tlsDict:
                        server_hello = json.loads(tlsDict[serverHelloDictKey])
                    if clientCertDictKey in tlsDict:
                        client_cert = json.loads(tlsDict[clientCertDictKey])
                except Exception as e:
                    logger.error("[insert_network_flow_data] Failed to read TLS dicts here with exception: "+ str(e) + "\nReturning...")
                    return
                testDevice = execute_select_query(fieldsToSelectAsString="id", tableNameString="device", listOfFieldsToCheckWithWhereClause=['participant', 'device_id'], parametersTuple=(currentUserDBrecordID, str(device_id), ))
                if testDevice is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDevice in >dhcpDictTimestamped< processing! Returning...")
                    return
                testDeviceID = None
                if not testDevice:
                    insert_result = execute_insert_query( tableNameString="device", listOfFieldNamesToBeInsertedAsASingleString="device_id,participant,added_on_timestamp", tupleOfValuesToBeInserted=(str(device_id), currentUserDBrecordID, client_ts, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >device< record in processing dhcpDictTimestamped :(")
                        return
                    testDeviceID = insert_result
                else:
                    testDeviceID = testDevice[0][ROW_ID_FIELD_IDX]

                client_hello_insert_result = execute_insert_query( tableNameString="tlsclienthello", listOfFieldNamesToBeInsertedAsASingleString="iot_device,participant,timestamp,tls_version,cipher_suites,cipher_suite_uses_grease,compression_methods,extension_types,extension_details,extension_uses_grease,sni,remote_ip,remote_port,device_ip,device_port,client_ts", tupleOfValuesToBeInserted=(device_id,currentUserDBrecordID,timestamp,client_hello['version'],client_hello['cipher_suites'],client_hello['cipher_suite_uses_grease'],client_hello['compression_methods'],client_hello['extension_types'],client_hello['extension_details'],client_hello['extension_uses_grease'],client_hello['sni'],client_hello['remote_ip'],client_hello['remote_port'],client_hello['device_ip'],client_hello['device_port'],client_hello['client_ts'], ) )
                if client_hello_insert_result == db.FAILED_INSERT_RETURN_VALUE:
                    logger.error("[insert_network_flow_data] Error inserting >client_hello< record in processing tlsDictListTimestamped :(")
                    return
                server_hello_insert_result = execute_insert_query( tableNameString="tlserverhello", listOfFieldNamesToBeInsertedAsASingleString="iot_device,participant,timestamp,version,cipher_suite,device_ip,device_port,remote_ip,remote_port,client_ts", tupleOfValuesToBeInserted=(device_id,currentUserDBrecordID,timestamp,server_hello['version'],server_hello['cipher_suite'],server_hello['device_ip'],server_hello['device_port'],server_hello['remote_ip'],server_hello['remote_port'],server_hello['client_ts'], ) )
                if server_hello_insert_result == db.FAILED_INSERT_RETURN_VALUE:
                    logger.error("[insert_network_flow_data] Error inserting >server_hello< record in processing tlsDictListTimestamped :(")
                    return
                client_cert_insert_result = execute_insert_query( tableNameString="tlsclientcert", listOfFieldNamesToBeInsertedAsASingleString="iot_device,participant,timestamp,pubkey,signature,cert_key_hash,remote_ip,remote_port,device_ip,device_port,client_ts", tupleOfValuesToBeInserted=(device_id,currentUserDBrecordID,timestamp,client_cert['pubkey'],client_cert['signature'],client_cert['hash'],client_cert['remote_ip'],client_cert['remote_port'],client_cert['device_ip'],client_cert['device_port'],client_cert['client_ts'], ) )
                if client_cert_insert_result == db.FAILED_INSERT_RETURN_VALUE:
                    logger.error("[insert_network_flow_data] Error inserting >client_cert< record in processing tlsDictListTimestamped :(")
                    return                
            timeTlsDictTsd += time.time() - processingStartTime
            timeProcessingTLSdictLogString = str(round(time.time() - processingStartTime, 2))
            logger.error("[insert_network_flow_data] -- done with TLS dict in " + str(timeProcessingTLSdictLogString) + " s -- [" + str(numberOfTLSdictItems) + " items]")
            
            processingStartTime = time.time()
            numberOfNetdiscoDictItems = len(netdiscoDictTimestamped)
            for key, value in netdiscoDictTimestamped.items():
                device_id = str(key)
                netd_dict_tuple = value
                netdisco_dict_firstone = netd_dict_tuple[0]
                netdisco_dict = netdisco_dict_firstone[0]
                netdisco_dict_ts = netdisco_dict_firstone[1]
                netdisco_dict_ts = datetime.strptime(netdisco_dict_ts, '%Y-%m-%d %H:%M:%S.%f')
                device_type = netdisco_dict['device_type']
                netdisco_properties_dict = netdisco_dict['properties']
                device_name = ''
                if 'fn' in netdisco_properties_dict:
                    device_name = netdisco_properties_dict['fn']
                elif 'name' in netdisco_properties_dict:
                    device_name = netdisco_properties_dict['name']
                else:
                    device_name = netdisco_dict['name']
                testDevice = execute_select_query(fieldsToSelectAsString="id", tableNameString="device", listOfFieldsToCheckWithWhereClause=['participant', 'device_id'], parametersTuple=(currentUserDBrecordID, str(device_id), ))
                if testDevice is None:
                    logger.error("[insert_network_flow_data] Error executing query for testDevice in >dhcpDictTimestamped< processing! Returning...")
                    return
                testDeviceID = None
                if not testDevice:
                    insert_result = execute_insert_query( tableNameString="device", listOfFieldNamesToBeInsertedAsASingleString="device_id,participant,device_type,added_on_timestamp", tupleOfValuesToBeInserted=(str(device_id), currentUserDBrecordID, device_type, client_ts, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >device< record in processing dhcpDictTimestamped :(")
                        return
                    testDeviceID = insert_result
                else:
                    testDeviceID = testDevice[0][ROW_ID_FIELD_IDX]
                testNetdiscoDeviceName = execute_select_query(fieldsToSelectAsString="id", tableNameString="netdiscodevname", listOfFieldsToCheckWithWhereClause=['device', 'netd_name'], parametersTuple=(testDeviceID,device_name, ))
                if testNetdiscoDeviceName is None:
                    logger.error("[insert_network_flow_data] Error executing query for testNetdiscoDeviceName in >netdiscoDictTimestamped< processing! Returning...")
                    return
                testNetdiscoDeviceNameID = None
                if not testNetdiscoDeviceName:
                    insert_result = execute_insert_query( tableNameString="netdiscodevname", listOfFieldNamesToBeInsertedAsASingleString="device,netd_name,netd_name_first_seen_ts", tupleOfValuesToBeInserted=(testDeviceID, device_name, netdisco_dict_ts, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >NetdiscoDeviceName< record in processing netdiscoDictTimestamped :(")
                        return
                    testNetdiscoDeviceNameID = insert_result
                else:
                    testNetdiscoDeviceNameID = testNetdiscoDeviceName[0][ROW_ID_FIELD_IDX]
                if testNetdiscoDeviceNameID is None:
                    logger.error("[insert_network_flow_data] ERROR inserting the testNetdiscoDeviceName record in offline DB insert code! :(")
                    return
                testNetdiscoDeviceType = execute_select_query(fieldsToSelectAsString="id", tableNameString="netdiscodevtype", listOfFieldsToCheckWithWhereClause=['device', 'netd_type'], parametersTuple=(testDeviceID,device_type, ))
                if testNetdiscoDeviceType is None:
                    logger.error("[insert_network_flow_data] Error executing query for testNetdiscoDeviceType in >netdiscoDictTimestamped< processing! Returning...")
                    return
                testNetdiscoDeviceTypeID = None
                if not testNetdiscoDeviceType:
                    insert_result = execute_insert_query( tableNameString="netdiscodevtype", listOfFieldNamesToBeInsertedAsASingleString="device,netd_type,netd_type_first_seen_ts", tupleOfValuesToBeInserted=(testDeviceID, device_type, netdisco_dict_ts, ) )
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_network_flow_data] Error inserting >NetdiscoDeviceType< record in processing netdiscoDictTimestamped :(")
                        return
                    testNetdiscoDeviceTypeID = insert_result
                else:
                    testNetdiscoDeviceTypeID = testNetdiscoDeviceType[0][ROW_ID_FIELD_IDX]
                if testNetdiscoDeviceTypeID is None:
                    logger.error("[insert_network_flow_data] ERROR inserting the testNetdiscoDeviceType record in offline DB insert code! :(")
                    return
            timeNetdiscoTsd += time.time() - processingStartTime
            secondsTakenToProcessNetdiscoDictLogString = str(round(time.time() - processingStartTime, 2))
            logger.error("[insert_network_flow_data] --done with netdisco dict in " + str(secondsTakenToProcessNetdiscoDictLogString) +" s-- [" + str(numberOfNetdiscoDictItems) + " items]")
            
            submitDataOverallProcessingTime = time.time() - submitDataProcessingStartTime
            numberOfJSONfilesWithFSMstatusInfoProcessed += 1
            ################################################################################
            #### Done with Data inserting CODE !
            ################################################################################
            # Done with processing all entries in the file, check if all the fields found were successfuly saved in the database
            # saving the full path to the current one here
            listOfFilepathsOfSuccessfulyInsertedJSONfiles.append(currentJsonFilePath)
            logger.error("[insert_network_flow_data] Done processing '" + str(currentJsonFilePath) + "'")
        else:
            continue # We don't process non-json files here, i.e. the log file of this script
    # END of the FOR loop here
    # Remove the successfully processed JSON files, of which all contents have been now inserted into the DB
    for filepath in listOfFilepathsOfSuccessfulyInsertedJSONfiles:
        if os.path.exists(filepath):
            os.remove(filepath)
            logger.error("[insert_network_flow_data] Removing processed file '" + str(filepath) + "'")
        else:
            logger.error("[insert_network_flow_data] Attemped to remove non-existent file '" + str(filepath) + "' !")
    secondsSpentInsertingNetworkFlowRecordsIntoTheDB = timeOfInsertingNetworkFlowRecordsIntoTheDB = round(time.time() - networkFlowInsertingStartTime, 2)
    logger.error("[insert_network_flow_data] Time spent inserting network flow records from " + str(numberOfJSONfilesWithFSMstatusInfoProcessed) + " JSON files is: " + str(secondsSpentInsertingNetworkFlowRecordsIntoTheDB) + " seconds.")
    logger.error("[insert_network_flow_data] ########## NETWORK FLOWS INSERTING PROCESS FINISHED ##########")

#### ----
#### ---- Pre-processing JSON files saved on disk by the web-app part of the Collector module here (specifically mostly sorting by timestamp and testing for sequence number errors)
#### ----

def get_seq_num_ordered_list_of_FSM_records():
    logger.error("[get_seq_num_ordered_list_of_FSM_records] ########## Beginning to sort FSM records ##########")

    listOfDirContents = LIST_OF_CURRENT_FSM_INFO_JSON_FILES

    LIST_OF_ALL_CURRENT_FSM_RECORDS = []
    totalNumberOfCollectedFSMrecords = -1

    numberOfZeroOccurrencesOutOfOrder = 0

    nonConsecutiveSeqNumCount = 0
    equalSeqNumCount = 0
    notWraparoundOverZeroCount = 0

    numberOfFilesFoundInTheDir = len(listOfDirContents)
    currentlyProcessedFileIdx = 0
    listOfFilepathsOfProcessedJSONfiles = []
    logger.error("[get_seq_num_ordered_list_of_FSM_records] Reading " + str(numberOfFilesFoundInTheDir) + " files found in the directory...")

    for filenameStr in listOfDirContents:
        filename = os.fsdecode(filenameStr)
        currentlyProcessedFileIdx += 1
        if filename.endswith(".json") or filename.endswith(".JSON"):
            currentJsonFilePath = os.path.join(FSM_status_data_dir, filenameStr)
            listOfFSMstatusDicts = None
            try:
                with open(currentJsonFilePath) as jsonFileHandle:
                    listOfFSMstatusDicts = json.load(jsonFileHandle)
            except Exception as e:
                logger.error("[get_seq_num_ordered_list_of_FSM_records] Failed to load the JSON file '" + str(currentJsonFilePath) + "' with exception: "+ str(e) + "\nReturning...")
                return
            if listOfFSMstatusDicts is None:
                logger.error("[get_seq_num_ordered_list_of_FSM_records] The call to 'json.load' returned None! Abandoning further execution.")
                return
            listOfFilepathsOfProcessedJSONfiles.append(currentJsonFilePath)
            for anFSMstatusDict in listOfFSMstatusDicts:
                LIST_OF_ALL_CURRENT_FSM_RECORDS.append(anFSMstatusDict)
    totalNumberOfCollectedFSMrecords = len(LIST_OF_ALL_CURRENT_FSM_RECORDS)

    logger.error("[get_seq_num_ordered_list_of_FSM_records] Collected " + str(totalNumberOfCollectedFSMrecords) + " FSM status records.")

    LIST_OF_ALL_CURRENT_FSM_RECORDS.sort(key = lambda record_dict: datetime.strptime(record_dict['ts_DATETIME6'], '%Y-%m-%d %H:%M:%S.%f'))

    timezone_here = pytz.timezone(TIMEZONE_NAME_STRING)

    fn_form_timestamp = datetime.now(timezone_here).strftime("%Y-%m-%dT%H-%M-%S.%f")
    output_fn_prefix = "sortedFSMrecords_"
    output_file_extension = "json"
    output_filename = output_fn_prefix + fn_form_timestamp + "." + output_file_extension

    output_file_path = pathlib.Path(__file__).parent.absolute() / NAME_OF_SORTED_FSM_STATUS_RECORDS_DIR / output_filename

    with open(str(output_file_path), 'w') as jsonFileHandle:
        try:
            json.dump(LIST_OF_ALL_CURRENT_FSM_RECORDS, jsonFileHandle)
        except Exception as e:
            logger.error("[get_seq_num_ordered_list_of_FSM_records] Exception while saving the LIST_OF_ALL_CURRENT_FSM_RECORDS:\n" + str(e))
            return
    logger.error("[get_seq_num_ordered_list_of_FSM_records] Successfully saved the sorted list into the '" + str(output_file_path) + "' file.")

    logger.error("[get_seq_num_ordered_list_of_FSM_records] Removing processed files:")

    for filepath in listOfFilepathsOfProcessedJSONfiles:
        if os.path.exists(filepath):
            os.remove(filepath)
            logger.error("[get_seq_num_ordered_list_of_FSM_records] Removing processed file '" + str(filepath) + "'")
        else:
            logger.error("[get_seq_num_ordered_list_of_FSM_records] Attemped to remove non-existent file '" + str(filepath) + "' !")   

    initDictPlaceholder = {'INITIAL': 'DICT'}
    prevDict = initDictPlaceholder
    noFinishedHeartbeatSelf = 0
    hrtbFinInPrev = 0
    hrtbFinInCurr = 0
    totalNumberOfLostFSMstatusRecords = 0
    for anFSMstatusDict in LIST_OF_ALL_CURRENT_FSM_RECORDS:
        if prevDict == initDictPlaceholder:
            prevDict = anFSMstatusDict
            continue
        heartbeatIn = False
        prevSeqNum = int(prevDict['recordSequenceNumber'])
        currSeqNum = int(anFSMstatusDict['recordSequenceNumber'])
        if prevDict['transitionName'] == 'heartbeat_triggered' or anFSMstatusDict['transitionName'] == 'heartbeat_triggered':
            heartbeatIn = True
        if prevSeqNum < currSeqNum:
            if prevSeqNum != currSeqNum - 1:
                nonConsecutiveSeqNumCount += 1
                totalNumberOfLostFSMstatusRecords += currSeqNum - prevSeqNum - 1
                if heartbeatIn:
                    noFinishedHeartbeatSelf += 1

                logger.error("nconsec: prevSeqNum = " + str(prevSeqNum) + " | currSeqNum = " + str(currSeqNum))
        elif prevSeqNum == currSeqNum:
            equalSeqNumCount += 1
            if heartbeatIn:
                noFinishedHeartbeatSelf += 1

            logger.error("equalSeqNum: prevSeqNum = " + str(prevSeqNum) + " | currSeqNum = " + str(currSeqNum))

        else:
            if currSeqNum != 0:

                notWraparoundOverZeroCount += 1
                totalNumberOfLostFSMstatusRecords += prevSeqNum - currSeqNum - 1
                if heartbeatIn:
                    noFinishedHeartbeatSelf += 1


                logger.error("nonWrap: prevSeqNum = " + str(prevSeqNum) + " | currSeqNum = " + str(currSeqNum))

        prevDict = anFSMstatusDict

    totalNumberOfDetectedSequenceErrors = nonConsecutiveSeqNumCount + equalSeqNumCount + notWraparoundOverZeroCount
    ratioOfSequenceNumberErrors = round( (totalNumberOfDetectedSequenceErrors / totalNumberOfCollectedFSMrecords) * 100, 2 )
    ratioOfNonConsecSeqNums = round( (nonConsecutiveSeqNumCount / totalNumberOfDetectedSequenceErrors) * 100, 2 )
    ratioOfEqualSeqNums = round( (equalSeqNumCount / totalNumberOfDetectedSequenceErrors) * 100, 2 )
    ratioOfNotWrapSeqErrors = round( (notWraparoundOverZeroCount / totalNumberOfDetectedSequenceErrors) * 100, 2 )
    ratioOfLostRecords = round( (totalNumberOfLostFSMstatusRecords / totalNumberOfCollectedFSMrecords) * 100, 2 )


    ratioOfLostRecordsLOGmsg = "[get_seq_num_ordered_list_of_FSM_records] Ratio of lost records: " + str(ratioOfLostRecords) + "% ( " + str(totalNumberOfLostFSMstatusRecords) + "/" + str(totalNumberOfCollectedFSMrecords) + ")"
    ratioOfSeqNumErrorsLOGmsg = "[get_seq_num_ordered_list_of_FSM_records] Ratio of seq num errors: " + str(ratioOfSequenceNumberErrors) + "% (" + str(totalNumberOfDetectedSequenceErrors) + "/" + str(totalNumberOfCollectedFSMrecords) + ")"
    ratioOfNonConsecSeqNumErrorsLOGmsg = "[get_seq_num_ordered_list_of_FSM_records] Ratio of non-consecutive [within all seq num errors]: " + str(ratioOfNonConsecSeqNums) + "% (" + str(nonConsecutiveSeqNumCount) + "/" + str(totalNumberOfDetectedSequenceErrors) + ")"
    ratioOfEqualSeqNumsErrorsLOGmsg = "[get_seq_num_ordered_list_of_FSM_records] Ratio of equal-nums [within all seq num errors]: " + str(ratioOfEqualSeqNums) + "% (" + str(equalSeqNumCount) + "/" + str(totalNumberOfDetectedSequenceErrors) + ")"
    ratioOfNotWrapSerErrorsLOGmsg = "[get_seq_num_ordered_list_of_FSM_records] Ratio of not-wrap-around [within all seq num errors]: " + str(ratioOfNotWrapSeqErrors) + "% (" + str(notWraparoundOverZeroCount) + "/" + str(totalNumberOfDetectedSequenceErrors) + ")"
    FSMrecordsOrderingReportString = ratioOfLostRecordsLOGmsg + "\n" + ratioOfSeqNumErrorsLOGmsg + "\n" + ratioOfNonConsecSeqNumErrorsLOGmsg + "\n" + ratioOfEqualSeqNumsErrorsLOGmsg + "\n" + ratioOfNotWrapSerErrorsLOGmsg
    logger.error(ratioOfLostRecordsLOGmsg)
    logger.error(ratioOfSeqNumErrorsLOGmsg)
    logger.error(ratioOfNonConsecSeqNumErrorsLOGmsg)
    logger.error(ratioOfEqualSeqNumsErrorsLOGmsg)
    logger.error(ratioOfNotWrapSerErrorsLOGmsg)
    logger.error("[get_seq_num_ordered_list_of_FSM_records] ########## Finished sorting FSM records ##########")

    return (LIST_OF_ALL_CURRENT_FSM_RECORDS, FSMrecordsOrderingReportString)

#### ----
#### ---- Inserting FSM status data from the list of dictionaries pre-processed (sorted) by the 'get_seq_num_ordered_list_of_FSM_records' method
#### ----

def insert_FSM_status_data(argTuple):
    logger.error("[insert_FSM_status_data] ########## FSM STATUS UPDATE PROCESS STARTED ##########")

    sorted_FSM_records = argTuple[0]
    FSMrecordsOrderingStatusString = argTuple[1]
    
    totalNumberOfNetworkFlows = -1
    totalNumberOfNetworkFlowsToBeUpdated = -1
    listOfFilepathsOfSuccessfulyUpdatedJSONfiles = []
    PREVIOUS_DICT_FOR_STITCHING = {}
    FSMstatusDataProcessingSTARTtime = time.time()
    listOfDirContents = LIST_OF_CURRENT_FSM_INFO_JSON_FILES

    TRANSITION_STARTING_STRING = 'starting'
    TRANSITION_FINISHED_STRING = 'finished'
    totalNumberOfProcessedTransitionPairs = 0

    totalNumberOfDetectedStates = 0
    totalNumberOfDetectedTransitions = 0
    numberOfStatesWithMismatchedDestAndSource = 0

    numberOfTransitionKindOfRecordsPairsWithMismatchedTransitionName = 0
    numberOfTransitionWithMismatchedSourceOrDestStateInfo = 0
    numberOfNeitherStateNorTransitionDetections = 0
    
    numberOfErrorsOfNotConsecutiveSequenceNumbers = 0
    numberOfSequenceNotWraparoundErrors = 0
    
    numberOfFilesFoundInTheDir = len(listOfDirContents)
    currentlyProcessedFileIdx = 0


    LIST_OF_DICT_WITH_USER_KEY_NOT_IN_DB = []


    numberOfStateChangeEntries = len(sorted_FSM_records)
    stateChangeEntryIndex = 0
    for aDict in sorted_FSM_records:
        stateChangeEntryIndex += 1
        logger.error("[insert_FSM_status_data] Processing status change [" + str(stateChangeEntryIndex) + " of " + str(numberOfStateChangeEntries) + "]")
        if not PREVIOUS_DICT_FOR_STITCHING:

            user_key = aDict['user_key']
            firstFSMrecordUserDBrow = execute_select_query(fieldsToSelectAsString="id", tableNameString="studyparticipant", listOfFieldsToCheckWithWhereClause=['user_key'], parametersTuple=(user_key,))
            if firstFSMrecordUserDBrow is None:
                logger.error("[insert_FSM_status_data] Error querying for the user_key '" + user_key + "' of the first FSM record in this run.")
                return
            elif len(firstFSMrecordUserDBrow) > 1:
                logger.error("[insert_FSM_status_data] The query for user_key of the first FSM record in this run of the script found more than 1 record for '" + user_key + "'")
                return
            elif not firstFSMrecordUserDBrow:
                LIST_OF_DICT_WITH_USER_KEY_NOT_IN_DB.append(aDict)

                logger.error("\n####\n[insert_FSM_status_data] No results were found by the query searching for a user of the Interceptor module identified by user_key '" + user_key + "'! Ignoring this FSM status record during processing but saving to a separate file later.")                
            PREVIOUS_DICT_FOR_STITCHING = aDict

            INFOmsg = "[insert_FSM_status_data] First FSM status record in this run, saving and advancing (no pair of records seen yet)"
            logger.error(INFOmsg)
            continue
        else:
            totalNumberOfProcessedTransitionPairs += 1

        PREVIOUS_user_key = PREVIOUS_DICT_FOR_STITCHING['user_key']
        PREVIOUS_device_id = PREVIOUS_DICT_FOR_STITCHING['device_id']
        PREVIOUS_ts_DATETIME6 = PREVIOUS_DICT_FOR_STITCHING['ts_DATETIME6']
        PREVIOUS_sequence_number = int(PREVIOUS_DICT_FOR_STITCHING['recordSequenceNumber'])
        PREVIOUS_transitionName = PREVIOUS_DICT_FOR_STITCHING['transitionName']
        PREVIOUS_startingOrFinished = PREVIOUS_DICT_FOR_STITCHING['startingOrFinished']
        PREVIOUS_fromState = PREVIOUS_DICT_FOR_STITCHING['fromState']
        PREVIOUS_toState = PREVIOUS_DICT_FOR_STITCHING['toState']
        PREVIOUS_listOfConditionsMet = PREVIOUS_DICT_FOR_STITCHING['listOfConditionsMet']
        PREVIOUS_listOfConditionsNotMet = PREVIOUS_DICT_FOR_STITCHING['listOfConditionsNotMet']

        CURRENT_user_key = aDict['user_key']
        CURRENT_device_id = aDict['device_id']
        CURRENT_ts_DATETIME6 = aDict['ts_DATETIME6']
        CURRENT_sequence_number = int(aDict['recordSequenceNumber'])
        CURRENT_transitionName = aDict['transitionName']
        CURRENT_startingOrFinished = aDict['startingOrFinished']
        CURRENT_fromState = aDict['fromState']
        CURRENT_toState = aDict['toState']
        CURRENT_listOfConditionsMet = aDict['listOfConditionsMet']
        CURRENT_listOfConditionsNotMet = aDict['listOfConditionsNotMet']

        if PREVIOUS_user_key != CURRENT_user_key:
            logger.error("\n####\n[insert_FSM_status_data] PREVIOUS_user_key != CURRENT_user_key! I.e. we see data from 2 different Interceptor modules! Quitting...")
            return
        if PREVIOUS_device_id != CURRENT_device_id:
            logger.error("\n####\n[insert_FSM_status_data] PREVIOUS_device_id != CURRENT_device_id! I.e. we see data from 2 different devices here! Quitting...")
            return

        currentUserDBrecord = execute_select_query(fieldsToSelectAsString="id", tableNameString="studyparticipant", listOfFieldsToCheckWithWhereClause=['user_key'], parametersTuple=(CURRENT_user_key,))
        if currentUserDBrecord is None:
            logger.error("\n####\n[insert_FSM_status_data] There is no such user/participant in the DB -- or -- there's an error in the implementation of the offline DB insert code. Quitting...")
            return
        elif len(currentUserDBrecord) > 1:
            logger.error("\n####\n[insert_FSM_status_data] Error! The 'currentUserDBrecord' returns more than 1 result per the user_key '" + user_key + "'! Quitting...")
            return
        elif not currentUserDBrecord:

            logger.error("\n####\n[insert_FSM_status_data] No results were found by the query searching for a user of the Interceptor module identified by user_key '" + user_key + "'! Ignoring this FSM status record during processing but saving to a separate file later.")
            LIST_OF_DICT_WITH_USER_KEY_NOT_IN_DB.append(aDict)

            continue
        currentUserDBrecordID = currentUserDBrecord[0][ROW_ID_FIELD_IDX]

        testDevice = execute_select_query(fieldsToSelectAsString="id", tableNameString="device", listOfFieldsToCheckWithWhereClause=['device_id'], parametersTuple=(str(CURRENT_device_id),))
        if testDevice is None:

            logger.error("\n####\n[insert_FSM_status_data] There is no such device of ID = " + str(CURRENT_device_id) + " in the DB at the moment ! Returning...")
            return
        elif len(testDevice) > 1:
            logger.error("\n####\n[insert_FSM_status_data] Found MORE THAN ONE records matching given CURRENT_device_id = " + str(CURRENT_device_id) + "! Quitting...")
            return
        elif not testDevice:

            logger.error("\n####\n[insert_FSM_status_data] The query for 'testDeviceID' with CURRENT_device_id = " + str(CURRENT_device_id) + " did not find any devices for the device_id '" + device_id + "'! Quitting...")
            return

        testDeviceID = testDevice[0][ROW_ID_FIELD_IDX]


        current_FSM_status_period_START_ts = PREVIOUS_ts_DATETIME6

        current_FSM_status_period_END_ts = CURRENT_ts_DATETIME6

        current_FSM_status_period_is_a_state = False

        if PREVIOUS_startingOrFinished == TRANSITION_FINISHED_STRING and CURRENT_startingOrFinished == TRANSITION_STARTING_STRING:

            ##########################
            #### Detected a STATE ####
            ##########################
            totalNumberOfDetectedStates += 1

            if PREVIOUS_toState != CURRENT_fromState:
                errorMsg = ""

                errorMsg += "[insert_FSM_status_data] Found an incoherent pair of FSM status records pointing to a STATE"
                errorMsg += "\nThe PREVIOUS transition points to a different 'dest' state than the CURRENT's transition 'from' state"
                errorMsg += "\nPREVIOUS_toState: " + str(PREVIOUS_toState) + " | CURRENT_fromState:" + str(CURRENT_fromState)
                errorMsg += "\n -- Skipping this records pair and moving on..."
                logger.error(errorMsg)

                PREVIOUS_DICT_FOR_STITCHING = aDict
                numberOfStatesWithMismatchedDestAndSource += 1
                continue
            else:
                 current_FSM_status_period_is_a_state = True
        elif PREVIOUS_startingOrFinished == TRANSITION_STARTING_STRING and CURRENT_startingOrFinished == TRANSITION_FINISHED_STRING:

            ###############################
            #### Detected a TRANSITION ####
            ###############################
            totalNumberOfDetectedTransitions += 1

            if PREVIOUS_transitionName != CURRENT_transitionName:

                errorMsg = "[insert_FSM_status_data] Found an incoherent pair of FSM status records pointing to a TRANSITION (differing name)"
                errorMsg += "\nPREVIOUS_transitionName: " + str(PREVIOUS_transitionName) + " | CURRENT_transitionName: " + str(CURRENT_transitionName)
                errorMsg += "\n -- Skipping this pair of records and moving on now..."
                logger.error(errorMsg)
                numberOfTransitionKindOfRecordsPairsWithMismatchedTransitionName += 1
                PREVIOUS_DICT_FOR_STITCHING = aDict
                continue

            if PREVIOUS_fromState != CURRENT_fromState or PREVIOUS_toState != CURRENT_toState:
                errorMsg = "[insert_FSM_status_data] Found an incoherent pair of FSM status records pointing to a TRANSITION"
                errorMsg += "\nIt doesn't seem to be records describing the same transition ('fromState' or/and 'toState' differ)"
                errorMsg += "\nPREVIOUS_fromState: " + str(PREVIOUS_fromState) + " != CURRENT_fromState: " + str(CURRENT_fromState)
                errorMsg += "\nPREVIOUS_toState: " + str(PREVIOUS_toState) + " != CURRENT_toState: " + str(CURRENT_toState)
                errorMsg += "\nSkipping this record pair and continuing..."
                logger.error(errorMsg)
                numberOfTransitionWithMismatchedSourceOrDestStateInfo += 1
                PREVIOUS_DICT_FOR_STITCHING = aDict
                continue
        else:
            ###############################################################
            #### Detected NEITHER a state NOR a transition in our book ####
            ###############################################################
            numberOfNeitherStateNorTransitionDetections += 1

            errorMsg = "[insert_FSM_status_data] Detected neither a STATE nor a TRANSITION pair of aDict entries!"
            if PREVIOUS_startingOrFinished == TRANSITION_STARTING_STRING and CURRENT_startingOrFinished == TRANSITION_STARTING_STRING:
                errorMsg += "\nDetected two FSM status records both describing a " + TRANSITION_STARTING_STRING + " event!"
            elif PREVIOUS_startingOrFinished == TRANSITION_FINISHED_STRING and CURRENT_startingOrFinished == TRANSITION_FINISHED_STRING:
                errorMsg += "\nDetected two FSM status records both describing a " + TRANSITION_FINISHED_STRING + " event!"
            else:
                errorMsg += "\nUNEXPECTED event! (THIS SHOULD NOT HAPPEN ACCORDING TO CURRENT LOGIC!) Detected this pair of records: "
            errorMsg = "\nThere seems to be a missing entry between the PREVIOUS and the CURRENT FSM status period - the integirty of FSM records is broken!"
            errorMsg += "\nPREVIOUS: " + str(PREVIOUS_DICT_FOR_STITCHING)
            errorMsg += "\nCURRENT: " + str(aDict)
            errorMsg += "\nSkipping this records pair for now and continuing..."
            logger.error(errorMsg)
            PREVIOUS_DICT_FOR_STITCHING = aDict
            continue

        if totalNumberOfNetworkFlows == -1:
            testNumberOfRecordsInNetworkFlowTsTable = execute_select_query(fieldsToSelectAsString="count(*)", tableNameString="networkflow_ts", listOfFieldsToCheckWithWhereClause=['participant', 'iot_device'], parametersTuple=(currentUserDBrecordID, testDeviceID))
            if testNumberOfRecordsInNetworkFlowTsTable is None:
                logger.error("[insert_FSM_status_data] Error retrieving the number of networkflow_ts records, testNumberOfRecordsInNetworkFlowTsTable is None! Returning...")
                return
            if not testNumberOfRecordsInNetworkFlowTsTable:
                logger.error("[insert_FSM_status_data] Error retrieving the number of networkflow_ts records, the results list (testNumberOfRecordsInNetworkFlowTsTable) is empty!")
                return
            else:
                totalNumberOfNetworkFlows = testNumberOfRecordsInNetworkFlowTsTable[0][0]
     
        customComparisonOperatorsListForTheSelectQuery = ['=', '=', '>', '<=']

        testNetworkFlowIDsListOfArrays = execute_select_query(fieldsToSelectAsString="id", tableNameString="networkflow_ts", listOfFieldsToCheckWithWhereClause=['participant', 'iot_device', 'flow_timestamp', 'flow_timestamp'], parametersTuple=(currentUserDBrecordID, testDeviceID, current_FSM_status_period_START_ts, current_FSM_status_period_END_ts), useUserDefinedComparisonOperatorsList=True, userDefinedComparisonOperatorsList=customComparisonOperatorsListForTheSelectQuery)
        
        if testNetworkFlowIDsListOfArrays is None:

            logger.error("[insert_FSM_status_data] Error executing query for testNetworkFlowIDsListOfArrays in > insert_FSM_status_data < ! Returning...")
            return
        if not testNetworkFlowIDsListOfArrays:


            WARNINGmsg = "[insert_FSM_status_data] There's no NetworkFlows for this FSM status change period in the DB, continuing..."
            logger.error(WARNINGmsg)
            PREVIOUS_DICT_FOR_STITCHING = aDict
            continue
        customCompOps_ifFSMalreadyUpdated = ['=', '=', 'is', '>', '<=']
        testNotUpdatedFlowsListOfArrays = execute_select_query(fieldsToSelectAsString="id", tableNameString="networkflow_ts", listOfFieldsToCheckWithWhereClause=['participant', 'iot_device', 'if_FSM_info_set', 'flow_timestamp', 'flow_timestamp'], parametersTuple=(currentUserDBrecordID, testDeviceID, None, current_FSM_status_period_START_ts, current_FSM_status_period_END_ts), useUserDefinedComparisonOperatorsList=True, userDefinedComparisonOperatorsList=customCompOps_ifFSMalreadyUpdated)
        if testNotUpdatedFlowsListOfArrays is None:
            logger.error("[insert_FSM_status_data] Error executing query for 'testNotUpdatedFlowsListOfArrays' in > insert_FSM_status_data < ! Returning...")
            return
        if not testNotUpdatedFlowsListOfArrays:
            warningMSG = "[insert_FSM_status_data] I did not find a Network Flow that would not be updated yet for this time range! Continuing..."
            logger.error(warningMSG)
            PREVIOUS_DICT_FOR_STITCHING = aDict
            continue
        if len(testNetworkFlowIDsListOfArrays) != len(testNotUpdatedFlowsListOfArrays):
            errorMsg = "[insert_FSM_status_data] Some flows in this time range have already been updated about their FSM status!"
            errorMsg += "\nlen(testNetworkFlowIDsListOfArrays) " + str(len(testNetworkFlowIDsListOfArrays)) + " != len(testNotUpdatedFlowsListOfArrays) " + str(len(testNotUpdatedFlowsListOfArrays))
            errorMsg += "\nPREVIOUS: " + str(PREVIOUS_DICT_FOR_STITCHING)
            errorMsg += "\nCURRENT: " + str(aDict)
            errorMsg += "\nReturning..."
            logger.error(errorMsg)
            return
        
        numberOfFlowsToBeUpdatedWithFSMstatus = len(testNetworkFlowIDsListOfArrays)
        totalNumberOfNetworkFlowsToBeUpdated += numberOfFlowsToBeUpdatedWithFSMstatus


        logger.error("[insert_FSM_status_data] Beginning to update [" + str(numberOfFlowsToBeUpdatedWithFSMstatus) + " of " + str(totalNumberOfNetworkFlows) + "] network flow records between '" + str(current_FSM_status_period_START_ts) + "' and '" + str(current_FSM_status_period_END_ts) + "'.")

        recordsUpdatingStartTime = time.time()
        secondsSpentOnSelectQueries = 0.0
        secondsSpentOnInsertQueries = 0.0
        secondsSpentOnUpdateQueries = 0.0

        cacheArrayForNetworkFlowRowsToBeUpdatedAsBelongingToStateTraffic = []
        cacheArrayForNetworkFlowRowsToBeUpdatedAsBelongingToTransitionTraffic = []
        cacheArrayOfInsertsIntoNetworkflowtofsmconditionTable = []
        
        for flowRowID in testNetworkFlowIDsListOfArrays:
            rowID = flowRowID[0]

            the_timezone = pytz.timezone(TIMEZONE_NAME_STRING)
            datetime_ts_of_this_FSM_status_update = datetime.now(the_timezone).strftime('%Y-%m-%d %H"%M:%S.%f')
            datetime_ts_of_this_FSM_status_update = datetime.strptime(datetime_ts_of_this_FSM_status_update, '%Y-%m-%d %H"%M:%S.%f')

            if current_FSM_status_period_is_a_state:
                selectPeriodStartTime = time.time()
                testFSMstate = execute_select_query(fieldsToSelectAsString="id", tableNameString="fsm_state", listOfFieldsToCheckWithWhereClause=['name'], parametersTuple=(CURRENT_fromState, ))
                secondsSpentOnSelectQueries += time.time() - selectPeriodStartTime
                if testFSMstate is None:
                    logger.error("[insert_FSM_status_data] Error executing query for testFSMstate in processing flows belonging to a detected state! No result other than > None < here. Returning...")
                    return
                testFSMstateID = None
                if not testFSMstate:
                    insertPeriodStartTime = time.time()
                    insert_result = execute_insert_query( tableNameString="fsm_state", listOfFieldNamesToBeInsertedAsASingleString="name,state_added_timestamp", tupleOfValuesToBeInserted=(CURRENT_fromState, datetime_ts_of_this_FSM_status_update, ) )
                    secondsSpentOnInsertQueries += time.time() - insertPeriodStartTime
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_FSM_status_data] Error inserting >FSM_state< record for a flow belonging to a state here! :( Quitting")
                        return
                    else:
                        testFSMstateID = insert_result
                elif len(testFSMstate) > 1:
                    logger.error("[insert_FSM_status_data] More than 1 record returned when searching for 'fsm_state' by name! ERROR! Quitting...")
                    return
                else:
                    testFSMstateID = testFSMstate[0][ROW_ID_FIELD_IDX]

                if testFSMstateID is None or testFSMstateID < 0:
                    logger.error("[insert_FSM_status_data] There is a problem with the testFSMstateID value, since it's unexpectedly equal to '" + str(testFSMstateID) + "', abandoning further script execution and quitting...")
                    return
                paramArrayWithAppendedRowID = [current_FSM_status_period_is_a_state, not current_FSM_status_period_is_a_state, testFSMstateID, datetime_ts_of_this_FSM_status_update, True, rowID ]
                cacheArrayForNetworkFlowRowsToBeUpdatedAsBelongingToStateTraffic.append(paramArrayWithAppendedRowID)


            elif PREVIOUS_startingOrFinished == TRANSITION_STARTING_STRING and CURRENT_startingOrFinished == TRANSITION_FINISHED_STRING:
                selectPeriodStartTime = time.time()
                testFSMtransition = execute_select_query(fieldsToSelectAsString="id", tableNameString="fsm_transition", listOfFieldsToCheckWithWhereClause=['name'], parametersTuple=(CURRENT_transitionName, ))
                secondsSpentOnSelectQueries += time.time() - selectPeriodStartTime
                if testFSMtransition is None:
                    logger.error("[insert_FSM_status_data] Error executing query for testFSMtransition for a flow apparently belonging to one! No result other than > None < here. Returning...")
                    return
                testFSMtransitionID = None
                if not testFSMtransition:
                    insertPeriodStartTime = time.time()
                    insert_result = execute_insert_query( tableNameString="fsm_transition", listOfFieldNamesToBeInsertedAsASingleString="name,transition_added_timestamp", tupleOfValuesToBeInserted=(CURRENT_transitionName, datetime_ts_of_this_FSM_status_update, ) )
                    secondsSpentOnInsertQueries += time.time() - insertPeriodStartTime
                    if insert_result == db.FAILED_INSERT_RETURN_VALUE:
                        logger.error("[insert_FSM_status_data] Error inserting >FSM_transition< record while processing a flow belonging to a transition :( Quitting")
                        return
                    else:
                        testFSMtransitionID = insert_result
                else:
                    testFSMtransitionID = testFSMtransition[0][ROW_ID_FIELD_IDX]
                if testFSMtransitionID is None or testFSMtransitionID < 0:
                    logger.error("[insert_FSM_status_data] ERROR: the testFSMtransitionID retrieved in the code processing a flow belonging to a transition has an unexpected and illegal value: '" + str(testFSMtransitionID) + "'! Abandoning further execution and returning now...")
                    return
                
                paramArrayWithAppendedRowID = [current_FSM_status_period_is_a_state, not current_FSM_status_period_is_a_state, testFSMtransitionID, datetime_ts_of_this_FSM_status_update, True, rowID ]
                cacheArrayForNetworkFlowRowsToBeUpdatedAsBelongingToTransitionTraffic.append(paramArrayWithAppendedRowID)

            else:
                errorMsg = ""
                errorMsg += "[insert_FSM_status_data -- TRANSITION or STATE condition] Error: "
                errorMsg += "It seems like the current aDict pair (PREVIOUS + CURRENT) doesn't belong to neither a transition nor a state!"
                errorMsg += "\nPREVIOUS: " + str(PREVIOUS_DICT_FOR_STITCHING)
                errorMsg += "\nCURRENT: " + str(aDict)
                errorMsg += "\n -- Skipping this entry and moving on..."
                logger.error(errorMsg)
                PREVIOUS_DICT_FOR_STITCHING = aDict
                continue
        PREVIOUS_DICT_FOR_STITCHING = aDict

        if cacheArrayForNetworkFlowRowsToBeUpdatedAsBelongingToStateTraffic:
            tableToBeUpdatedNameString = 'networkflow_ts'
            listOfFSMstatusUpdateFields = ['belongs_to_state_traffic', 'belongs_to_transition_traffic', 'flow_state_id', 'record_last_updated', 'if_FSM_info_set']
            updatePeriodStartTime = time.time()
            batchUpdateResult = execute_update_query_for_multiple_records_at_once(tableNameString=tableToBeUpdatedNameString, listOfNamesOfFieldsToBeUpdated=listOfFSMstatusUpdateFields, listOfListsOfNewValuesToBeSetWithUpdatedRowIDappended=cacheArrayForNetworkFlowRowsToBeUpdatedAsBelongingToStateTraffic)
            secondsSpentOnUpdateQueries += time.time() - updatePeriodStartTime
            if batchUpdateResult == db.FAILED_UPDATE_RETURN_VALUE:
                logger.error("[insert_FSM_status_data] Error BATCH-updating networkflow_ts' FSM status records with information about belonging to an FSM > state < traffic :( Quitting...")
                return
            else:
                logger.error("[insert_FSM_status_data] Successfully updated " + str(batchUpdateResult) + " rows belonging to STATES in the 'networkflow_ts' table.")
        else:

            logger.error("[insert_FSM_status_data] No STATE records in FSM status info to be used for updating network flows.")

        if cacheArrayForNetworkFlowRowsToBeUpdatedAsBelongingToTransitionTraffic:
            tableToBeUpdatedNameString = 'networkflow_ts'
            listOfFSMstatusUpdateFields = ['belongs_to_state_traffic', 'belongs_to_transition_traffic', 'flow_transition_id', 'record_last_updated', 'if_FSM_info_set']
            updatePeriodStartTime = time.time()
            batchUpdateResult = execute_update_query_for_multiple_records_at_once(tableNameString=tableToBeUpdatedNameString, listOfNamesOfFieldsToBeUpdated=listOfFSMstatusUpdateFields, listOfListsOfNewValuesToBeSetWithUpdatedRowIDappended=cacheArrayForNetworkFlowRowsToBeUpdatedAsBelongingToTransitionTraffic)
            secondsSpentOnUpdateQueries += time.time() - updatePeriodStartTime
            if batchUpdateResult == db.FAILED_UPDATE_RETURN_VALUE:
                logger.error("[insert_FSM_status_data] Error BATCH-updating networkflow_ts' FSM status records with information about belonging to an FSM > transition < traffic :( Quitting...")
                return
            else:
                logger.error("[insert_FSM_status_data] Successfully updated " + str(batchUpdateResult) + " rows belonging to TRANSITIONS in the 'networkflow_ts' table.")
        else:

            logger.error("[insert_FSM_status_data] No TRANSITION records in FSM status info to be used for updating network flows.")

        if cacheArrayOfInsertsIntoNetworkflowtofsmconditionTable:
            nameOfTableToBeInsertedInto = 'networkflowtofsmcondition'
            listOfFieldsToBeInsertedIntoAsASingleString = "flow_id,condition_id,transition_id,is_condition_met,entry_added_timestamp"
            insertPeriodStartTime = time.time()
            batchInsertResult = execute_insert_query_multiple_inserts_at_once( tableNameString=nameOfTableToBeInsertedInto, listOfFieldNamesToBeInsertedAsASingleString=listOfFieldsToBeInsertedIntoAsASingleString, arrayOfTuplesToBeBatchInsertedAtOnce=cacheArrayOfInsertsIntoNetworkflowtofsmconditionTable )
            secondsSpentOnInsertQueries += time.time() - insertPeriodStartTime
            if batchInsertResult == db.FAILED_INSERT_RETURN_VALUE:
                errorMsg = ""
                errorMsg += "[insert_FSM_status_data] Error batch-inserting into the table > networkflowtofsmcondition <, returning..."
                logger.error(errorMsg)
                return
            else:
                logger.error("[insert_FSM_status_data] Successfully inserted " + str(batchInsertResult) + " rows into the > " + nameOfTableToBeInsertedInto + " < table, moving on...")
        
        timeOfUpdatingAllRecordsWithCurrentFSMstatus = time.time() - recordsUpdatingStartTime

        totalTimeOfRecUpdatesTimeLogString = str(round(timeOfUpdatingAllRecordsWithCurrentFSMstatus, 2))
        secondsSelectingQueryLogString = str(round(secondsSpentOnSelectQueries, 2))
        secondsInsertingQueryLogString = str(round(secondsSpentOnInsertQueries, 2))
        secondsUpdatingQueryLogString = str(round(secondsSpentOnUpdateQueries, 2))
        logger.error("[insert_FSM_status_data] Finished this FSM status update in " + totalTimeOfRecUpdatesTimeLogString + " s (SEL: " + secondsSelectingQueryLogString + " | INS: " + secondsInsertingQueryLogString + " | UPD: " + secondsUpdatingQueryLogString  + ")")


    logger.error("[insert_FSM_status_data] Processed ..:: " + str(totalNumberOfProcessedTransitionPairs) + " ::.. FSM status records pairs")


    if LIST_OF_DICT_WITH_USER_KEY_NOT_IN_DB:
        timezone_here = pytz.timezone(TIMEZONE_NAME_STRING)

        fn_form_timestamp = datetime.now(timezone_here).strftime("%Y-%m-%dT%H-%M-%S.%f")
        output_fn_prefix = "FSMrecordsWUserKeyNotInDB_"
        output_file_extension = "json"
        output_filename = output_fn_prefix + fn_form_timestamp + "." + output_file_extension
        output_file_path = pathlib.Path(__file__).parent.absolute() / NAME_OF_SORTED_FSM_STATUS_RECORDS_DIR / NAME_OF_NO_USER_KEY_RECORDS_DIR / output_filename
        with open(str(output_file_path), 'w') as jsonFileHandle:
            try:
                json.dump(LIST_OF_DICT_WITH_USER_KEY_NOT_IN_DB, jsonFileHandle)
            except Exception as e:
                logger.error("[insert_FSM_status_data] Exception while saving the LIST_OF_DICT_WITH_USER_KEY_NOT_IN_DB:\n" + str(e))
                return
        logger.error("[get_seq_num_ordered_list_of_FSM_records] Successfully saved the list of FSM status records with user_key not found in the DB into the '" + str(output_file_path) + "' file.")
    totalProcessingTimeInSeconds = round(time.time() - FSMstatusDataProcessingSTARTtime, 2)
    logger.error(" Total processing time: " + str(totalProcessingTimeInSeconds) + " seconds.")
    logger.error("Reminder of how the ordering of records went:\n" + FSMrecordsOrderingStatusString)
    logger.error("[insert_FSM_status_data] ########## FSM STATUS UPDATE PROCESS FINISHED ##########")


if __name__== "__main__":

    LIST_OF_CURRENT_NETWORK_FLOW_JSON_FILES = os.listdir(inspectorsPacketInfoJSONdirectory)
    LIST_OF_CURRENT_FSM_INFO_JSON_FILES = os.listdir(FSM_status_data_dir)

    insert_network_flow_data()

    orderingResultAndReportTuple = get_seq_num_ordered_list_of_FSM_records()

    insert_FSM_status_data(orderingResultAndReportTuple)

