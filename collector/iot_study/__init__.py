from flask import request, make_response, jsonify # was in `routes.py`

from flask import Flask
from flask import render_template, send_file, Response
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.interval import IntervalTrigger
from apscheduler.schedulers.base import STATE_RUNNING
import atexit

from bokeh.embed import components

import uuid
import json
import io
import csv
import logging
import time
from datetime import datetime, timedelta

import pandas as pd
import numpy as np

from sklearn.cluster import KMeans, DBSCAN, OPTICS, cluster_optics_dbscan, MiniBatchKMeans
from sklearn.metrics import silhouette_score
from sklearn.mixture import GaussianMixture
from sklearn.neighbors import NearestNeighbors
from sklearn.preprocessing import StandardScaler
import jenkspy

from .plotting import plot_demo_1, plot_nicely, plot_clustering_vis, plot_nicely_ts

from config import Config
from celery import Celery

from rq import Queue
from rq.job import Job
from .flask_bg_worker_rq import redis_queue_connection

db = SQLAlchemy()

redis_queue = Queue(connection=redis_queue_connection)

scheduler = BackgroundScheduler()

BACKEND_JOB_1_INTERVAL_S = 60

DEV_NAME_TO_ID_DICT = {
    'd1': 1,
    'd2': 2,
    'd3': 3,
    'd4': 4
}
celery = Celery(__name__, broker=Config.CELERY_BROKER)

logger = logging.getLogger(__name__)

config_name = 'config.ProdConfig'

def make_celery(app):
    logger.error('[__init__] entering make_celery')
    celery.conf.update(app.config)
    
    TaskBase = celery.Task

    class ContextTask(TaskBase):
        abstract = True

        def __call__(self, *args, **kwargs):
            with app.app_context():
                return TaskBase.__call__(self, *args, **kwargs)
    celery.Task = ContextTask

    return celery

def create_app():
    app = Flask(__name__, instance_relative_config=False)
    app.config.from_object(config_name)

    db.init_app(app)

    migrate = Migrate(app, db)

    with app.app_context():
        from . import routes
        from . import backend

        db.create_all()

        if scheduler.state != STATE_RUNNING:
            scheduler.start()
        atexit.register(lambda: scheduler.shutdown())

        make_celery(app)

        return app

    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    
    return app

