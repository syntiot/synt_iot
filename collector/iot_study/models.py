from . import db
from sqlalchemy.dialects import mysql as sqlalch_mysql_dialect

class IoTvendor(db.Model):

    __tablename__ = 'iotvendor'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=False, unique=True, nullable=False)

    def __repr__(self):
        return '<IoTvendor {}>'.format(self.name)

class IoTdeviceCategory(db.Model):

    __tablename__ = 'iotdevicecategory'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=False, unique=True, nullable=False)

    def __repr__(self):
        return '<IoTdeviceCategory {}>'.format(self.name)
    
class StudyParticipant(db.Model):

    __tablename__ = 'studyparticipant'
    id = db.Column(db.Integer, primary_key=True)
    user_key = db.Column(db.String(32), index=True, unique=True, nullable=False)
    key_creation_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=True, nullable=False)
    signed_consent_form = db.Column(db.Boolean, index=False, unique=False, nullable=False)
    utc_offset_seconds = db.Column(db.Integer)

    def __repr__(self):
        return '<StudyParticipant {}>'.format(self.user_key)

class Device(db.Model):

    __tablename__ = 'device'
    id = db.Column(db.Integer, primary_key=True)
    is_iot = db.Column(db.Boolean, index=False, unique=False, nullable=True)
    name = db.Column(db.String(64), index=False, unique=False, nullable=True)
    device_id = db.Column(db.String(16), index=True, unique=True, nullable=False)
    device_ip = db.Column(db.String(15), index=True, unique=False, nullable=True)
    device_oui = db.Column(db.String(6), index=True, unique=False, nullable=True)
    vendor = db.Column(db.Integer, db.ForeignKey('iotvendor.id'), nullable=True)
    category = db.Column(db.Integer, db.ForeignKey('iotdevicecategory.id'), nullable=True)
    participant = db.Column(db.Integer, db.ForeignKey('studyparticipant.id'), nullable=True)
    dhcp_name = db.Column(db.String(64), index=False, unique=False, nullable=True)
    resolver_ip = db.Column(db.String(15), index=False, unique=False, nullable=True)
    device_type = db.Column(db.String(64), index=False, unique=False, nullable=True)
    added_on_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=True, unique=False, nullable=False)
    ip_updated_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=True)
    category_updated_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=True)
    dhcp_name_updated_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=True)
    resolver_ip_updated_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=True)
    device_type_updated_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=True)

    last_updated = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=True)

    def __repr__(self):
        return '<Device {}>'.format(self.name)

class UserAgent(db.Model):

    __tablename__ = 'useragent'
    id = db.Column(db.Integer, primary_key=True)
    device = db.Column(db.Integer, db.ForeignKey('device.id'), nullable=False)
    user_agent_string = db.Column(db.Text, index=False, unique=False, nullable=False)
    ua_first_seen_ts = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)


class DHCPname(db.Model):

    __tablename__ = 'dhcpname'
    id = db.Column(db.Integer, primary_key=True)
    device = db.Column(db.Integer, db.ForeignKey('device.id'), nullable=False)
    dhcp_hostname = db.Column(db.String(255), index=False, unique=False, nullable=True)
    hostname_first_seen_ts = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)

class DHCPresolverIP(db.Model):

    __tablename__ = 'dhcpresolverip'
    id = db.Column(db.Integer, primary_key=True)
    device = db.Column(db.Integer, db.ForeignKey('device.id'), nullable=False)
    resolver_ip = db.Column(db.String(15), index=False, unique=False, nullable=True)
    resolv_ip_first_seen_ts = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)

class NetdiscoDevName(db.Model):

    __tablename__ = 'netdiscodevname'
    id = db.Column(db.Integer, primary_key=True)
    device = db.Column(db.Integer, db.ForeignKey('device.id'), nullable=False)
    netd_name = db.Column(db.String(128), index=False, unique=False, nullable=True)
    netd_name_first_seen_ts = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)

class NetdiscoDevType(db.Model):

    __tablename__ = 'netdiscodevtype'
    id = db.Column(db.Integer, primary_key=True)
    device = db.Column(db.Integer, db.ForeignKey('device.id'), nullable=False)
    netd_type = db.Column(db.String(128), index=False, unique=False, nullable=True)
    netd_type_first_seen_ts = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)
    
class DeviceState(db.Model):

    __tablename__ = 'devicestate'
    id = db.Column(db.Integer, primary_key=True)
    device_id = db.Column(db.String(16), index=False, unique=True, nullable=False)
    number_of_states = db.Column(db.Integer)
    best_score = db.Column(db.Float)
    
class RemoteEntity(db.Model):
    '''
    Describes the entity contacting the IoT device (sending data to it or receiving the data)
    '''
    __tablename__ = 'remoteentity'
    id = db.Column(db.Integer, primary_key=True)

    ip_address = db.Column(db.String(15), index=True, unique=True, nullable=True)

    secondLevelDomainName = db.Column(db.String(255), index=False, unique=False, nullable=True)

    hostname = db.Column(db.String(15), index=False, unique=True, nullable=True)

    name = db.Column(db.String(64), index=False, unique=True, nullable=True)

    isFirstParty = db.Column(db.Boolean, index=False, unique=False, nullable=True)
    isSupportParty = db.Column(db.Boolean, index=False, unique=False, nullable=True)
    isThirdParty = db.Column(db.Boolean, index=False, unique=False, nullable=True)

class EntitiesContactingIoTdevices(db.Model):
    '''
    Mapping of remote addresess contacted by IoT devices
    '''
    __tablename__ = 'entitiescontactingiotdevices'
    id = db.Column(db.Integer, primary_key=True)
    remote_entity = db.Column(db.Integer, db.ForeignKey('remoteentity.id'))
    iotdevice = db.Column(db.Integer, db.ForeignKey('device.id'))
    resolver_ip = db.Column(db.String(15), index=False, unique=True, nullable=False)
    port_number = db.Column(db.Integer)

class NetworkFlow(db.Model):

    __tablename__ = 'networkflow'
    id = db.Column(db.Integer, primary_key=True)
    iot_device = db.Column(db.Integer, db.ForeignKey('device.id'))
    device_port = db.Column(db.Integer)
    remote_ip_or_hostname = db.Column(db.String(255), index=True, unique=False, nullable=False)
    remote_port = db.Column(db.Integer)
    protocol_used = db.Column(db.String(16), index=False, unique=False, nullable=False)
    bytes_received = db.Column(db.Integer)
    bytes_sent = db.Column(db.Integer)
    who_initiated_communication = db.Column(db.String(10), index=False, unique=False, nullable=True)

    timestamp_of_first_packet = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=True, unique=False, nullable=False)
    participant = db.Column(db.Integer, db.ForeignKey('studyparticipant.id'))
    remote_entity = db.Column(db.Integer, db.ForeignKey('remoteentity.id'))
    is_dns_request = db.Column(db.Boolean, index=False, unique=False, nullable=True)

class NetworkFlowMetaInfo(db.Model):

    __tablename__ = 'networkflowmetainfo'
    id = db.Column(db.Integer, primary_key=True)
    flow_id = db.Column(db.Integer, db.ForeignKey('networkflow.id'))
    flow_frequency = db.Column(db.String(15), index=True, unique=False, nullable=True)
    flow_type = db.Column(db.String(15), index=True, unique=False, nullable=True)
    contacted_organization_name = db.Column(db.String(255), index=False, unique=False, nullable=True)

class NetworkFlow_ts(db.Model):

    __tablename__ = 'networkflow_ts'
    id = db.Column(db.Integer, primary_key=True)
    iot_device = db.Column(db.Integer, db.ForeignKey('device.id'))
    device_port = db.Column(db.Integer)
    remote_ip_or_hostname = db.Column(db.String(255), index=False, unique=False, nullable=False)
    remote_port = db.Column(db.Integer)
    protocol_used = db.Column(db.String(16), index=False, unique=False, nullable=False)
    bytes_received = db.Column(db.Integer)
    bytes_sent = db.Column(db.Integer)
    who_initiated_communication = db.Column(db.String(10), index=False, unique=False, nullable=True)
    timestamp_of_first_packet = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)
    participant = db.Column(db.Integer, db.ForeignKey('studyparticipant.id'))
    remote_entity = db.Column(db.Integer, db.ForeignKey('remoteentity.id'))
    is_dns_request = db.Column(db.Boolean, index=False, unique=False, nullable=True)
    flow_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=True, unique=False, nullable=False)
    belongs_to_state_traffic = db.Column(db.Boolean, index=False, unique=False, nullable=True)
    belongs_to_transition_traffic = db.Column(db.Boolean, index=False, unique=False, nullable=True)
    flow_state_id = db.Column(db.Integer, db.ForeignKey('fsm_state.id'))
    flow_transition_id = db.Column(db.Integer, db.ForeignKey('fsm_transition.id'))
    record_last_updated = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=True, unique=False, nullable=False)
    if_FSM_info_set = db.Column(db.Boolean, index=True, unique=False, nullable=True)

class NetworkFlowToFSMCondition(db.Model):

    __tablename__ = 'networkflowtofsmcondition'
    id = db.Column(db.Integer, primary_key=True)
    flow_id = db.Column(db.Integer, db.ForeignKey('networkflow_ts.id'))
    condition_id = db.Column(db.Integer, db.ForeignKey('fsm_condition.id'))
    transition_id = db.Column(db.Integer, db.ForeignKey('fsm_transition.id'))
    is_condition_met = db.Column(db.Boolean, index=True, unique=False, nullable=True)
    entry_added_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)

class FSM_state(db.Model):

    __tablename__ = 'fsm_state'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True, unique=False, nullable=False)
    state_added_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)
    
class FSM_transition(db.Model):

    __tablename__ = 'fsm_transition'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True, unique=False, nullable=False)
    transition_added_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)
    
class FSM_condition(db.Model):

    __tablename__ = 'fsm_condition'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(128), index=True, unique=False, nullable=False)
    condition_added_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)
    
class DNSdomain(db.Model):

    __tablename__ = 'dnsdomain'
    id = db.Column(db.Integer, primary_key=True)
    domain = db.Column(db.String(64), index=True, unique=False, nullable=False)

    domain_added_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=True)

class DNSresolver(db.Model):

    __tablename__ = 'dnsresolver'
    id = db.Column(db.Integer, primary_key=True)
    resolver_ip = db.Column(db.String(15), index=True, unique=False, nullable=False)
    
class DNSrequest(db.Model):
    
    __tablename__ = 'dnsrequest'
    id = db.Column(db.Integer, primary_key=True)
    iot_device = db.Column(db.Integer, db.ForeignKey('device.id'))
    domain = db.Column(db.Integer, db.ForeignKey('dnsdomain.id'))
    resolver_ip = db.Column(db.Integer, db.ForeignKey('dnsresolver.id'))
    request_timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=True, unique=False, nullable=False)

class DNSrequest_ts(db.Model):
    
    __tablename__ = 'dnsrequest_ts'
    id = db.Column(db.Integer, primary_key=True)
    iot_device = db.Column(db.Integer, db.ForeignKey('device.id'))
    domain = db.Column(db.Integer, db.ForeignKey('dnsdomain.id'))
    resolver_ip = db.Column(db.Integer, db.ForeignKey('dnsresolver.id'))
    timestamp_of_first_packet = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)
    timestamp_of_receiving_last_IP = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)
    
class TLSinfo(db.Model):
    
    __tablename__ = 'tlsinfo'
    id = db.Column(db.Integer, primary_key=True)
    iot_device = db.Column(db.Integer, db.ForeignKey('device.id'))
    timestamp_of_connection_init = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=True, nullable=False)
    tls_version = db.Column(db.String(10), index=False, unique=False, nullable=False)
    cipher_suites = db.Column(db.String(255), index=False, unique=False, nullable=False)
    fingerprint = db.Column(db.String(512), index=False, unique=False, nullable=False)

class TLS_client_hello(db.Model):

    __tablename__ = 'tlsclienthello'
    id = db.Column(db.Integer, primary_key=True)
    iot_device = db.Column(db.Integer, db.ForeignKey('device.id'))
    participant = db.Column(db.Integer, db.ForeignKey('studyparticipant.id'))
    timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)

    tls_version = db.Column(db.String(15), index=False, unique=False, nullable=False)
    cipher_suites = db.Column(db.String(512), index=False, unique=False, nullable=False)
    cipher_suite_uses_grease = db.Column(db.Boolean, index=False, unique=False, nullable=True)
    compression_methods = db.Column(db.String(512), index=False, unique=False, nullable=False)
    extension_types = db.Column(db.String(512), index=False, unique=False, nullable=False)
    extension_details = db.Column(db.String(512), index=False, unique=False, nullable=False)
    extension_uses_grease = db.Column(db.Boolean, index=False, unique=False, nullable=True)
    sni = db.Column(db.String(256), index=False, unique=False, nullable=False)
    remote_ip = db.Column(db.String(15), index=False, unique=False, nullable=True)
    remote_port = db.Column(db.Integer)
    device_ip = db.Column(db.String(15), index=False, unique=False, nullable=True)
    device_port = db.Column(db.Integer)
    client_ts = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)

class TLS_server_hello(db.Model):

    __tablename__ = 'tlsserverhello'
    id = db.Column(db.Integer, primary_key=True)
    iot_device = db.Column(db.Integer, db.ForeignKey('device.id'))
    participant = db.Column(db.Integer, db.ForeignKey('studyparticipant.id'))
    timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)
    version = db.Column(db.String(15), index=False, unique=False, nullable=False)
    cipher_suite = db.Column(db.String(512), index=False, unique=False, nullable=False)
    device_ip = db.Column(db.String(15), index=False, unique=False, nullable=True)
    device_port = db.Column(db.Integer)
    remote_ip = db.Column(db.String(15), index=False, unique=False, nullable=True)
    remote_port = db.Column(db.Integer)
    client_ts =db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)

class TLS_client_cert(db.Model):

    __tablename__ = 'tlsclientcert'
    id = db.Column(db.Integer, primary_key=True)
    iot_device = db.Column(db.Integer, db.ForeignKey('device.id'))
    participant = db.Column(db.Integer, db.ForeignKey('studyparticipant.id'))
    timestamp = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)                              
    pubkey = db.Column(db.String(1024), index=False, unique=False, nullable=False)
    signature = db.Column(db.String(512), index=False, unique=False, nullable=False)
    cert_key_hash = db.Column(db.String(512), index=False, unique=False, nullable=False)
    remote_ip = db.Column(db.String(15), index=False, unique=False, nullable=True)
    remote_port = db.Column(db.Integer)
    device_ip = db.Column(db.String(15), index=False, unique=False, nullable=True)
    device_port = db.Column(db.Integer)
    client_ts = db.Column(sqlalch_mysql_dialect.DATETIME(fsp=6), index=False, unique=False, nullable=False)
    
class InferredActivity(db.Model):

    __tablename__ = 'inferredactivity'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), index=False, unique=True, nullable=False)

class InferenceRule(db.Model):

    __tablename__ = 'inferencerule'
    id = db.Column(db.Integer, primary_key=True)
    rule_body = db.Column(db.String(128), index=False, unique=True, nullable=False)
    
class Inference(db.Model):

    __tablename__ = 'inference'
    id = db.Column(db.Integer, primary_key=True)
    inferred_activity = db.Column(db.Integer, db.ForeignKey('inferredactivity.id'))
    inference_rule = db.Column(db.Integer, db.ForeignKey('inferencerule.id'))

class ESquestion(db.Model):

    __tablename__ = 'esquestion'
    id = db.Column(db.Integer, primary_key=True)
    question = db.Column(db.String(128), index=False, unique=True, nullable=False)
    responseIndex = db.Column(db.Integer)

class ESresponse(db.Model):

    __tablename__ = 'esresponse'
    id = db.Column(db.Integer, primary_key=True)
    responseIndex = db.Column(db.Integer, primary_key=True)
    responseText = db.Column(db.String(128), index=False, unique=True, nullable=False)

class ExperienceSample(db.Model):

    __tablename__ = 'experiencesample'
    id = db.Column(db.Integer, primary_key=True)
    participant = db.Column(db.Integer, db.ForeignKey('studyparticipant.id'))
    Q1 = db.Column(db.Integer, db.ForeignKey('esquestion.id'))
    responseToQ1 = db.Column(db.Integer, db.ForeignKey('esresponse.id'))

