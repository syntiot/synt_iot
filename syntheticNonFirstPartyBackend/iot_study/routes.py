from flask import Flask
app = Flask(__name__)

import logging

logger = logging.getLogger(__name__)

@app.route('/yourTestEndpointName')
def hello_world_test():
    return "Congrats, hello from the Simulation backend!"

@app.route('/behavior_simulation/submit_a_phony_file/<no_of_KBs_to_be_returned>', methods = ['POST'])
def received_and_discard_file(no_of_KBs_to_be_returned):
    if request.method == 'POST':
        file = request.files['file']
        returnThisManyKBs = float(no_of_KBs_to_be_returned)
        if returnThisManyKBs > 0:
            tempFile = make_a_phony_file(returnThisManyKBs)
            response = make_response(tempFile)
            response.headers.set('Content-Type', 'image/jpeg')
            logger.error('[rec_and_disc_file] response with requested file of size ' + str(returnThisManyKBs) + ' KB')
            return response
        logger.error('[ROUTES] returning positive response for the phony file upload...')
        return make_response("Received and discarded, thanks!", 200)

def make_a_phony_file(desiredSizeInKBs):
    desiredSizeInBytes = desiredSizeInKBs * 1024
    data = "d" * (int(desiredSizeInBytes / 2) - 1)
    arr = bytearray(data, "utf-16")
    return arr
    
