#!/usr/bin/python3
import os.path
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import logging

logging.basicConfig(stream=sys.stderr)

sys.path.insert(0, '~/FlaskDeploymentDir/other-party-backend')
sys.path.insert(0, '~/FlaskDeploymentDir/other-party-backend/iot_study')

from iot_study.routes import app as application
application.secret_key = 'yourSecretKeyBetterKeptOutside'
