import logging

import cons_defs as cdefs
from SimulatedIoTdevice import *

# Make the logger timezone aware
amsTZ = pytz.timezone('Europe/Amsterdam')
def customTimeZoneConverter(*args):
    utc_dt = pytz.utc.localize(datetime.datetime.utcnow())
    converted = utc_dt.astimezone(amsTZ)
    return converted.timetuple()
FORMAT = '[%(asctime)s] - [%(levelname)-5s] - [%(funcName)s] - %(message)s'
#
log_ts = datetime.datetime.now(amsTZ).strftime("%d%m%Y-%H%M%S%z")
logging.basicConfig(filename='log_files/testLog_' + str(log_ts) + '.log', filemode='a',level=logging.INFO, format=FORMAT) # saving to file
logging.Formatter.converter = customTimeZoneConverter
logging.getLogger('transitions').setLevel(logging.ERROR) #get rid of WARNING level logs of impossible transitions that occlude the interesting information

# Define the test case's FSM
'''
# Loaded from the Synthetic device definition file as cdefs.FSM_states, where also the initial state lives (as cdefs.FSM_initial_state)
states = ['off', 'booting_up', 'online_idle', 'offline_idle',
              'synchronizing_time', 'checking_for_updates',
              'contacting_1st_party_API', 'contacting_support_party_CDN', 'contacting_3rd_party',
              'receiving_user_request', 'responding_to_user_request'
    ]
'''
 # At the moment, we simplify the test model by assuming the following:
 # a1. the device will always be online (no interruptions of internet connection)
 # a2. the device will always contact the 'cloud' before being able to respond to user request (no locally cached competences/knowledge)
 # a3. the vendor hosts the core service either in their own domain or using an external hosting provider (Content Delivery Network)
 # a4. there is no queuing of connection requests (if there's no internet connection when it's needed, there will be no save & re-try)
 # a5. there is no power outages to handle by the FSM (at the moment of writing it this doesn't seem to affect functionality but should be kept in mind)
'''
# Loaded from the Synthetic device definition file as cdefs.FSM_transitions
transitions = [
    {'trigger': 'powered_up', 'source': 'off', 'dest': 'booting_up', 'before': 'initialize_boot_process', 'after': 'finalize_boot_process'},
    {'trigger': 'done_booting', 'source': 'booting_up', 'dest': 'offline_idle', 'conditions': ['boot_process_completed', 'internet_connection_unavailable']},
    {'trigger': 'done_booting', 'source': 'booting_up', 'dest': 'online_idle', 'conditions': ['boot_process_completed', 'internet_connection_available']},
    {'trigger': 'internet_connection_restored', 'source': 'offline_idle', 'dest': 'online_idle', 'conditions': 'internet_connection_available'},
    {'trigger': 'internet_connection_lost', 'source': ['online_idle', 'contacting_1st_party_API', 'contacting_support_party_CDN', 'contacting_3rd_party'], 'dest': 'offline_idle', 'conditions': ['internet_connection_unavailable', 'boot_process_completed']},
    {'trigger': 'internet_connection_lost_while_booting', 'source': ['contacting_1st_party_API', 'contacting_support_party_CDN', 'contacting_3rd_party'], 'dest': 'booting_up', 'conditions': ['internet_connection_unavailable', 'boot_process_in_progress']},
    {'trigger': 'time_sync_triggered', 'source': ['booting_up', 'online_idle'], 'dest': 'synchronizing_time', 'conditions': ['internet_connection_available', 'time_for_time_sync'], 'before': 'sync_time_with_ntp_server'},
    {'trigger': 'check_for_updates_triggered', 'source': ['booting_up', 'online_idle'], 'dest': 'checking_for_updates', 'conditions': ['internet_connection_available', 'time_for_updates'], 'before': 'check_for_updates'},
    {'trigger': 'heartbeat_triggered', 'source': ['booting_up', 'online_idle'], 'dest': '=', 'conditions': ['is_it_time_for_a_heartbeat', 'internet_connection_available'], 'after': 'send_a_heartbeat_home'},
    {'trigger': 'detected_user_request', 'source': ['online_idle', 'offline_idle'], 'dest': 'receiving_user_request', 'conditions': ['boot_process_completed', 'user_request_issued'], 'after': 'update_list_of_interactions_with_device'},
    {'trigger': 'done_interpreting_user_request', 'source': 'receiving_user_request', 'dest': 'contacting_1st_party_API', 'conditions': ['internet_connection_available', 'understood_what_user_wants', 'hosts_service_API_itself'], 'before': 'connect_to_1st_party'},
    {'trigger': 'done_interpreting_user_request', 'source': 'receiving_user_request', 'dest': 'contacting_support_party_CDN', 'conditions': ['internet_connection_available', 'understood_what_user_wants', 'hosts_service_API_externally']},
    {'trigger': 'done_interpreting_user_request', 'source': 'receiving_user_request', 'dest': 'responding_to_user_request', 'conditions': ['understood_what_user_wants', 'internet_connection_unavailable']},
    {'trigger': 'response_from_the_cloud_brain', 'source': 'contacting_1st_party_API', 'dest': 'responding_to_user_request', 'conditions': ['internet_connection_available', 'boot_process_completed', 'serving_user_request_now', 'cloud_brain_responded']},
    {'trigger': 'response_from_the_cloud_brain', 'source': 'contacting_support_party_CDN', 'dest': 'responding_to_user_request', 'conditions': ['boot_process_completed', 'internet_connection_available', 'boot_process_completed', 'serving_user_request_now', 'cloud_brain_responded']},
    {'trigger': 'done_responding_to_user_request', 'source': 'responding_to_user_request', 'dest': 'online_idle', 'conditions': ['internet_connection_available', 'boot_process_completed', 'done_responding_to_the_user']},
    {'trigger': 'done_responding_to_user_request', 'source': 'responding_to_user_request', 'dest': 'offline_idle', 'conditions': ['internet_connection_unavailable', 'boot_process_completed', 'done_responding_to_the_user']},
   {'trigger': 'tracking_triggered', 'source': 'booting_up', 'dest': 'contacting_1st_party_API', 'conditions': ['internet_connection_available', 'hosts_service_API_itself'], 'before': 'connect_to_1st_party'},
    {'trigger': 'tracking_triggered', 'source': 'booting_up', 'dest': 'contacting_support_party_CDN', 'conditions': ['internet_connection_available', 'no_support_party_CDN_connections_yet'], 'before': 'connect_to_support_party_CDN'},
    {'trigger': 'tracking_triggered', 'source': 'contacting_1st_party_API', 'dest': 'contacting_support_party_CDN', 'conditions': ['internet_connection_available'], 'before': 'connect_to_support_party_CDN'},
    {'trigger': 'tracking_triggered', 'source': 'booting_up', 'dest': 'contacting_3rd_party', 'conditions': ['internet_connection_available', 'no_3rd_party_connections_yet'], 'before': 'connect_to_3rd_party'},
    {'trigger': 'tracking_triggered', 'source': 'contacting_1st_party_API', 'dest': 'contacting_3rd_party', 'conditions': 'internet_connection_available', 'before': 'connect_to_3rd_party'},
    {'trigger': 'tracking_triggered', 'source': ['booting_up', 'contacting_1st_party_API'], 'dest': 'offline_idle', 'conditions': ['internet_connection_unavailable', 'boot_process_completed']},
    {'trigger': 'tracking_info_sent', 'source': 'contacting_1st_party_API', 'dest': 'booting_up', 'conditions': 'boot_process_in_progress'},
    {'trigger': 'tracking_info_sent', 'source': ['contacting_1st_party_API', 'contacting_support_party_CDN', 'contacting_3rd_party'], 'dest': 'online_idle', 'conditions': ['internet_connection_available', 'boot_process_completed']},
    {'trigger': 'tracking_info_sent', 'source': 'contacting_3rd_party', 'dest': 'offline_idle', 'conditions': ['internet_connection_unavailable', 'boot_process_completed']},
    {'trigger': 'tracking_info_sent', 'source': 'contacting_3rd_party', 'dest': 'booting_up', 'conditions': 'boot_process_in_progress'},
    {'trigger': 'tracking_info_sent', 'source': 'contacting_support_party_CDN', 'dest': 'booting_up', 'conditions': 'boot_process_in_progress'},
    {'trigger': 'done_checking_for_updates', 'source': 'checking_for_updates', 'dest': 'booting_up', 'conditions': 'boot_process_in_progress'},
    {'trigger': 'done_checking_for_updates', 'source': 'checking_for_updates', 'dest': 'online_idle', 'conditions': ['boot_process_completed', 'internet_connection_available']},
    {'trigger': 'done_checking_for_updates', 'source': 'checking_for_updates', 'dest': 'offline_idle', 'conditions': ['boot_process_completed', 'internet_connection_unavailable']},
    {'trigger': 'done_synchronizing_time', 'source': 'synchronizing_time', 'dest': 'booting_up', 'conditions': 'boot_process_in_progress'},
    {'trigger': 'done_synchronizing_time', 'source': 'synchronizing_time', 'dest': 'online_idle', 'conditions': ['boot_process_completed', 'internet_connection_available']},
    {'trigger': 'done_synchronizing_time', 'source': 'synchronizing_time', 'dest': 'offline_idle', 'conditions': ['boot_process_completed', 'internet_connection_unavailable']}
    ] # format: trigger-event, source-state, destination-state, listOfConditions [which need to be implemented as method of the Simulation class]
'''
#print("len(transitions): " + str(len(transitions)))
#conditions = [
#    ] # WARNING! All have to be implemented as methods in the `SimulatedIoTdevice class` ! -> added to transition conditions
FSM_ignore_invalid_triggers = True
FSM_show_conditions = False # Make the graph of the FSM smaller for the sake of sharing visualization
FSM_auto_transitions = False
FSM_show_auto_transitions = False
FSM_show_state_attributes = True
FSM_before_state_change_callback_name = 'execute_before_each_state_change'
FSM_after_state_change_callback_name = 'execute_after_each_state_change'
FSM_send_event = True # For being able to access the EventData structure that is supposed to hold information about current state, trigger/transition name

FSM1 = FSMdefinition.FSMdefinition(cdefs.FSM_states, cdefs.FSM_initial_state, cdefs.FSM_transitions, FSM_ignore_invalid_triggers, FSM_auto_transitions, FSM_show_conditions, FSM_show_auto_transitions, FSM_show_state_attributes, FSM_before_state_change_callback_name, FSM_after_state_change_callback_name, FSM_send_event)
# Define the test case's device properties

emulatedDeviceSpecs1 = EmulatedDeviceInfo.EmulatedDeviceInfo(
    devType = cdefs.DeviceTypes.VOICE_ASSISTANT,
    name = 'testEcho1',
    IPaddress = '192.168.999.999',
    OUI = 'AA:22:B4',
    resolverIP = '8.8.8.8',
    userAgent = 'Abrowser2.0',
    encryptsCommunication = True,
    usesCertPinning = False,
    TLSversionUsed = '1.2',
    cipherSuitesUsed = '',
    maxDataPackageSizeInMB = cdefs.DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[cdefs.DeviceTypes.VOICE_ASSISTANT],
    maxFirmwareUpdateSizeInMB = 5,
    connTypeToFreq = {cdefs.ConnectionTypes.CORE_SERVICE_API: cdefs.ConnectionFrequencies.HIGH,
                            cdefs.ConnectionTypes.CDN_RESOURCE: cdefs.ConnectionFrequencies.MEDIUM,
                            cdefs.ConnectionTypes.TRACKING_ADS: cdefs.ConnectionFrequencies.NONE,
                            cdefs.ConnectionTypes.ANALYTICS: cdefs.ConnectionFrequencies.NONE,
                            cdefs.ConnectionTypes.TIME_SYNC_NTP: cdefs.ConnectionFrequencies.LOW,
                            cdefs.ConnectionTypes.CHECK_FOR_UPDATES: cdefs.ConnectionFrequencies.LOW,
                          },
    bootProcessTimeInSeconds = cdefs.BOOT_TIME_IN_S,
    preferredNTPservers = [cdefs.BASE_URL_NTP_SERVICE_0],
    primary1stPartyServers = cdefs.ENTITY_TO_URL[cdefs.ContactedEntities.FIRST_PARTY]
    )

if __name__ == '__main__':
    # Instantiate the simulated device
    d1 = SimulatedIoTdevice(FSM1, emulatedDeviceSpecs1)
    #logger.info('Powering it up...')
    d1.powered_up() # Turn the device on
    logging.info("\n\nLOOPING NOW (by actively attempting to trigger all possible transitions)\n")

    while (not d1.is_off()):

        d1.done_booting()
        d1.internet_connection_lost()
        d1.internet_connection_restored()
        d1.time_sync_triggered()
        d1.check_for_updates_triggered()
        d1.detected_user_request()
        d1.done_interpreting_user_request()
        d1.response_from_the_cloud_brain()
        d1.done_responding_to_user_request()
        d1.tracking_triggered()
        d1.tracking_info_sent()
        d1.done_checking_for_updates()
        d1.done_synchronizing_time()
        d1.heartbeat_triggered()
    print("Machine DONE, turning off...")

