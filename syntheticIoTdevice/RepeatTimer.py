""" Simply run function fn passed as a parameter every n seconds
    after right2clicky's answer here:
    https://stackoverflow.com/questions/12435211/python-threading-timer-repeat-function-every-n-seconds
"""
from threading import Timer

class RepeatTimer(Timer):
    def run(self):
        while not self.finished.wait(self.interval):
            self.function(*self.args, **self.kwargs)
