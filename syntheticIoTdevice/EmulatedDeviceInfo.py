class EmulatedDeviceInfo:

    def __init__(self,devType,name,IPaddress,OUI,resolverIP,userAgent,
                 encryptsCommunication,usesCertPinning,TLSversionUsed,cipherSuitesUsed,
                 maxDataPackageSizeInMB, maxFirmwareUpdateSizeInMB, connTypeToFreq, bootProcessTimeInSeconds,
                     preferredNTPservers, primary1stPartyServers):
        self.devType = devType
        self.name = name
        self.IPaddress = IPaddress
        self.OUI = OUI
        self.resolverIP = resolverIP
        self.userAgent = userAgent
        self.encryptsCommunication = encryptsCommunication
        self.usesCertPinning = usesCertPinning
        self.TLSversionUsed = TLSversionUsed
        self.cipherSuitesUsed = cipherSuitesUsed
        self.maxDataPackageSizeInMB = maxDataPackageSizeInMB
        self.maxFirmwareUpdateSizeInMB = maxFirmwareUpdateSizeInMB
        self.connTypeToFreq = connTypeToFreq # A map of frequency with which the device connects to x-parties
        self.bootProcessTimeInSeconds = bootProcessTimeInSeconds
        self.preferredNTPservers = preferredNTPservers
        self.primary1stPartyServers = primary1stPartyServers
