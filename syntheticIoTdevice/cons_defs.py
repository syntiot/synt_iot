import math
import enum
import json
import sys
'''
Definitions of the constants and auxiliary data structures
'''
# Quick and easy way of changing the profile of device behavior
TEST_PARAM_CONFIG_FILENAME = 'resources/paramConfigFiles/testParamConfig.json'
PROD_PARAM_CONFIG_FILENAME = 'resources/paramConfigFiles/prodParamConfig.json'
PARAM_CONFIG_FILE_NAME = PROD_PARAM_CONFIG_FILENAME
# Default values of optional parameters
DEFAULT_NO_OF_SLICES_PER_USE_PERIOD = 10
DEFAULT_NUMBER_OF_TIME_SLICES_TO_WAIT_BEFORE_FIRST_USER_INTERACTION = 1
DEFAULT_NO_OF_SUB_SLICES_PER_TIME_PERIOD = 25
DEFAULT_BOOT_TIME_IN_S = 55
DEFAULT_BOOT_TIME_IN_S_DEBUGGING = 5
DEFAULT_FSM_states = ["off", "booting_up", "online_idle", "offline_idle", "synchronizing_time", "checking_for_updates", "contacting_1st_party_API", "contacting_support_party_CDN", "contacting_3rd_party", "receiving_user_request", "responding_to_user_request"]
DEFAULT_FSM_initial_state = "off"
DEFAULT_FSM_transitions = [
    {'trigger': 'powered_up', 'source': 'off', 'dest': 'booting_up', 'before': 'initialize_boot_process', 'after': 'finalize_boot_process'},
    {'trigger': 'done_booting', 'source': 'booting_up', 'dest': 'offline_idle', 'conditions': ['boot_process_completed', 'internet_connection_unavailable']},
    {'trigger': 'done_booting', 'source': 'booting_up', 'dest': 'online_idle', 'conditions': ['boot_process_completed', 'internet_connection_available']},
    {'trigger': 'internet_connection_restored', 'source': 'offline_idle', 'dest': 'online_idle', 'conditions': 'internet_connection_available'},
    {'trigger': 'internet_connection_lost', 'source': ['online_idle', 'contacting_1st_party_API', 'contacting_support_party_CDN', 'contacting_3rd_party'], 'dest': 'offline_idle', 'conditions': ['internet_connection_unavailable', 'boot_process_completed']},
    {'trigger': 'internet_connection_lost_while_booting', 'source': ['contacting_1st_party_API', 'contacting_support_party_CDN', 'contacting_3rd_party'], 'dest': 'booting_up', 'conditions': ['internet_connection_unavailable', 'boot_process_in_progress']},
    {'trigger': 'time_sync_triggered', 'source': ['booting_up', 'online_idle'], 'dest': 'synchronizing_time', 'conditions': ['internet_connection_available', 'time_for_time_sync'], 'before': 'sync_time_with_ntp_server'},
    {'trigger': 'check_for_updates_triggered', 'source': ['booting_up', 'online_idle'], 'dest': 'checking_for_updates', 'conditions': ['internet_connection_available', 'time_for_updates'], 'before': 'check_for_updates'},
    {'trigger': 'heartbeat_triggered', 'source': ['booting_up', 'online_idle'], 'dest': '=', 'conditions': ['is_it_time_for_a_heartbeat', 'internet_connection_available'], 'after': 'send_a_heartbeat_home'},
    {'trigger': 'detected_user_request', 'source': ['online_idle', 'offline_idle'], 'dest': 'receiving_user_request', 'conditions': ['boot_process_completed', 'user_request_issued'], 'after': 'update_list_of_interactions_with_device'},
    {'trigger': 'done_interpreting_user_request', 'source': 'receiving_user_request', 'dest': 'contacting_1st_party_API', 'conditions': ['internet_connection_available', 'understood_what_user_wants', 'hosts_service_API_itself'], 'before': 'connect_to_1st_party'},
    {'trigger': 'done_interpreting_user_request', 'source': 'receiving_user_request', 'dest': 'contacting_support_party_CDN', 'conditions': ['internet_connection_available', 'understood_what_user_wants', 'hosts_service_API_externally']},
    {'trigger': 'done_interpreting_user_request', 'source': 'receiving_user_request', 'dest': 'responding_to_user_request', 'conditions': ['understood_what_user_wants', 'internet_connection_unavailable']},
    {'trigger': 'response_from_the_cloud_brain', 'source': 'contacting_1st_party_API', 'dest': 'responding_to_user_request', 'conditions': ['internet_connection_available', 'boot_process_completed', 'serving_user_request_now', 'cloud_brain_responded']},
    {'trigger': 'response_from_the_cloud_brain', 'source': 'contacting_support_party_CDN', 'dest': 'responding_to_user_request', 'conditions': ['boot_process_completed', 'internet_connection_available', 'boot_process_completed', 'serving_user_request_now', 'cloud_brain_responded']},
    {'trigger': 'done_responding_to_user_request', 'source': 'responding_to_user_request', 'dest': 'online_idle', 'conditions': ['internet_connection_available', 'boot_process_completed', 'done_responding_to_the_user']},
    {'trigger': 'done_responding_to_user_request', 'source': 'responding_to_user_request', 'dest': 'offline_idle', 'conditions': ['internet_connection_unavailable', 'boot_process_completed', 'done_responding_to_the_user']},
   {'trigger': 'tracking_triggered', 'source': 'booting_up', 'dest': 'contacting_1st_party_API', 'conditions': ['internet_connection_available', 'hosts_service_API_itself'], 'before': 'connect_to_1st_party'},
    {'trigger': 'tracking_triggered', 'source': 'booting_up', 'dest': 'contacting_support_party_CDN', 'conditions': ['internet_connection_available', 'no_support_party_CDN_connections_yet'], 'before': 'connect_to_support_party_CDN'},
    {'trigger': 'tracking_triggered', 'source': 'contacting_1st_party_API', 'dest': 'contacting_support_party_CDN', 'conditions': ['internet_connection_available'], 'before': 'connect_to_support_party_CDN'},
    {'trigger': 'tracking_triggered', 'source': 'booting_up', 'dest': 'contacting_3rd_party', 'conditions': ['internet_connection_available', 'no_3rd_party_connections_yet'], 'before': 'connect_to_3rd_party'},
    {'trigger': 'tracking_triggered', 'source': 'contacting_1st_party_API', 'dest': 'contacting_3rd_party', 'conditions': 'internet_connection_available', 'before': 'connect_to_3rd_party'},
    {'trigger': 'tracking_triggered', 'source': ['booting_up', 'contacting_1st_party_API'], 'dest': 'offline_idle', 'conditions': ['internet_connection_unavailable', 'boot_process_completed']},
    {'trigger': 'tracking_info_sent', 'source': 'contacting_1st_party_API', 'dest': 'booting_up', 'conditions': 'boot_process_in_progress'},
    {'trigger': 'tracking_info_sent', 'source': ['contacting_1st_party_API', 'contacting_support_party_CDN', 'contacting_3rd_party'], 'dest': 'online_idle', 'conditions': ['internet_connection_available', 'boot_process_completed']},
    {'trigger': 'tracking_info_sent', 'source': 'contacting_3rd_party', 'dest': 'offline_idle', 'conditions': ['internet_connection_unavailable', 'boot_process_completed']},
    {'trigger': 'tracking_info_sent', 'source': 'contacting_3rd_party', 'dest': 'booting_up', 'conditions': 'boot_process_in_progress'},
    {'trigger': 'tracking_info_sent', 'source': 'contacting_support_party_CDN', 'dest': 'booting_up', 'conditions': 'boot_process_in_progress'},
    {'trigger': 'done_checking_for_updates', 'source': 'checking_for_updates', 'dest': 'booting_up', 'conditions': 'boot_process_in_progress'},
    {'trigger': 'done_checking_for_updates', 'source': 'checking_for_updates', 'dest': 'online_idle', 'conditions': ['boot_process_completed', 'internet_connection_available']},
    {'trigger': 'done_checking_for_updates', 'source': 'checking_for_updates', 'dest': 'offline_idle', 'conditions': ['boot_process_completed', 'internet_connection_unavailable']},
    {'trigger': 'done_synchronizing_time', 'source': 'synchronizing_time', 'dest': 'booting_up', 'conditions': 'boot_process_in_progress'},
    {'trigger': 'done_synchronizing_time', 'source': 'synchronizing_time', 'dest': 'online_idle', 'conditions': ['boot_process_completed', 'internet_connection_available']},
    {'trigger': 'done_synchronizing_time', 'source': 'synchronizing_time', 'dest': 'offline_idle', 'conditions': ['boot_process_completed', 'internet_connection_unavailable']}
]
# Declaring the data structures to be filled in with parameters from the config file specified above
NO_OF_TIME_SLICES_PER_USE_PERIOD = {}
## User specification
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS = {} # in minutes, tuple giving a range (min, max)
DEVICE_USE_PERIOD_HOURS_RANGE = {}
## Device specification
DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB = {} # Values sent based on non-timestmaped data collected by the Inspector between Dec 16th 2019 and Jan 29th 2020
DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB = {}
HEARTBEAT_FREQ_PER_DEVICE_IN_S = {} # Based on observed shortest times between traffic peaks of devices sniffed with the inspector
UPDATE_MIN_FREQUENCIES_IN_DAYS = {}
## FSM definition
FSM_states = []
FSM_initial_state = ''
FSM_transitions = []
#### ENUMS
class TransitionTriggeringEvents(enum.Enum):
    POWER_ON = 1
    BOOT_SEQUENCE_DONE = 2
    USER_INTERACTION = 3
class TransitionConditions(enum.Enum):
    INTERNET_CONNECTION_AVAILABLE = 1
    INTERNET_CONNECTION_UNAVAILABLE = 2
    TIME_TO_SYNCHRONIZE_TIME = 3
    TIME_TO_CHECK_FOR_UPDATES = 4
class TestDevices(enum.Enum):
    ECHO_1 = 1
    GOOGLE_HOME_1 = 6
    HUE_BRIDGE_1 = 11
    LIGHTIFY_GATEWAY_1 = 16
# Types of connections IoT devices make (and that our synthetic one will strive to emulate)
class ConnectionTypes(enum.Enum):
    OTHER = 0 # for all unclassified connection types, not used yet
    CORE_SERVICE_API = 1
    CDN_RESOURCE = 2
    TRACKING_ADS = 3
    ANALYTICS = 4
    TIME_SYNC_NTP = 5
    CHECK_FOR_UPDATES = 6
class ContactedEntities(enum.Enum):
    FIRST_PARTY = 1
    SUPPORT_PARTY = 2
    THIRD_PARTY = 3
    NTP_SERVICE = 4
# Device categories, after IMC'19 paper ('Information Exposure From Consumer IoT Devices: A multidimensional, network-informed measurement approach')
class DeviceCategories(enum.Enum):
    CAMERA = 1 # Amazon cloudcam, Amcrest cam, Blink, D-Link, Ring doorbel, WiMaker Spy camera, Xiaomi Cam etc.
    SMART_HUB = 2 # Insteon, Ligthtify, Hue, Sengled, SmartThings, Wink (2), Xiaomi
    HOME_AUTOMATION = 3 # movement sensor, lightbulbs, thermostats, plugs
    TV = 4 # Apple/Fire/LG TV, Roku, Samsung
    AUDIO = 5 # Echos, Google Homes
    APPLIANCE = 6 # Anova sousvide, brewer, microwave (e.g. GE's), weather station, Dryer, Fridge, washer, iKettle, cleaner, rice cooker
# Device types (NOTE: more specific than categories)
'''
# Constructed at runtime based on the config file
class DeviceTypes(enum.Enum):
    VOICE_ASSISTANT = 1
    LIGHTBULB_HUB_GATEWAY = 2
    THERMOSTAT = 3
    VIDEO_DOORBELL = 4
    SECURITY_CAMERA = 5 # could be indoor, outdoor or both
    MOVEMENT_SENSOR = 6
    PLUG = 7
    STREAMING_AV = 8 # streaming Audio/Video, e.g. Roku, smart TV
'''
# Most freq connections per observed devices: (Hue, 2mins), (Lightify, 30mins), (Echo,5mins|40s), (GHomeMini, 1 min)
class ConnectionFrequencies(enum.Enum):
    NONE = 0
    HIGH = 1
    MEDIUM = 2
    LOW = 3
    VERY_LOW = 4
    EXTREMELY_LOW = 5
# Time of the day (used to decide e.g. how often a user might interact with the device during this time span)
'''
# Constructed at runtime based on the config file
class TimesOfTheDay(enum.Enum):
    NIGHT = 0 # when we assume everyone is typically sleeping, say ~23.59-6
    EARLY_MORNING = 1 # when people typically get up early to prepare and leave for work/school, ~6-9
    MORNING = 2 # 'later' morning, i.e. after one would leave for work and before lunchtime, say 9-11.30
    LUNCHTIME = 3 # ~11.30-13.30
    AFTERNOON = 4 # ~13.30-17.30
    EVENING = 5 # ~17.30-20.30
    LATE_EVENING = 6 # ~20.30-23.59
'''
class Weekdays(enum.Enum):
    MONDAY = 0 # NOTE! Indexing MUST correspond to the indexing of Python's datetime.date.weekday() function, since it's used as such in `SimulatedIoTuser`
    TUESDAY = 1
    WEDNESDAY = 2
    THURSDAY = 3
    FRIDAY = 4
    SATURDAY = 5
    SUNDAY = 6
# Reading parameter values from a config JSON file specified above
paramsDict = {}
# Read the entire parametrized config file
with open(PARAM_CONFIG_FILE_NAME) as fh:
    paramsDict = json.load(fh)
# Construct user-defined Enums at runtime
## DeviceTypes
userDefinedDeviceTypes = paramsDict['DeviceTypes']
tempUserDefinedDeviceTypesDict = {name: index for index, name in enumerate(userDefinedDeviceTypes)}
DeviceTypes = enum.Enum('DeviceTypes', tempUserDefinedDeviceTypesDict)
# TimesOfTheDay
userDefinedTimesOfTheDay = paramsDict['TimesOfTheDay']
tempTimesOfTheDayDict = {name: index for index, name in enumerate(userDefinedTimesOfTheDay)}
TimesOfTheDay = enum.Enum('TimesOfTheDay', tempTimesOfTheDayDict)
# Read the dictionaries
for entryArray in paramsDict['DEVICE_USE_PERIOD_HOURS_RANGE']:
    dictKey = entryArray[0] # These are strings at this point
    dictValue = entryArray[1]
    listOfTwoFloatValues = eval(dictValue)
    listOfTwoStringValues = [str(val) for val in listOfTwoFloatValues]
    DEVICE_USE_PERIOD_HOURS_RANGE[eval(dictKey)] = tuple(listOfTwoStringValues) # MUST REMAIN STRING at this point
for entryArray in paramsDict['EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS']:
    dictKey = entryArray[0] #json.loads(entryArray[0]) # These are strings at this point
    dictValue = entryArray[1] # json.loads(entryArray[1])
    #minMaxNumericalArray = eval(dictValue)
    #minMaxStringArray = [ str(num) for num in minMaxNumericalArray ]
    #print('entryArray[0]: ' + str(entryArray[0]) + ' | entryArray[1]: ' + str(entryArray[1]))
    EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(eval(dictKey))] = tuple(eval(dictValue))
for entryArray in paramsDict['DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB']:
    dictKey = entryArray[0] # These are strings at this point
    dictValue = entryArray[1]
    DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[eval(dictKey)] = int(dictValue)
for entryArray in paramsDict['DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB']:
    dictKey = entryArray[0] # These are strings at this point
    dictValue = entryArray[1]
    DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[eval(dictKey)] = int(dictValue)
for entryArray in paramsDict['HEARTBEAT_FREQ_PER_DEVICE_IN_S']:
    dictKey = entryArray[0] # These are strings at this point
    dictValue = entryArray[1]
    HEARTBEAT_FREQ_PER_DEVICE_IN_S[eval(dictKey)] = int(dictValue)
for entryArray in paramsDict['UPDATE_MIN_FREQUENCIES_IN_DAYS']:
    dictKey = entryArray[0] # These are strings at this point
    dictValue = entryArray[1]
    UPDATE_MIN_FREQUENCIES_IN_DAYS[eval(dictKey)] = int(dictValue)
# ---- Start of FSM definition
## Retrieve the list of states, if user-defined
if 'FSM_states' in paramsDict and len(paramsDict['FSM_states']) > 0:
    # Parse the user-provided list of states from JSON
    FSM_states = paramsDict['FSM_states']
else:
    FSM_states = DEFAULT_FSM_states
## Retrieve the initial state, if user-defined
if 'FSM_initial_state' in paramsDict and paramsDict['FSM_initial_state'] != '':
    # Parse the user-provided initial state from JSON
    FSM_initial_state = paramsDict['FSM_initial_state']
else:
    FSM_initial_state = DEFAULT_FSM_initial_state
## Retrieve the list of transitions, if user-defined
if 'FSM_transitions' in paramsDict and len(paramsDict['FSM_transitions']) >0:
    # Parse the list of transitions from JSON format into 'transitions' format
    for transitionArray in paramsDict['FSM_transitions']:
        # Extract the name of the transition to be added
        transitionNameTwoItemArray = transitionArray[0]
        if len(transitionNameTwoItemArray) != 2:
            errorMessage = "Transition name array of the transition array '" + str(transitionArray) + "' does not contain the expected two items! Quitting program execution..."
            #logging.error(errorMessage)
            sys.exit(errorMessage)
        else:
            transitionName = transitionNameTwoItemArray[1] # This is always expected to be a single item - just the name of the transition
            if transitionName == '':
                #logging.info("Empty transition name found in the transition array '" + str(transitionArray) + "', skipping further processing of this array!")
                # The transition HAS TO have a name, otherwise it might be a template array left at the end of the default FSM definition for user's convenience
                continue
        ## Skipping the test of whether the 1st array element is consistent with the element type (transition name, source/dest state name etc.) at this point
        # Extract the name(s) of source state(s)
        sourceStateOrStatesArray = transitionArray[1]
        multipleSourceStates = False
        sourceStateNameString = ''
        sourceStateNamesArray = []
        if len(sourceStateOrStatesArray) < 2:
            errorMessage = "Source state of the transition array '" + str(transitionArray) + "' is not specified; it does not contain the expected minimum of two items! Quitting program execution..."
            #logging.error(errorMessage)
            sys.exit(errorMessage)
        if len(sourceStateOrStatesArray) == 2:
            multipleSourceStates = False
            sourceStateNameString = sourceStateOrStatesArray[1]
            if sourceStateNameString == '':
                errorMessage = "ERROR! Source state of the transition array '" + str(transitionArray) + "' is not specified! Quitting..."
                #logging.error(errorMessage)
                sys.exit(errorMessage)
        else:
            # Here we assume none of these will be left as an empty string
            multipleSourceStates = True
            sourceStateNamesArray = sourceStateOrStatesArray[1:]
        # Extract the name(s) of the destination state(s). Pay attention to the self-transition! (marked with "=")
        destStateOrStatesArray = transitionArray[2]
        destStateNameString = ''
        if len(destStateOrStatesArray) < 2:
            errorMessage = "Destination state of the transition array '" + str(transitionArray) + "' is not specified; it does not contain the expected minimum of two items! Quitting program execution..."
            #logging.error(errorMessage)
            sys.exit(errorMessage)
        if len(destStateOrStatesArray) == 2:
            destStateNameString = destStateOrStatesArray[1]
            # If it's the self-transition (special case!), we just pass the "=" sign on as the single destination state for 'transitions' to handle
            if destStateNameString == '':
                errorMessage = "ERROR! Destination state of the transition array '" + str(transitionArray) + "' is not specified! Quitting..."
                #logging.error(errorMessage)
                sys.exit(errorMessage)
        else:
            errorMessage = "Multiple destination states seem to be defined for the transition array '" + str(transitionArray) + "', while only one destination state is expected! Quitting program execution..."
            #logging.error(errorMessage)
            sys.exit(errorMessage)
        # Extract the conditions, if any
        conditionsArray = transitionArray[3]
        conditionNameString = ''
        multipleConditions = False
        conditionNamesArray = []
        if len(conditionsArray) < 2:
            errorMessage = "Wrong format of the conditions array of the transition array '" + str(transitionArray) + "'; it does not contain the expected minimum of two items! Quitting program execution..."
            #logging.error(errorMessage)
            sys.exit(errorMessage)
        if len(conditionsArray) == 2:
            conditionNameString = conditionsArray[1]
        else:
            multipleConditions = True
            conditionNamesArray = conditionsArray[1:]
        # Extract the behavior handlers, starting with those executed 'before' the transition happens
        beforeHandlerArray = transitionArray[4]
        multipleBeforeHandlers = False
        beforeHandlerNameString = ''
        beforeHandlerNamesArray = []
        if len(beforeHandlerArray) < 2:
            errorMessage = "Wrong format of the 'before' behavior handlers specification array of the transition array '" + str(transitionArray) + "'; it does not contain the expected minimum of two items! Quitting program execution..."
            #logging.error(errorMessage)
            sys.exit(errorMessage)
        if len(beforeHandlerArray) == 2:
            beforeHandlerNameString = beforeHandlerArray[1]
        else:
            multipleBeforeHandlers = True
            beforeHandlerNamesArray = beforeHandlerArray[1:]
        # Extract the behavior handlers: those executed 'after' the transition happens
        afterHandlerArray = transitionArray[5]
        multipleAfterHandlers = False
        afterHandlerNameString = ''
        afterHandlerNamesArray = []
        if len(afterHandlerArray) < 2:
            errorMessage = "Wrong format of the 'after' behavior handlers specification array of the transition array '" + str(transitionArray) + "'; it does not contain the expected minimum of two items! Quitting program execution..."
            #logging.error(errorMessage)
            sys.exit(errorMessage)
        if len(afterHandlerArray) == 2:
            afterHandlerNameString = afterHandlerArray[1]
        else:
            multipleAfterHandlers = True
            afterHandlerNamesArray = afterHandlerArray[1:]
        # Having now collected all the pieces of the transition definition puzzle, let's add it in the 'transitions'-readable format the the 'FSM_transitions' array, as a dict
        ## Compile the dict object
        transitionDict = {}
        transitionDict['trigger'] = transitionName
        if multipleSourceStates:
            transitionDict['source'] = sourceStateNamesArray
        else:
            transitionDict['source'] = sourceStateNameString
        transitionDict['dest'] = destStateNameString
        if multipleConditions:
            transitionDict['conditions'] = conditionNamesArray
        else:
            if conditionNameString != "":
                # Do not add empty "conditions" clause
                transitionDict['conditions'] = conditionNameString
        if multipleBeforeHandlers:
            transitionDict['before'] = beforeHandlerNamesArray
        else:
            if beforeHandlerNameString != '':
                # Do not add empty "before" clause
                transitionDict['before'] = beforeHandlerNameString
        if multipleAfterHandlers:
            transitionDict['after'] = afterHandlerNamesArray
        else:
            if afterHandlerNameString != '':
                # Do not add empty "after" clause
                transitionDict['after'] = afterHandlerNameString
        ## Append the dict object to the list of user-defined transitions
        FSM_transitions.append(transitionDict)
else:
    FSM_transitions = DEFAULT_FSM_transitions
# ---- end of FSM definition
# Optional parameters
if 'NO_OF_TIME_SLICES_PER_USE_PERIOD' in paramsDict and len(paramsDict['NO_OF_TIME_SLICES_PER_USE_PERIOD']) > 0:
    for entryArray in paramsDict['NO_OF_TIME_SLICES_PER_USE_PERIOD']:
        dictKey = entryArray[0] # These are strings at this point
        dictValue = entryArray[1]
        NO_OF_TIME_SLICES_PER_USE_PERIOD[eval(dictKey)] = int(dictValue)
else:
    for item in list(TimesOfTheDay):
        NO_OF_TIME_SLICES_PER_USE_PERIOD[eval(str(item))] = DEFAULT_NO_OF_SLICES_PER_USE_PERIOD
#
ESTIMATED_SIZE_OF_UPDATE_REQUEST_IN_KBS = paramsDict["ESTIMATED_SIZE_OF_UPDATE_REQUEST_IN_KBS"]
ESTIMATED_SIZE_OF_NO_UPDATE_RESPONSE_IN_KBS = paramsDict["ESTIMATED_SIZE_OF_NO_UPDATE_RESPONSE_IN_KBS"]
# These are optional in the config file, handle it accordingly
if 'BOOT_TIME_IN_S' in paramsDict and paramsDict['BOOT_TIME_IN_S'] != '':
    BOOT_TIME_IN_S = paramsDict["BOOT_TIME_IN_S"]
else:
    BOOT_TIME_IN_S = DEFAULT_BOOT_TIME_IN_S
if 'BOOT_TIME_IN_S_DEBUGGING' in paramsDict and paramsDict["BOOT_TIME_IN_S_DEBUGGING"] != '':
    BOOT_TIME_IN_S_DEBUGGING = paramsDict["BOOT_TIME_IN_S_DEBUGGING"]
else:
    BOOT_TIME_IN_S_DEBUGGING = DEFAULT_BOOT_TIME_IN_S_DEBUGGING
if 'NUMBER_OF_TIME_SLICES_TO_WAIT_BEFORE_FIRST_USER_INTERACTION' in paramsDict and paramsDict['NUMBER_OF_TIME_SLICES_TO_WAIT_BEFORE_FIRST_USER_INTERACTION'] != '':
    NUMBER_OF_TIME_SLICES_TO_WAIT_BEFORE_FIRST_USER_INTERACTION = paramsDict["NUMBER_OF_TIME_SLICES_TO_WAIT_BEFORE_FIRST_USER_INTERACTION"]
else:
    NUMBER_OF_TIME_SLICES_TO_WAIT_BEFORE_FIRST_USER_INTERACTION = DEFAULT_NUMBER_OF_TIME_SLICES_TO_WAIT_BEFORE_FIRST_USER_INTERACTION
if 'NO_OF_SUB_SLICES_PER_TIME_PERIOD' in paramsDict and paramsDict['NO_OF_SUB_SLICES_PER_TIME_PERIOD'] != '':
    NO_OF_SUB_SLICES_PER_TIME_PERIOD = paramsDict["NO_OF_SUB_SLICES_PER_TIME_PERIOD"]
else:
    NO_OF_SUB_SLICES_PER_TIME_PERIOD = DEFAULT_NO_OF_SUB_SLICES_PER_TIME_PERIOD
#### VARIABLES
# Filesystem
FILESYSTEM_DIR_SEPARATOR = '/'
RESOURCE_FILES_DIRECTORY = 'resources'
STD_DIAGRAMS_DIR = '/state_diagrams'
OS_DIR_SEPARATOR = '/'
X_PARTY_SERVERS_CSV_FILENAME = 'x-party-servers.csv'
# MISC
# Properties of the times of the day (use periods)
# Read from the config JSON file
'''
NO_OF_TIME_SLICES_PER_USE_PERIOD[TimesOfTheDay.NIGHT] = 10 # all set to 10 for now, customizable if needed
NO_OF_TIME_SLICES_PER_USE_PERIOD[TimesOfTheDay.EARLY_MORNING] = 10
NO_OF_TIME_SLICES_PER_USE_PERIOD[TimesOfTheDay.MORNING] = 10
NO_OF_TIME_SLICES_PER_USE_PERIOD[TimesOfTheDay.LUNCHTIME] = 10
NO_OF_TIME_SLICES_PER_USE_PERIOD[TimesOfTheDay.AFTERNOON] = 10
NO_OF_TIME_SLICES_PER_USE_PERIOD[TimesOfTheDay.EVENING] = 10
NO_OF_TIME_SLICES_PER_USE_PERIOD[TimesOfTheDay.LATE_EVENING] = 10
'''
# Device properties # Also loaded from the config file now
#ESTIMATED_SIZE_OF_UPDATE_REQUEST_IN_KBS = 128
#ESTIMATED_SIZE_OF_NO_UPDATE_RESPONSE_IN_KBS = 64
#
'''
DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.VOICE_ASSISTANT] = 3 * 1024 # Assuming max 3 MB voice recordings sent around by/to a VA
DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.LIGHTBULB_HUB_GATEWAY] = 256 # Hue Bridge has max peaks around 8 KBs, Lightify GW has approx 1.5 KB peaks, using floor(8,1.5)
DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.THERMOSTAT] = 256 # I don't expect it to be much different than a lighbulb's bridge/gateway
DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.VIDEO_DOORBELL] = 50 * 1024 # In streaming mode these can go crazy with the size of streamed files, e.g. in HD. Number not backed by specific insights/knowledge though (at this point)
DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.SECURITY_CAMERA] = 50 * 1024 # most probably same as a video doorbell (that is kind of a security camera itself)
DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.MOVEMENT_SENSOR] = 256 # seems to make sense to place it in the range of LIGHTBULB's HUB/GATEWAY traffic
DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.PLUG] = 128 # making it half of lightbulb, since you cannot really change many variables, rather just turn it on/off?
DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.STREAMING_AV] = 200 * 1024 # four times more than the video doorbell/security camera, since e.g. 4K? Totally guessing for now
#
DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.VOICE_ASSISTANT] = 3 * 1024 # Echo had max peaks of ~325 KBs per 5-s windows, then mostly ~20 KBs
DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.LIGHTBULB_HUB_GATEWAY] = 256 # Making a total guess, trying to compare it to a VA
DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.THERMOSTAT] = 256
DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.VIDEO_DOORBELL] = 50 * 1024
DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.SECURITY_CAMERA] = 50 * 1024
DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.MOVEMENT_SENSOR] = 256
DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.PLUG] = 128
DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.STREAMING_AV] = 200 * 1024
'''
# User properties
#print('EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS: ' + str(EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS))
#EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS = json.loads(paramsDict['EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS'])
#print('EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS: ' + str(EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS))
# Of course it all depends on the 'profile' of the user, whether the user uses the device more of less etc.
# The settings/spec below are loosely based on statistics obtained by Clutch, Voicebot, Code Computerlove.
# For instance, most of the VAs are placed in (descending freq order) living rooms, bedrooms and kitchens. From the same report, VAs are mostly
# 'used to stream music, control lighting, set timers and get the weather'.
'''
# E.g. the user might ask to look stuff up in calendar for tomorrow, search stuff on the internet, check weather forecast, etc.
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.NIGHT], Weekdays.MONDAY)] = (0, 2)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.NIGHT], Weekdays.TUESDAY)] = (0, 2)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.NIGHT], Weekdays.WEDNESDAY)] = (0, 2)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.NIGHT], Weekdays.THURSDAY)] = (0, 2)
# People might stay up late or come back from a party
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.NIGHT], Weekdays.FRIDAY)] = (0, 1)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.NIGHT], Weekdays.SATURDAY)] = (0, 1)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.NIGHT], Weekdays.SUNDAY)] = (0, 2)
# Similarly to the before-bed routine, at least in my opinion at the moment, voice interaction might come in handy
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EARLY_MORNING], Weekdays.MONDAY)] = (0, 5)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EARLY_MORNING], Weekdays.TUESDAY)] = (0, 5)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EARLY_MORNING], Weekdays.WEDNESDAY)] = (0, 5)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EARLY_MORNING], Weekdays.THURSDAY)] = (0, 5)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EARLY_MORNING], Weekdays.FRIDAY)] = (0, 5)
# Say most people like to sleep in on these days, so no talking to VAs then
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EARLY_MORNING], Weekdays.SATURDAY)] = (0, 2)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EARLY_MORNING], Weekdays.SUNDAY)] = (0, 2)
# If someone's home at this time, they would most probably do stuff instead of playing around with the assistant?
# Let's assume a house without home-staying users for now
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.MORNING], Weekdays.MONDAY)] = (0, 1)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.MORNING], Weekdays.TUESDAY)] = (0, 1)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.MORNING], Weekdays.WEDNESDAY)] = (0, 1)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.MORNING], Weekdays.THURSDAY)] = (0, 1)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.MORNING], Weekdays.FRIDAY)] = (0, 1)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.MORNING], Weekdays.SATURDAY)] = (0, 3)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.MORNING], Weekdays.SUNDAY)] = (0, 3)
# If it's the weekend, it could be cooking
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LUNCHTIME], Weekdays.MONDAY)] = (0, 0)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LUNCHTIME], Weekdays.TUESDAY)] = (0, 0)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LUNCHTIME], Weekdays.WEDNESDAY)] = (0, 0)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LUNCHTIME], Weekdays.THURSDAY)] = (0, 0)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LUNCHTIME], Weekdays.FRIDAY)] = (0, 0)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LUNCHTIME], Weekdays.SATURDAY)] = (0, 5)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LUNCHTIME], Weekdays.SUNDAY)] = (0, 10)
#
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.AFTERNOON], Weekdays.MONDAY)] = (0, 0)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.AFTERNOON], Weekdays.TUESDAY)] = (0, 0)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.AFTERNOON], Weekdays.WEDNESDAY)] = (0, 0)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.AFTERNOON], Weekdays.THURSDAY)] = (0, 0)
# On the weekend it might be used for entertainment or possibly cooking as well
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.AFTERNOON], Weekdays.FRIDAY)] = (0, 0)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.AFTERNOON], Weekdays.SATURDAY)] = (0, 3)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.AFTERNOON], Weekdays.SUNDAY)] = (0, 3)
#
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EVENING], Weekdays.MONDAY)] = (0, 2)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EVENING], Weekdays.TUESDAY)] = (0, 2)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EVENING], Weekdays.WEDNESDAY)] = (0, 2)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EVENING], Weekdays.THURSDAY)] = (0, 2)
# On the weekend it might be used for entertainment or possibly cooking as well
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EVENING], Weekdays.FRIDAY)] = (0, 3)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EVENING], Weekdays.SATURDAY)] = (0, 3)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.EVENING], Weekdays.SUNDAY)] = (0, 5)
#
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LATE_EVENING], Weekdays.MONDAY)] = (0, 2)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LATE_EVENING], Weekdays.TUESDAY)] = (0, 2)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LATE_EVENING], Weekdays.WEDNESDAY)] = (0, 2)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LATE_EVENING], Weekdays.THURSDAY)] = (0, 2)
# On the weekend it might be used for entertainment or possibly cooking as well
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LATE_EVENING], Weekdays.FRIDAY)] = (0, 3)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LATE_EVENING], Weekdays.SATURDAY)] = (0, 3)
EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(DeviceTypes.VOICE_ASSISTANT, DEVICE_USE_PERIOD_HOURS_RANGE[TimesOfTheDay.LATE_EVENING], Weekdays.SUNDAY)] = (0, 2)
'''
# Server / backed properties
BASE_URL_FIRST_PARTY_LS = 'YOUR_URL'
BASE_URL_SUPPORT_PARTY_DS = 'YOUR_URL'
BASE_URL_THIRD_PARTY_BDS = 'YOUR_URL'
BASE_URL_FIRST_PARTY_2 = '' # if needed, one more machine can be set up, with a separate public IP (as an X-party, for 1st or support)
TEST_ENDPOINT_1 = '/submit_a_phony_file'
GROUND_TRUTH_SUBMISSION_ENDPOINT_URL = '/submit_gtdata'
#
GROUND_TRUTH_SUBMISSION_URL = BASE_URL_FIRST_PARTY_LS + GROUND_TRUTH_SUBMISSION_ENDPOINT_URL
# Time sync servers
BASE_URL_NTP_SERVICE_0 = '0.europe.pool.ntp.org' # From https://www.ntppool.org/en, available indices: 0-3
GOOGLE_NTP_SERVICE_S1 = 'time1.google.com' # These're mostly used by the IoT devices observed in our dataset
GOOGLE_NTP_SERVICE_S2 = 'time2.google.com' # and most probably the best to use while emulating
GOOGLE_NTP_SERVICE_S3 = 'time3.google.com'
GOOGLE_NTP_SERVICE_S4 = 'time4.google.com'
US_NTP_SERVER = 'us.pool.ntp.org' # One can also test for region-specific servers
NA_NTP_SERVER_0 = '0.north-america.pool.ntp.org'
SE_NTP_SERVER = 'se.pool.ntp.org'
#### MAPS / DICTIONARIES
CONN_TYPE_TO_ENTITY = {}
CONN_TYPE_TO_ENTITY[ConnectionTypes.CORE_SERVICE_API] = ContactedEntities.FIRST_PARTY
CONN_TYPE_TO_ENTITY[ConnectionTypes.CDN_RESOURCE] = ContactedEntities.SUPPORT_PARTY
CONN_TYPE_TO_ENTITY[ConnectionTypes.TRACKING_ADS] = ContactedEntities.THIRD_PARTY
CONN_TYPE_TO_ENTITY[ConnectionTypes.ANALYTICS] = ContactedEntities.THIRD_PARTY
CONN_TYPE_TO_ENTITY[ConnectionTypes.TIME_SYNC_NTP] = ContactedEntities.NTP_SERVICE
CONN_TYPE_TO_ENTITY[ConnectionTypes.CHECK_FOR_UPDATES] = ContactedEntities.FIRST_PARTY
ENTITY_TO_URL = {}
ENTITY_TO_URL[ContactedEntities.FIRST_PARTY] = [ BASE_URL_FIRST_PARTY_LS + TEST_ENDPOINT_1 ]
ENTITY_TO_URL[ContactedEntities.SUPPORT_PARTY] = [ BASE_URL_SUPPORT_PARTY_DS + TEST_ENDPOINT_1 ]
ENTITY_TO_URL[ContactedEntities.THIRD_PARTY] = [ BASE_URL_THIRD_PARTY_BDS + TEST_ENDPOINT_1 ]
ENTITY_TO_URL[ContactedEntities.NTP_SERVICE] = [ GOOGLE_NTP_SERVICE_S1, GOOGLE_NTP_SERVICE_S2, GOOGLE_NTP_SERVICE_S3, GOOGLE_NTP_SERVICE_S4 ]
# entries are tuples of (min,max) number of seconds since last request defining boundaries for the frequency of connections
CONNECTION_FREQUENCIES = {}
CONNECTION_FREQUENCIES[ConnectionFrequencies.HIGH] = (0, 1.5 * 60) # (0 - 1.5 mins)
CONNECTION_FREQUENCIES[ConnectionFrequencies.MEDIUM] = (CONNECTION_FREQUENCIES[ConnectionFrequencies.HIGH][1], 5 * 60) # (1.5 min -5 mins)
CONNECTION_FREQUENCIES[ConnectionFrequencies.LOW] = (CONNECTION_FREQUENCIES[ConnectionFrequencies.MEDIUM][1], 2 * 60 * 60) # (5 mins - 2 hours)
CONNECTION_FREQUENCIES[ConnectionFrequencies.VERY_LOW] = (CONNECTION_FREQUENCIES[ConnectionFrequencies.LOW][1], 36 * 60 * 60) # (2 hours - 1.5 day)
CONNECTION_FREQUENCIES[ConnectionFrequencies.EXTREMELY_LOW] = (CONNECTION_FREQUENCIES[ConnectionFrequencies.VERY_LOW][1], 7 * 24 * 60 * 60) # (1.5 day - 7 days)
CONNECTION_FREQUENCIES[ConnectionFrequencies.NONE] = (math.inf, math.inf) # freq = 0
CONNECTION_FREQUENCIES.setdefault((math.inf, math.inf)) # default value set to freq=0 (never conctact)
#
'''
HEARTBEAT_FREQ_PER_DEVICE_IN_S[DeviceTypes.VOICE_ASSISTANT] = 50
HEARTBEAT_FREQ_PER_DEVICE_IN_S[DeviceTypes.LIGHTBULB_HUB_GATEWAY] = 120 # it's ~30 mins (!) for Lightify Gateway and ~2 mins for Hue Bridge
HEARTBEAT_FREQ_PER_DEVICE_IN_S[DeviceTypes.THERMOSTAT] = 60 # No empirical data for the moment, have a look at IMC and other papers!
HEARTBEAT_FREQ_PER_DEVICE_IN_S[DeviceTypes.VIDEO_DOORBELL] = 45 # Guesswork only, see above
HEARTBEAT_FREQ_PER_DEVICE_IN_S[DeviceTypes.SECURITY_CAMERA] = 45 # See above
HEARTBEAT_FREQ_PER_DEVICE_IN_S[DeviceTypes.MOVEMENT_SENSOR] = 70 # See above
HEARTBEAT_FREQ_PER_DEVICE_IN_S[DeviceTypes.PLUG] = 240 # See above
HEARTBEAT_FREQ_PER_DEVICE_IN_S[DeviceTypes.STREAMING_AV] = 55 # See above
'''
HEARTBEAT_DATA_PACKETS_SIZE_PER_DEVICE_IN_KBS = {} # a tuple of KBs for (sent,received) data
HEARTBEAT_DATA_PACKETS_SIZE_PER_DEVICE_IN_KBS[DeviceTypes.VOICE_ASSISTANT] = (1, 0.5) # Guesswork for now, TODO: to be updated based on observed traffic
HEARTBEAT_DATA_PACKETS_SIZE_PER_DEVICE_IN_KBS[DeviceTypes.LIGHTBULB_HUB_GATEWAY] = (0.5, 0.25)
HEARTBEAT_DATA_PACKETS_SIZE_PER_DEVICE_IN_KBS[DeviceTypes.THERMOSTAT] = (0.5, 0.5)
HEARTBEAT_DATA_PACKETS_SIZE_PER_DEVICE_IN_KBS[DeviceTypes.VIDEO_DOORBELL] = (1.25, 0.75)
HEARTBEAT_DATA_PACKETS_SIZE_PER_DEVICE_IN_KBS[DeviceTypes.SECURITY_CAMERA] = (1, 0.5)
HEARTBEAT_DATA_PACKETS_SIZE_PER_DEVICE_IN_KBS[DeviceTypes.MOVEMENT_SENSOR] = (0.5, 0.25)
HEARTBEAT_DATA_PACKETS_SIZE_PER_DEVICE_IN_KBS[DeviceTypes.PLUG] = (0.25, 0.25)
HEARTBEAT_DATA_PACKETS_SIZE_PER_DEVICE_IN_KBS[DeviceTypes.STREAMING_AV] = (0.75, 0.5)
SUPPORT_PARTY_CONN_PACKET_SIZE_IN_KBS = {} # contains tuples of (sent,received) sizes in KBs
SUPPORT_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.VOICE_ASSISTANT] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.VOICE_ASSISTANT] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.VOICE_ASSISTANT] * (2/3))
SUPPORT_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.LIGHTBULB_HUB_GATEWAY] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.LIGHTBULB_HUB_GATEWAY] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.LIGHTBULB_HUB_GATEWAY] * (2/3))
SUPPORT_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.THERMOSTAT] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.THERMOSTAT] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.THERMOSTAT] * (2/3))
SUPPORT_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.VIDEO_DOORBELL] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.VIDEO_DOORBELL] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.VIDEO_DOORBELL] * (2/3))
SUPPORT_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.SECURITY_CAMERA] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.SECURITY_CAMERA] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.SECURITY_CAMERA] * (2/3))
SUPPORT_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.MOVEMENT_SENSOR] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.MOVEMENT_SENSOR] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.MOVEMENT_SENSOR] * (2/3))
SUPPORT_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.PLUG] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.PLUG] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.PLUG] * (2/3))
SUPPORT_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.STREAMING_AV] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.STREAMING_AV] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.STREAMING_AV] * (2/3))
THIRD_PARTY_CONN_PACKET_SIZE_IN_KBS = {} # contains tuples of (sent,received) sizes in KBs
THIRD_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.VOICE_ASSISTANT] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.VOICE_ASSISTANT] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.VOICE_ASSISTANT] * (2/3))
THIRD_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.LIGHTBULB_HUB_GATEWAY] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.LIGHTBULB_HUB_GATEWAY] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.LIGHTBULB_HUB_GATEWAY] * (2/3))
THIRD_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.THERMOSTAT] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.THERMOSTAT] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.THERMOSTAT] * (2/3))
THIRD_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.VIDEO_DOORBELL] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.VIDEO_DOORBELL] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.VIDEO_DOORBELL] * (2/3))
THIRD_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.SECURITY_CAMERA] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.SECURITY_CAMERA] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.SECURITY_CAMERA] * (2/3))
THIRD_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.MOVEMENT_SENSOR] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.MOVEMENT_SENSOR] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.MOVEMENT_SENSOR] * (2/3))
THIRD_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.PLUG] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.PLUG] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.PLUG] * (2/3))
THIRD_PARTY_CONN_PACKET_SIZE_IN_KBS[DeviceTypes.STREAMING_AV] = (DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[DeviceTypes.STREAMING_AV] * (2/3), DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[DeviceTypes.STREAMING_AV] * (2/3))
'''
UPDATE_MIN_FREQUENCIES_IN_DAYS[DeviceTypes.VOICE_ASSISTANT] = 21 # say every 3 weeks, +/- x% as in the random rate per device
UPDATE_MIN_FREQUENCIES_IN_DAYS[DeviceTypes.LIGHTBULB_HUB_GATEWAY] = 60
UPDATE_MIN_FREQUENCIES_IN_DAYS[DeviceTypes.THERMOSTAT] = 45
UPDATE_MIN_FREQUENCIES_IN_DAYS[DeviceTypes.VIDEO_DOORBELL] = 25
UPDATE_MIN_FREQUENCIES_IN_DAYS[DeviceTypes.SECURITY_CAMERA] = 45
UPDATE_MIN_FREQUENCIES_IN_DAYS[DeviceTypes.MOVEMENT_SENSOR] = 30
UPDATE_MIN_FREQUENCIES_IN_DAYS[DeviceTypes.PLUG] = 90
UPDATE_MIN_FREQUENCIES_IN_DAYS[DeviceTypes.STREAMING_AV] = 30
'''
# Lenghts of respective freq time periods (helper values for underlying code)
CONN_FREQ_PERIODS_LEN_IN_S = {}
CONN_FREQ_PERIODS_LEN_IN_S[ConnectionFrequencies.NONE] = 0
CONN_FREQ_PERIODS_LEN_IN_S[ConnectionFrequencies.HIGH] = CONNECTION_FREQUENCIES[ConnectionFrequencies.HIGH][1] - CONNECTION_FREQUENCIES[ConnectionFrequencies.HIGH][0]
CONN_FREQ_PERIODS_LEN_IN_S[ConnectionFrequencies.MEDIUM] = CONNECTION_FREQUENCIES[ConnectionFrequencies.MEDIUM][1] - CONNECTION_FREQUENCIES[ConnectionFrequencies.MEDIUM][0]
CONN_FREQ_PERIODS_LEN_IN_S[ConnectionFrequencies.LOW] = CONNECTION_FREQUENCIES[ConnectionFrequencies.LOW][1] - CONNECTION_FREQUENCIES[ConnectionFrequencies.LOW][0]
CONN_FREQ_PERIODS_LEN_IN_S[ConnectionFrequencies.VERY_LOW] = CONNECTION_FREQUENCIES[ConnectionFrequencies.VERY_LOW][1] - CONNECTION_FREQUENCIES[ConnectionFrequencies.VERY_LOW][0]
CONN_FREQ_PERIODS_LEN_IN_S[ConnectionFrequencies.EXTREMELY_LOW] = CONNECTION_FREQUENCIES[ConnectionFrequencies.EXTREMELY_LOW][1] - CONNECTION_FREQUENCIES[ConnectionFrequencies.EXTREMELY_LOW][0]
CONN_FREQ_PERIODS_LEN_IN_S.setdefault(0) # also default length is 0 and the condition `if timeElapsedSinceLastCondTest < X` will work correctly then
