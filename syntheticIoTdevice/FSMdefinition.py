class FSMdefinition(object):

    #def __init__(self,states,transitions,conditions):
    def __init__(self, states, initial_state, transitions, FSM_ignore_invalid_triggers, FSM_auto_transitions, FSM_show_conditions, FSM_show_auto_transitions, FSM_show_state_attributes, FSM_before_state_change, FSM_after_state_change, FSM_send_event):
        self.states = states
        self.transitions = transitions
        self.initial_state = initial_state
        self.ignore_invalid_triggers = FSM_ignore_invalid_triggers
        self.auto_transitions = FSM_auto_transitions
        self.show_conditions = FSM_show_conditions
        self.show_auto_transitions = FSM_show_auto_transitions
        self.show_state_attributes = FSM_show_state_attributes
        self.before_state_change = FSM_before_state_change
        self.after_state_change = FSM_after_state_change
        self.send_event = FSM_send_event
        # No need, conditions are part of the definition of transitions actually
        #self.conditions = conditions
