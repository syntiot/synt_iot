#from transitions import Machine # the Python package with FSM implementation
from transitions.extensions import GraphMachine as Machine # use Diagrams feat
import threading
import time
import datetime
import socket
import random
import struct
import requests
import logging
import pathlib
#import inspect # imported in the method
import pytz
import json # for the code sending ground truth data to the backend
import sys # for the sake of 'sys.exc_info()[0]'
import hashlib # For checksum
import sys # For sys.maxsize when wrapping around the value of 'self.FSMstateChange_recordSequenceNumber'

import cons_defs as cdefs

import EmulatedDeviceInfo
import FSMdefinition
import SimulatedIoTuser
from RepeatTimer import RepeatTimer # for uploading ground truth data

class SimulatedIoTdevice(object):
    '''
    Simulating a commercial IoT device in software,
    as an RPi-run Python script
    '''
    def __init__(self,FSMdefinition,deviceSpecification):
        """
        Initialize the simulated device with information about its State Transition Diagram
        (specifying what states and how does it assume) as well as device-specific information
        (defining technical and vendor/model specific properties of the simulated device)
        """
        logging.info('Initializing the `SimulatedIoTdevice`') # After having configured the `transitions` logger/logging, simply calling `logging` here works
        #print('Entering SimulatedIoTdevice.__init__')
        #self.name = deviceSpecification.name # Redundant, since it's contained within deviceInfo object
        # Initialize information about this particular device
        self.deviceInfo = deviceSpecification
        # Initialize information about the particular definition
        # of the State Transition Diagram / FSM that will decide
        # how will this device behave
        #self.states = FSMdefinition.states
        #self.transitions = FSMdefinition.transitions
        # No need, since they're part of the definition of transitions actually
        #self.conditions = FSMdefinition.conditions
        # Add the simulated user of this particular device type
        self.simulatedUser = SimulatedIoTuser.SimulatedIoTuser(self.deviceInfo.devType)
        # Initialize the `transitions` package's FSM object
        self.FSM = Machine(
            model = self,
            states = FSMdefinition.states,
            initial = FSMdefinition.initial_state,
            transitions = FSMdefinition.transitions,
            ignore_invalid_triggers = FSMdefinition.ignore_invalid_triggers,
            auto_transitions = FSMdefinition.auto_transitions,
            show_conditions = FSMdefinition.show_conditions,
            show_auto_transitions = FSMdefinition.show_auto_transitions,
            show_state_attributes = FSMdefinition.show_state_attributes,
            before_state_change = FSMdefinition.before_state_change,
            after_state_change = FSMdefinition.after_state_change,
            send_event = FSMdefinition.send_event
            )
        # Draw the diagram
        # draw the whole graph ...
        self.device_timezone = pytz.timezone('Europe/Amsterdam') # Random, anonimized for submission
        STDgraphTimestamp = time.strftime("%d%m%Y-%H%M%S")
        thisScriptDirPath = str(pathlib.Path(__file__).parent.absolute())
        #stdGraphFilepath = thisScriptDirPath + cdefs.OS_DIR_SEPARATOR + cdefs.STD_DIAGRAMS_DIR + cdefs.OS_DIR_SEPARATOR + 'std_' + STDgraphTimestamp + '.png'
        # Skipping timestamp to avoid generating a bunch of useless duplicate files while testing
        stdGraphFilepath = thisScriptDirPath + cdefs.OS_DIR_SEPARATOR + cdefs.STD_DIAGRAMS_DIR + cdefs.OS_DIR_SEPARATOR + 'std.png'
        self.FSM.get_graph().draw(stdGraphFilepath, prog='dot')
        # Model-specific variables
        self.bootStartedTimestamp = None
        self.lastTimeSyncTimestamp = None
        self.lastUpdatesCheckTimestamp = None
        self.lastUpdatesDownloadTimestamp = None
        self.lastUserRequestTimestamp = None
        self.lastHeartbeatTimestamp = None
        self.last1stPartyConnTimestamp = None
        self.lastSupportPartyConnTimestamp = None
        self.last3rdPartyConnTimestamp = None
        self.randomTimeSyncDelay = -1.0 # number of seconds to delay time sync with, to simulate real multi-process environment of IoT device
        self.randomUpdatesCheckDelay = -1.0
        self.servingUserRequestNow = False
        self.didWeAlreadyLogThatBootProcessHasFinished = False
        #
        self.intervalOfSendingFSMstatusUpdatesToBackendInSeconds = 5
        #
        self.FSMstateChanges_listOfDicts = []
        self.FSMstateChangeDictKey_timestamp = 'ts_DATETIME6'
        self.FSMstateChangeDictKey_transitionStartingOrDone = 'startingOrFinished'
        self.FSMstateChangeDictKey_fromState = 'fromState'
        self.FSMstateChangeDictKey_toState = 'toState'
        self.FSMstateChangeDictKey_transitionName = 'transitionName'
        self.FSMstateChangeDictKey_listOfConditionsMet = 'listOfConditionsMet'
        self.FSMstateChangeDictKey_listOfConditionsNotMet = 'listOfConditionsNotMet'
        # OK, redundant since Inspector would treat multiple Synthetic devices running on the same network machine the same device_id and user_key
        self.FSMstateChangeDictKey_simDevUniqueIDforGTpayload = 'devUIDforGTpayload'
        self.FSMstateChange_transitionStarting = 'starting'
        self.FSMstateChange_transitionFinished = 'finished'
        #
        self.FSMstateChange_sha512DictKey = 'checksumSHA512'
        self.FSMstateChangeDictKey_recordSequenceNumber = 'recordSequenceNumber'
        self.FSMstateChange_recordSequenceNumber = 0
        #self.FSMstateChange_uploadIntervalInSeconds = 60 #30.0 #5.0
        # changed BELOW on Tue Apr 14, 11:57 so that (a) nothing will disturb the boot process here and (b) the frequency of uploads is still enough
        self.FSMstateChange_uploadIntervalInSeconds = deviceSpecification.bootProcessTimeInSeconds
        #self.FSMstateChange_uploadTimer = threading.Timer(self.FSMstateChange_uploadIntervalInSeconds, self.upload_FSM_status_update)
        self.FSMstateChange_uploadTimer = RepeatTimer(self.FSMstateChange_uploadIntervalInSeconds, self.upload_FSM_status_update)
        self.FSMstateChange_uploadTimer.start() # Moved from 'finalize_boot_process'
        # Can be loaded from cons_defs/JSON config file later
        self.customGroundTruthHeaderKey = 'synthetic_app_says_GT_it_is'
        self.customGroundTruthHeaderValue = 'Sir, yes Sir!'
        # Time of Synthetic device initialization with microsecond precision - to be used as a unique identifier of the device
        # by the IoT Inspector which will intercept the Ground truth packets' data and piece them together before adding device_id
        # and user key to then send it off to the backend, which needs both info from the Device Machine (FSM) and Inspector.
        self.ts_us_uid = datetime.datetime.now(self.device_timezone).strftime('%Y-%m-%d_%H-%M-%S.%f')
        logging.info('The `SimulatedIoTdevice` is initialized now')

    ##### CALLBACK methods
    # Implementation of `before` and `after` methods

    def initialize_boot_process(self, event):
        """
        Boot process: initialize necessary variables
        COMMENT: since this is executed before the device is taken out from
                 the state `off`, it should be set as an `after` (transition)
                 method. Was `before` to test the `transitions` package first.
        """
        logging.info('Initializing the boot process')
        #print('Entering SimulatedIoTdevice.initialize_boot_process')
        if self.bootStartedTimestamp == None:
            if self.device_timezone is None:
                logging.error("[initialize_boot_process]\nERROR!\nself.device_timezone is None!\n###### RETURNING!")
                return
            self.bootStartedTimestamp = datetime.datetime.now(self.device_timezone).timestamp()

    def finalize_boot_process(self, event):
        """
        Boot process: execute the actual boot sequence/tasks in this method
        (since we base on the control flow orchestrated by the `transitions` package)
        """
        # Start the timer triggering the upload function
        logging.info('Finalizing the boot process') # What if there's no internet connection?
        # The methods below cannot succeed
        # Time synchronization (with random, up to 10% delay in-built in the method)
        # actually make a connection to device's preferred NTP server and update last sync time

    def boot_process_completed(self, event):
        """ Return the status of boot process, is it done yet?
            A condition callback
        """
        time_now = datetime.datetime.now(self.device_timezone).timestamp()
        if time_now - self.bootStartedTimestamp >= self.deviceInfo.bootProcessTimeInSeconds:
            if not self.didWeAlreadyLogThatBootProcessHasFinished:
                logging.info("Boot process has completed now.")
                self.didWeAlreadyLogThatBootProcessHasFinished = True
            return True
        else:
            return False

    def boot_process_in_progress(self, event):
        return not SimulatedIoTdevice.boot_process_completed(self, event)

    def internet_connection_available(self, event):
        """ Check if the device has a working internet connection """
        remote_server_url = 'www.startpage.com'
        try:
            host = socket.gethostbyname(remote_server_url)
            s = socket.create_connection((host,80), 2)
            s.close()
            return True
        except socket.gaierror as e:
            logging.info("EXCEPTION Socket gaierror occurred (" + str(e) + ")")
            return False
        except socket.error as e:
            logging.info("EXCEPTION Socket error occurred (" + str(e) + ")")
            return False

    def internet_connection_unavailable(self, event):
        """ Check if there is no internet connection """
        return not SimulatedIoTdevice.internet_connection_available(self, event)

    def time_for_time_sync(self, event):
        """
        Return a boolean saying whether it's time to synchronize time yet.
        Includes a random delay in time synchronization per each device to
        simulate the real environment in which the time synchronization process
        will operate in a real device.
        """
        if self.lastTimeSyncTimestamp == None:
            return True
        time_now = datetime.datetime.now(self.device_timezone).timestamp()
        # NOTE: the value of `self.lastTimeSyncTimestamp` was initialized with `None`
        #       and is set only in `time_for_updates`, which we ASSUME here will be
        #       executed BEFORE calling this method, in `finalize_boot_process`,
        #       to the return value of `synchronize_time_with_ntp_server` method
        timeSinceLastTimeSync = time_now - self.lastTimeSyncTimestamp
        # Get and compute necessary information
        freqOfTimeSync = self.deviceInfo.connTypeToFreq[cdefs.ConnectionTypes.TIME_SYNC_NTP]
        lenOfTheFreqPeriod = SimulatedIoTdevice.get_len_of_freq_period(freqOfTimeSync)
        if self.randomTimeSyncDelay == -1.0:
            self.randomTimeSyncDelay = random.uniform(1, 1.1) * lenOfTheFreqPeriod # delay of up to 10% of time
        if timeSinceLastTimeSync >= lenOfTheFreqPeriod + self.randomTimeSyncDelay:
            self.randomTimeSyncDelay = -1.0
            #logging.info("Responding: yes, it's time for time sync now.")
            return True
        else:
            return False

    def time_for_updates(self, event):
        """ Returns a boolean saying whether it's time to check for updates. """
        if self.lastUpdatesCheckTimestamp == None:
            # There was no update checks yet, recommending one now
            return True
        time_now = datetime.datetime.now(self.device_timezone).timestamp()
        timeSinceLastUpdatesCheck = time_now - self.lastUpdatesCheckTimestamp
        freqOfUpdatesCheck = self.deviceInfo.connTypeToFreq[cdefs.ConnectionTypes.CHECK_FOR_UPDATES]
        lenOfTheFreqPeriod = SimulatedIoTdevice.get_len_of_freq_period(freqOfUpdatesCheck)
        if self.randomUpdatesCheckDelay == -1.0:
            self.randomUpdatesCheckDelay = random.uniform(1, 1.1) * lenOfTheFreqPeriod
        if timeSinceLastUpdatesCheck >= lenOfTheFreqPeriod + self.randomUpdatesCheckDelay:
            self.randomUpdatesCheckDelay = -1.0
            #logging.info("Responding: yes, it's time for updates now.")
            return True
        else:
            return False # too soon

    def time_for_1st_party_connection(self, event):
        """ Is it the time to connect to a 1st party server? """
        if self.last1stPartyConnTimestamp == None: # there was no successful connections yet
            # We simply make the connection? (which'll occur on boot)
            return True
        else:
            time_now = datetime.datetime.now(self.device_timezone).timestamp()
            timeSinceLastConnection = time_now - self.last1stPartyConnTimestamp
            presetFreqOfConnections = self.deviceInfo.connTypeToFreq[cdefs.ConnectionTypes.CORE_SERVICE_API]
            lenOfTheFreqPeriod = SimulatedIoTdevice.get_len_of_freq_period(presetFreqOfConnections)
            if timeSinceLastConnection >= lenOfTheFreqPeriod:
                return True
            else:
                return False # too soon

    def no_support_party_CDN_connections_yet(self, event):
        return self.lastSupportPartyConnTimestamp == None

    def time_for_3rd_party_connection(self, event):
        pass

    def no_3rd_party_connections_yet(self, event):
        return self.last3rdPartyConnTimestamp == None

    def user_request_issued(self, event):
        """ Simulating the use of simulated device """
        # Users make use of the device differently based on device type
        if self.simulatedUser.isItTheTimeForInteraction(self.deviceInfo.devType):
            self.lastUsetRequestTimestamp = datetime.datetime.now(self.device_timezone).timestamp()
            logging.info("Responding: yes, it's time for user request / interaction now. Setting self.servingUserRequestNow to True...")
            self.servingUserRequestNow = True
            return True
        else:
            return False

    def understood_what_user_wants(self, event):
        """ Simply simulating local processing/interpretation of users' request by taking time """
        #print('Entering SimulatedIoTdevice.understood_what_user_wants')
        processingSimTime = random.uniform(0.33, 0.75)
        time.sleep(processingSimTime)
        logging.info("Understood what the user wants just now.")
        return True # Yeah, it will always understand for now

    def hosts_service_API_itself(self, event):
        """
        Does the device host it's services/API on premises or do they use CDN for that?
        ## For now, we will use a naive approach expecting the name will contain a good
        ## pointer as to what device it is and we have a hardcoded list of entities from
        ## the biggest IoT vendors to answer positively here, otherwise it'll be a no.
        """
        #print('Entering SimulatedIoTdevice.hosts_service_API_itself')
        biggestIoTvendors = ['amazon', 'google', 'samsung', 'xiaomi']
        # Assumption: these have their own, on-premises 'cloud'/API backends, not outsourced to CDNs
        devicesFromBiggestIoTvendors = ['echo', 'home', 'ring', 'nest', 'chromecast', 'roku']
        for devName in devicesFromBiggestIoTvendors:
            if devName.lower() in self.deviceInfo.name.lower():
                return True
        return False

    def hosts_service_API_externally(self, event):
        """ Does the vendor of this device use external API hosting/CDN services for their core service hosting? """
        #print('Entering SimulatedIoTdevice.hosts_service_API_externally')
        return not SimulatedIoTdevice.hosts_service_API_itself(self, event)

    def cloud_brain_responded(self, event):
        """ Simply simulate the waiting time for response from the service API / CDN """
        #print('Entering SimulatedIoTdevice.cloud_brain_responded')
        processingSimTime = random.uniform(0.25, 2.75)
        time.sleep(processingSimTime)
        logging.info("Got a response from the cloud brain just now")
        return True

    def done_responding_to_the_user(self, event):
        """ Simply simulate the waiting time for issuing a response to the user """
        simRespTime = random.uniform(0.5, 2.5)
        time.sleep(simRespTime)
        logging.info("Done responding to the user just now. Also setting self.servingUserRequestNow to False...")
        self.servingUserRequestNow = False
        return True

    def is_it_time_for_a_heartbeat(self, event):
        """ Implementing the condition check on whether it's time to send a heartbeat at the moment """
        lenOfHeartbeatPeriodInSeconds = cdefs.HEARTBEAT_FREQ_PER_DEVICE_IN_S[self.deviceInfo.devType]
        #logging.info('type(lenOfHeartbeatPeriodInSeconds): %s', str(type(lenOfHeartbeatPeriodInSeconds)))
        if self.lastHeartbeatTimestamp == None:
            # No heartbeat sent yet, do it now
            #logging.info("No heartbeat sent yet, requesting it now")
            return True
        else:
            # decide whether it's time for it at the moment
            time_now = datetime.datetime.now(self.device_timezone).timestamp()
            timeSinceLastHeartbeat = time_now - self.lastHeartbeatTimestamp
            if timeSinceLastHeartbeat >= lenOfHeartbeatPeriodInSeconds:
                #logging.info("Time for a heartbeat connection, requesting it now")
                return True
        return False

    ## METHODS MAKING CONNECTIONS instead of just returning boolean values for condition status

    def send_a_heartbeat_home(self, event):
        """ Simulate the heartbeat connections, with the frequency varying per device type """
        # For now keeping it as a relay here
        firstPartyURL = self.deviceInfo.primary1stPartyServers[0]
        noOfKBsToSend, noOfKBsToReceive = cdefs.HEARTBEAT_DATA_PACKETS_SIZE_PER_DEVICE_IN_KBS[self.deviceInfo.devType]
        reqestStatus = SimulatedIoTdevice.send_and_request_phony_file(self,firstPartyURL,noOfKBsToSend,noOfKBsToReceive)
        if reqestStatus: # all succeeded
            logging.info("Successfully sent a heartbeat home now.")
            self.lastHeartbeatTimestamp = datetime.datetime.now(self.device_timezone).timestamp()
        else:
            logging.info("Error sending heartbeat home!")

    def serving_user_request_now(self, event):
        """ Return boolean/logical information whether the device is in the process of serving user request at the moment """
        return self.servingUserRequestNow

    # .: Utility methods :.

    def update_list_of_interactions_with_device(self, event):
        """ Update the list of all interactions with the device with a current datetime object """
        self.simulatedUser.allInteractionsWithTheDeviceDTs.append( datetime.datetime.now(self.device_timezone) )
        logging.info("Recorded a user interaction with the device now.")

    def sync_time_with_ntp_server(self, event):
        """
        Connect to the NTP/time sync server specified in cdefs
        and simulate time synchronization
        after: https://stackoverflow.com/questions/36500197/python-get-time-from-ntp-server
        """
        # Pick a random host from the list of preferred NTP servers
        numberOfNTPservers = len(self.deviceInfo.preferredNTPservers)
        host = self.deviceInfo.preferredNTPservers[random.randint(0,numberOfNTPservers-1)]
        port = 123 # NTP standard port, don't expect it to change
        buf = 1024
        address = (host, port)
        msg = '\x1b' + 47 * '\0'
        # Reference time (in seconds since 1900-01-01 00:00:00)
        REF_TIME_1970 = 2208988800 # DROP L! All integers in Python 3 are long #2208988800L # 1970-01-01 00:00:00
        # connect to the NTP server
        #print('DBG: Connecting to "' + str(address) + '"...')
        client = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        client.sendto(msg.encode(), address)
        msg, address = client.recvfrom(buf)
        if msg:
            # unpacking the result
            t = struct.unpack("!12I", msg)[10]
            t -= REF_TIME_1970
            theTime = time.ctime(t).replace("  ", " ") # in case it'd be needed
            synchronization_time = datetime.datetime.now(self.device_timezone).timestamp()
            logging.info("Synchronized time just now.")
            self.lastTimeSyncTimestamp = synchronization_time
            return synchronization_time
        else:
            return -1 # Unexpected situation, kept as it for now

    def check_for_updates(self, event):
        """ Check for updates with 1st party server(s) and apply them if needed """
        time_now = datetime.datetime.now(self.device_timezone).timestamp()
        first_update_flag = False
        if self.lastUpdatesCheckTimestamp == None:
            first_update_flag = True
        else:
            timeSinceLastUpdateCheckInSeconds = time_now - self.lastUpdatesCheckTimestamp
        freqOfUpdatesCheck = self.deviceInfo.connTypeToFreq[cdefs.ConnectionTypes.CHECK_FOR_UPDATES]
        lenOfTheFreqPeriodInSeconds = SimulatedIoTdevice.get_len_of_freq_period(freqOfUpdatesCheck)
        typicalNoOfDaysBetweenPublishingUpdates = cdefs.UPDATE_MIN_FREQUENCIES_IN_DAYS[self.deviceInfo.devType]
        # introduce variability for more real-life simulation
        typicalNoOfDaysBetweenPublishingUpdates = typicalNoOfDaysBetweenPublishingUpdates * random.uniform(1, 1.15)
        # Check if we need to simulate the `check for updates` at this point
        if first_update_flag or timeSinceLastUpdateCheckInSeconds >= lenOfTheFreqPeriodInSeconds:
            # So we need to simulate the time sync connection now
            # For now, we assume it's always the first 1st party server we contact for updates
            updateHostURL = self.deviceInfo.primary1stPartyServers[0]
            # If there was no check for updates yet, just do it (and do not simulate that there's an actual update to be downloaded
            if first_update_flag:
                # consider evaluating the result returned by the method (boolean for success), for not the method will only log the exception inside and return False then
                SimulatedIoTdevice.send_and_request_phony_file(self,updateHostURL,cdefs.ESTIMATED_SIZE_OF_UPDATE_REQUEST_IN_KBS,cdefs.ESTIMATED_SIZE_OF_NO_UPDATE_RESPONSE_IN_KBS)
                time_now = datetime.datetime.now(self.device_timezone).timestamp()
                self.lastUpdatesCheckTimestamp = time_now
                logging.info("Checked for updates just now.")
                return time_now # Yeah, both set the instance variable AND return its value
            # Let's start by figuring out whether we need to simulate a connection that
            # will simulate downloading a firmware update
            mins, sec = divmod(timeSinceLastUpdateCheckInSeconds, 60)
            hours, mins = divmod(mins, 60)
            timeSinceLastUpdateDownloadInDays, hours = divmod(hours, 24)
            if timeSinceLastUpdateDownloadInDays >= typicalNoOfDaysBetweenPublishingUpdates:
                # Time to download a phony update file now
                maxFirmwareSizeInMB = self.deviceInfo.maxFirmwareUpdateSizeInMB
                resultStatus = SimulatedIoTdevice.send_and_request_phony_file(self,updateHostURL,cdefs.ESTIMATED_SIZE_OF_UPDATE_REQUEST_IN_KBS,maxFirmwareSizeInMB)
                if resultStatus:
                    self.lastUpdatesCheckTimestamp = datetime.datetime.now(self.device_timezone).timestamp()
                    logging.info("Checked for updates and downloaded one just now.")
                else:
                    logging.info("Error checking for updates and downloading one now :(")
                    return -1 # Error we don't expect that much unless server down, keeping return value -1 for now
            else:
                # Simulate just a small connection ending in 'no new updates now'
                # consider evaluating the result returned by the method (boolean for success), for not the method will only log the exception inside and return False then
                SimulatedIoTdevice.send_and_request_phony_file(self,updateHostURL,cdefs.ESTIMATED_SIZE_OF_UPDATE_REQUEST_IN_KBS,cdefs.ESTIMATED_SIZE_OF_NO_UPDATE_RESPONSE_IN_KBS)
                time_now = datetime.datetime.now(self.device_timezone).timestamp()
                self.lastUpdatesCheckTimestamp = time_now
                self.lastUpdatesDownloadTimestamp == time_now
                logging.info("Checked for updates just now.")
                return time_now

    def connect_to_1st_party(self, event):
        """ Make a simulated connection to the 1st party API (happens on boot and then on user requests) """
        # Add a small delay to make it more 'realistic'
        randomConnectionDelay = random.uniform(0.1, 0.95)
        time.sleep(randomConnectionDelay)
        firstPartyURL = cdefs.ENTITY_TO_URL[cdefs.ContactedEntities.FIRST_PARTY][0] # Taking the first one from the list for now
        randomTrafficVolumeVariation = random.uniform(0.95, 1.05)
        noOfKBsToSend = cdefs.DEV_TYPE_TO_MAX_SIZE_SENT_DATA_KB[self.deviceInfo.devType] * randomTrafficVolumeVariation
        noOfKBsToReceive = cdefs.DEV_TYPE_TO_MAX_SIZE_RECVD_DATA_KB[self.deviceInfo.devType] * randomTrafficVolumeVariation
        reqestStatus = SimulatedIoTdevice.send_and_request_phony_file(self, firstPartyURL, noOfKBsToSend, noOfKBsToReceive)
        if reqestStatus: # all succeeded
            self.last1stPartyConnTimestamp = datetime.datetime.now(self.device_timezone).timestamp()
            logging.info("Successfully connected to 1st party now.")
        else:
            logging.info("Error connecting to 1st party here! :(")

    def connect_to_support_party_CDN(self, event):
        """ Simply simulate connection to a support party / CDN here """
        logging.info("Connecting to the support party (entering)")
        # Add a random short delay to simulate real device's operations (?)
        randomConnectionDelay = random.uniform(0.1, 0.45)
        time.sleep(randomConnectionDelay)
        '''
        # If needed before deploying the support party server - a STUB
        self.lastSupportPartyConnTimestamp = datetime.datetime.now(self.device_timezone).timestamp()
        logging.info("STUB: successfully connected to support party / CDN now.")
        return True
        '''
        # Execute the connection
        supportPartyURL = cdefs.ENTITY_TO_URL[cdefs.ContactedEntities.SUPPORT_PARTY][0] # Taking the first one from the list for now
        noOfKBsToSend, noOfKBsToReceive = cdefs.SUPPORT_PARTY_CONN_PACKET_SIZE_IN_KBS[self.deviceInfo.devType]
        reqestStatus = SimulatedIoTdevice.send_and_request_phony_file(self, supportPartyURL, noOfKBsToSend, noOfKBsToReceive)
        if reqestStatus: # all succeeded
            logging.info("Successfully connected to support party / CDN now.")
            self.lastSupportPartyConnTimestamp = datetime.datetime.now(self.device_timezone).timestamp()
        else:
            logging.info("Error connecting to the support party / CDN here :(")

    def connect_to_3rd_party(self, event):
        """ Simply simulate connection to a 3rd party server / IP """
        logging.info("Connecting to the 3rd party (entering)")
        # Add a random short delay to simulate real device's operations (?)
        randomConnectionDelay = random.uniform(0.1, 0.45)
        time.sleep(randomConnectionDelay)
        # Until the 3rd party server will be deployed - a STUB
        self.last3rdPartyConnTimestamp = datetime.datetime.now(self.device_timezone).timestamp()
        logging.info("STUB: successfully connected to support party / CDN now.")
        return True
        # TODO! UNCOMMENT THE REST ONCE THE 3RD PARTY BACKEND WILL BE UP AND RUNNING
        # Execute the connection
        thirdPartyURL = cdefs.ENTITY_TO_URL[cdefs.ContactedEntities.THIRD_PARTY][0] # Taking the first one from the list for now
        noOfKBsToSend, noOfKBsToReceive = cdefs.THIRD_PARTY_CONN_PACKET_SIZE_IN_KBS[self.deviceInfo.devType]
        reqestStatus = SimulatedIoTdevice.send_and_request_phony_file(self, thirdPartyURL, noOfKBsToSend, noOfKBsToReceive)
        if reqestStatus: # all succeeded
            logging.info("Successfully connected to 3rd party now.")
            self.last3rdPartyConnTimestamp = datetime.datetime.now(self.device_timezone).timestamp()
        else:
            logging.info("Error connecting to 3rd party here! :( Not failing silently now...")

    ##### UTILITY METHODS - NOT callbacks! (thus shouldn't received the extra ', event' parameter due to adding 'send_event=True' in Machine definition?)

    def send_and_request_phony_file(self,URL,noOfKBsToSend,noOfKBsToReceive):
        """
        Send and request a file of size specified in parameters to a specified URL.
        The goal is to emulate a connection between the device and remote server.
        """
        # introduce some random variation to make it more real-life
        noOfKBsToSend = noOfKBsToSend * random.uniform(0.85, 1.15)
        noOfKBsToReceive = noOfKBsToReceive * random.uniform(0.85, 1.15)
        noOfKBsToReceive = round(noOfKBsToReceive, 2) # keep only 2 decimals, no real need for higher precision
        files = {'file': SimulatedIoTdevice.make_a_phony_file(noOfKBsToSend)}
        URL = URL + '/' + str(noOfKBsToReceive)
        try:
            #logger.info('Sending the random file of size %s KB...', str(noOfKBsToSend))
            #response = requests.post(URL, files=files, json=requestDetails)
            response = requests.post(URL, files=files) # https://2.python-requests.org/en/v2.3.0/user/quickstart/
            #logger.info('Received a response of size %s KB', len(response.content)/1024)
        except requests.exceptions.Timeout:
            logging.info("[send_and_request_phony_file] EXCEPTION Request to URL '" + str(URL) +"' timeouted! Returning False...")
            return False
        except requests.exceptions.TooManyRedirects:
            logging.info("[send_and_request_phony_file] EXCEPTION Request to URL '" + str(URL) +"' got a response of 'Too Many Redirects'! This most probably means that the URL is wrong. Returning False...")
            return False
        except requests.exceptions.RequestException as e: # https://stackoverflow.com/questions/16511337/correct-way-to-try-except-using-python-requests-module
            logging.info("[send_and_request_phony_file] EXCEPTION Request to URL '" + str(URL) + "' yielded the 'requests.exceptions.RequestException' exception, details follow:\n" + str(e) + "\nReturning False...")
            return False
        finally:
            pass
        return True

    def make_a_phony_file(desiredSizeInKBytes):
        """ Create a binary file of specified size composed of random data. """
        #print('Entering SimulatedIoTdevice.make_a_phony_file')
        desiredSize = desiredSizeInKBytes * 1024
        data = "d" * (int(desiredSize / 2) - 1)

        arr = bytearray(data, 'utf-16')
        return arr

    def get_time_boundaries_per_freq(constantDefiningFrequency):
        """
        Given a constant defining frequency, return a tuple with
        (minTimeToElapse, maxTimeToElapse) that describes amount
        of time that needs to elapse (min,max) in order to execute
        an action/connection with the frequency given as parameter.
        """
        #print('Entering SimulatedIoTdevice.get_time_boundaries_per_freq')
        return cdefs.CONNECTION_FREQUENCIES[constantDefiningFrequency]

    def get_len_of_freq_period(constantDefiningFrequency):
        """
        Given a constant defining frequency, return the length of the
        time range considered to belong to frequency given as parameter.
        The length of the time range is expressed in seconds.
        """
        #print('Entering SimulatedIoTdevice.get_len_of_freq_period')
        return cdefs.CONN_FREQ_PERIODS_LEN_IN_S[constantDefiningFrequency]

    def print_the_caller_details_from_stack():
        # After: https://www.stefaanlippens.net/python_inspect/
        import inspect
        return str(inspect.stack()[2][3])


    def execute_before_each_state_change(self, event): # use EventData
        """ Insert into the list of dictionaries 'self.FSMstateChanges_listOfDicts'
            following information about the transition that will start after this method is done:

            timestamp - microsecond precision
            startingOrFinished - starting transition (1) or just done with it (0)?
            fromState - source state name
            toState - destination state name
            transitionName - self-explanatory
            [OPTIONAL] listOfConditionsMet - if any
            [OPTIONAL] listOfConditionsNotMet - if any
        """
        if self.device_timezone is None:
            logging.error("[execute_before_each_state_change] ERROR in setting timezone-awareness for FSM status log!\nself.device_timezone is None!\nReturning...")
            return
        else:
            timestamp_now = datetime.datetime.now(self.device_timezone).strftime('%Y-%m-%d %H:%M:%S.%f') # Timezone aware
            startingOrFinished = self.FSMstateChange_transitionStarting
            # This was always set to False in tests so far, leaving out for readability of the logs then ("event.result: " + str(event.result) + )
            #logString = "event.event.name: " + str(event.event.name) + " | event.transition.source: " + str(event.transition.source) + " | dest: " + str(event.transition.dest)
            ###### If interested, see a discussion/example of many in the Python community recommend a strategy of "easier to ask for forgiveness than permission" (EAFP) rather than "look before you leap" (LBYL) here: https://stackoverflow.com/questions/610883/how-to-know-if-an-object-has-an-attribute-in-python, here simply going with hasattr
            conditionsMet = None
            conditionsNotMet = None
            if hasattr(event.transition, 'conditions'):
                # This check might be redundant here, no time to play with that though
                if event.transition.conditions is not None: # There are conditions assigned to this transition at all, since the parameter/conditions are optional
                    #logString +=  " | conditions: " + str(event.transition.conditions)
                    conditionsMet = []
                    for conditionObj in event.transition.conditions:
                        conditionsMet.append(str ( conditionObj.func ))
            if hasattr(event.transition, 'unless'):
                if event.transition.unless is not None:
                    #logString += " | unless: " + str(event.transition.unless)
                    conditionsNotMet = []
                    for conditionObj in event.transition.unless:
                        conditionsNotMet.append(str ( conditionObj.func ))
            #logging.info(logString)
            # Instead of logging now insert it into our local data structure (https://stackoverflow.com/questions/1038160/data-structure-for-maintaining-tabular-data-in-memory)
            SimulatedIoTdevice.add_state_change_dict_entry(self, timestamp_now, startingOrFinished, event.transition.source, event.transition.dest, event.event.name, conditionsMet, conditionsNotMet)

    def execute_after_each_state_change(self, event):
        """
            Insert into the list of dictionaries 'self.FSMstateChanges_listOfDicts'
            following information about the transition that has just ended:

            timestamp - microsecond precision
            startingOrFinished - starting transition (1) or just done with it (0)?
            fromState - source state
            toState - destination state
            transitionName - self-explanatory
            [OPTIONAL] listOfConditionsMet - if any
            [OPTIONAL] listOfConditionsNotMet - if any
        """
        if self.device_timezone is None: # Making sure the timezone-awareness is not failing silently! on Mon Apr 27, 20:40
            logging.error("[execute_after_each_state_change] ERROR in setting timezone-awareness for FSM status log!\nself.device_timezone is None!\nReturning...")
            return
        else:
            timestamp_now = datetime.datetime.now(self.device_timezone).strftime('%Y-%m-%d %H:%M:%S.%f') # Made it timezone-aware on Thu Apr 23, 19:48
            startingOrFinished = self.FSMstateChange_transitionFinished
            # This was always set to False in tests so far, leaving out for readability of the logs then ("event.result: " + str(event.result) + )
            #logString = "event.event.name: " + str(event.event.name) + " | event.transition.source: " + str(event.transition.source) + " | dest: " + str(event.transition.dest)
            conditionsMet = None
            conditionsNotMet = None
            if hasattr(event.transition, 'conditions'):
                if event.transition.conditions is not None: # There are conditions assigned to this transition at all, since the parameter/conditions are optional
                    #logString +=  " | conditions: " + str(event.transition.conditions)
                    conditionsMet = []
                    for conditionObj in event.transition.conditions:
                        # From 'https://github.com/pytransitions/transitions/blob/master/transitions/core.py': 'func (callable): The function to call for the condition check'
                        conditionsMet.append(str ( conditionObj.func ))
            if hasattr(event.transition, 'unless'):
                # This check might be redundant here, no time to play with that though
                if event.transition.unless is not None:
                    #logString += " | unless: " + str(event.transition.unless)
                    conditionsNotMet = []
                    for conditionObj in event.transition.unless:
                        conditionsNotMet.append(str ( conditionObj.func ))
            #logging.info(logString)
            SimulatedIoTdevice.add_state_change_dict_entry(self, timestamp_now, startingOrFinished, event.transition.source, event.transition.dest, event.event.name, conditionsMet, conditionsNotMet)

    def add_state_change_dict_entry(self, timestamp, startingOrFinished, fromState, toState, transitionName, listOfConditionsMet = None, listOfConditionsNotMet = None):
        """ Insert a state change describing entry into the self.FSMstateChanges_listOfDicts """
        temp_dict = {}
        # Should be the first value in the dictionary so that it doesn't get cut by the size of packet payload
        #temp_dict[self.FSMstateChangeDictKey_simDevUniqueIDforGTpayload] = self.ts_us_uid # DECIDED it's out of scope
        temp_dict[self.FSMstateChangeDictKey_timestamp] = str( timestamp )
        temp_dict[self.FSMstateChangeDictKey_transitionStartingOrDone] = str( startingOrFinished )
        temp_dict[self.FSMstateChangeDictKey_transitionName] = str( transitionName )
        temp_dict[self.FSMstateChangeDictKey_fromState] = str( fromState )
        temp_dict[self.FSMstateChangeDictKey_toState] = str( toState )
        temp_dict[self.FSMstateChangeDictKey_listOfConditionsMet] = str( listOfConditionsMet )
        temp_dict[self.FSMstateChangeDictKey_listOfConditionsNotMet] = str( listOfConditionsNotMet )
        temp_dict[self.FSMstateChangeDictKey_recordSequenceNumber] = str(self.FSMstateChange_recordSequenceNumber)
        if self.FSMstateChange_recordSequenceNumber < sys.maxsize:
            self.FSMstateChange_recordSequenceNumber += 1
        else:
            self.FSMstateChange_recordSequenceNumber = 0
        # Now compute the hash of the dict without the hash itself
        ## 1. Make sure the dictionary is considered in the same way (sorted keys order, immutable) on both sides of checksum computation
        #tempDictDataFrozenString = str( tuple( sorted( temp_dict.items() ) ) )# Trying to not encode it as bytes object here to make it the same as on the receiving end of computing the hash, i.e. at the Interceptor which runs Python2 # .encode('utf-8')
        tupleOfSortedDictItems = tuple( sorted( temp_dict.items() ) )
        tempDictDataFrozenString = str( tupleOfSortedDictItems ).encode('utf-8')
        LIST_OF_SORTED_DICT_ITEMS = sorted( temp_dict.items() )
        tempSTRING = ""
        for key, value in LIST_OF_SORTED_DICT_ITEMS:
            tempSTRING += str(key) + str(value)
        tempDictDataFrozenString = tempSTRING.encode('utf-8')
        ## 2. Computer the hash value
        tempDictHashValue = hashlib.sha512(tempDictDataFrozenString).hexdigest()
        ## 3. Add the hash value to the dictionary itself
        temp_dict[self.FSMstateChange_sha512DictKey] = tempDictHashValue
        #logging.info("tempDictDataFrozenString: " + str(tempDictDataFrozenString) + "\ntemp_dict: " + str(temp_dict))
        self.FSMstateChanges_listOfDicts.append(temp_dict)

    def upload_FSM_status_update(self):
        """ Send data collected in 'self.FSMstateChanges_listOfDicts' to the Inspector backend
            (triggered by a timer every 'self.FSMstateChange_uploadIntervalInSeconds' seconds)
        """
        # 0. Check if there's anything to be sent - are there any transitions logged on the list?
        if len(self.FSMstateChanges_listOfDicts) == 0:
            logging.info("No ground truth data to upload, done then") # Skipping after confirmed it works not to clutter the logs of actual importance
            return True # Successfully returning, just didn't have anything to do
        # 1. Take a snapshot of the list in order to be able to remove uploaded items from it once received a read receipt from the backend
        listOfDictionariesToBeUploaded = self.FSMstateChanges_listOfDicts # "Freezing" the values we upload and remove afterwards in this local copy of it
        # 2. Prepare the data to be sent
        #post_data = json.dumps(listOfDictionariesToBeUploaded)
        post_data = listOfDictionariesToBeUploaded # Got a hint of "double-encoding" here
        #post_data = json.dumps(listOfDictionariesToBeUploaded, ensure_ascii=False).encode('utf8') # GIVES TypeError: Object of type bytes is not JSON serializable !
        #post_data = str( listOfDictionariesToBeUploaded ) # it's a list of dictionaries, so simply make a string out of it and it's enough
        gt_submission_url = cdefs.GROUND_TRUTH_SUBMISSION_URL
        ###### TESTING whether inspector can intercept this packet by allowing it to get addressed to a (non-existent) HTTP version of the URL
        gt_submission_url = gt_submission_url.replace('https', 'http')
        #gt_submission_url += ':80'
        # 3. Try uploading the ground truth data
        upload_sucessful = False
        ## Try 5 times in a row with increasing delay between attempts (in case, for instance, the backend is rebooting or anything else on the way)
        for attemptNo in range(5):
            status_text = 'Uploading ground truth data to the backend...\n'
            if attemptNo > 0:
                status_text += ' (Attempt {} of 5)'.format(attemptNo + 1)
            logging.info('[UPLOAD of ground truth data] ' + status_text)
            # Upload data via POST request
            #response = requests.post(gt_submission_url, data=post_data).text ## Original inspector's line
            custom_post_headers = { self.customGroundTruthHeaderKey: self.customGroundTruthHeaderValue }
            #custom_post_headers = { self.customGroundTruthHeaderKey: self.customGroundTruthHeaderValue, 'Content-type': 'application/json', 'Accept': 'text/plain' }
            #custom_post_headers = { self.customGroundTruthHeaderKey: self.customGroundTruthHeaderValue, 'Content-Type': 'text/plain' }
            #response = requests.post(gt_submission_url, data=post_data, headers=custom_post_headers).text # The actual line that should be used
            response = requests.post(gt_submission_url, json=post_data, headers=custom_post_headers).text # Trying 'data' parameter instead of 'json' for interception (?)
            #response = requests.post(gt_submission_url, json={"hell": "yeah"}, headers=custom_post_headers).text
            #response = requests.get(gt_submission_url, params=post_data, headers=custom_post_headers).text # Trying GET, GET isn't the answer... parameters trigger (URI too long error)
            '''
            # Local testing - printouts/logging instead of actually sending the response
            logging.info("TEST")
            logging.info("Connecting to URL: '" + str(gt_submission_url) + "'")
            logging.info("Sending this JSON: " + str(post_data))
            response = '{"gt_reception_status": "received_and_saved"}'
            '''
            logging.info('[UPLOAD of ground truth data] Got back server response: ' + response)

            # Update whitelist
            try:
                '''
                # Just debug logging
                logging.info("Log of the sent data followed by received response")
                logging.info(post_data)
                utils.log(response)
                '''
                response_dict = json.loads(response)
                if response_dict['gt_reception_status'] == 'STUB_REQUEST_received_and_saved':
                    upload_sucessful = True
                    break

            except Exception:
                #logging.info('[UPLOAD of ground truth data] Failed. Retrying:', traceback.format_exc())
                logging.info('[UPLOAD of ground truth data] Failed. Retrying: ' + str(sys.exc_info()[0]))
            time.sleep((attemptNo + 1) ** 2)

        if not upload_sucessful:
            # so if it didn't succeed trying 5 times already
            return False

        # 4. Remove successfully uploaded data (listOfDictionariesToBeUploaded) from the constantly updated list (self.FSMstateChanges_listOfDicts)
        #logging.info("DONE uploading GT, cleaning the list now...")
        numberOfElementsToRemove = len(listOfDictionariesToBeUploaded)
        try:
            #logging.info("before deletion: " + str(self.FSMstateChanges_listOfDicts))
            del self.FSMstateChanges_listOfDicts[:numberOfElementsToRemove]
            #logging.info("after deletion: " + str(self.FSMstateChanges_listOfDicts))
        except IndexError:
            logging.info("Indexing error while removing uploaded records of Ground truth to the backend! Returning False from upload_FSM_status_update...")
            return False
        return True
