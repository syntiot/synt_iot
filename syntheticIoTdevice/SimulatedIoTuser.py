import random
import time
import math
import pytz # python's world timezone definitions
import logging

class SimulatedIoTuser(object):

    def __init__(self,deviceType):
        # list of datetimes of interactions with the device, constructed in a chronological order
        self.allInteractionsWithTheDeviceDTs = []
        self.deviceType = deviceType
        self.orderNumberOfPreviousInteractionTimeSlice = -1
        self.orderNumberOfPreviousTossTimeSubSlice = -1
        self.lastEnoughInteractionsRetDT = None
        self.lastNoInteractionInThisPeriodDT = None
        self.lastConsideringInteractionDT = None
        self.lastWeHadInteractionInThisSliceAlreadyDT = None
        self.lastToinCossLostDT = None
        self.lastStillIn1stSliceDebugLogDT = None
        self.lastDebugLogOfAlreadyTossedACoinInThisSubSliceDT = None
        self.debugLoggingIntervalInSeconds = 10
        self.debugLoggingIntervalInSeconds_SHORT = 5

    def isItTheTimeForInteraction(self,deviceType):
        import datetime
        import cons_defs as cdefs
        amsTZ = pytz.timezone('Europe/Amsterdam') # Random, anonimized for submission
        # MAKE SURE ALL TIMES ARE TIMEZONE AWARE !
        localDateTimeNow = datetime.datetime.now(amsTZ)
        if localDateTimeNow == None:
            logging.info("[isItTheTimeForInteraction] localDateTimeNow is None! returning...")
            logging.error("[isItTheTimeForInteraction] ERROR! localDateTimeNow is None! returning...")
            return False
        localNowTime = localDateTimeNow.time()
        TZawareNowDateTime = datetime.datetime.now(amsTZ)
        TZawareSTARTdatetime = None
        TZawareENDdatetime = None
        currentUsePeriodStartTimeHM = None
        currentUsePeriodEndTimeHM = None
        currentUsePeriodTimeOfTheDay = None
        numberOfInteractionsExecutedInCurrentUseTimePeriod = 0 # How many times did this simulated user interact with the simulated device?
        datetimeOfLastInteractionOfThisUsePeriod = None
        currentUseTimePeriodTimeDelta = None
        # This has to be declared here since it's assigned in an inner scope (16 characters/spaces) but later used in this indentation scope (8 characters/spaces)
        currentPeriodStartdt_tzaware = None
        #
        # Establish which time period of interaction with the device is it at the moment and how many interactions in this period have already occurred
        for timeOfTheDay in cdefs.TimesOfTheDay:
            startTimeHM_string, endTimeHM_string = cdefs.DEVICE_USE_PERIOD_HOURS_RANGE[timeOfTheDay]
            # For the sake of `map(int, timestring...`, making sure there'll always be decimal part
            if "." not in startTimeHM_string:
                startTimeHM_string += ".0"
            if "." not in endTimeHM_string:
                endTimeHM_string += ".0"
            startTimeHM_hours, startTimeHM_minutes = map(int, startTimeHM_string.split(".", 1)) # "1" is an optional parameter 'maxsplit' (how many splits should be done)
            endTimeHM_hours, endTimeHM_minutes = map(int, endTimeHM_string.split(".", 1))
            startTimeHM = datetime.time(startTimeHM_hours, startTimeHM_minutes)
            endTimeHM = datetime.time(endTimeHM_hours, endTimeHM_minutes)
            thatsCurrentPeriod = False
            midnightIncluded = False
            if startTimeHM <= endTimeHM:
                # There's no over-midnight transition
                thatsCurrentPeriod = ( startTimeHM <= localNowTime < endTimeHM ) # end time is not included
            else:
                beforeMidnightWithinRange = ( startTimeHM <= localNowTime )
                afterMidnightWithinRange = ( localNowTime < endTimeHM )
                thatsCurrentPeriod = (beforeMidnightWithinRange or afterMidnightWithinRange)
                midnightIncluded = True
            # Establish how many interactions occurred in the current time period already
            startPeriodDate = None
            endPeriodDate = None
            if thatsCurrentPeriod:
                # Set the time boundaries of the current use period
                currentUsePeriodStartTimeHM = startTimeHM
                currentUsePeriodEndTimeHM = endTimeHM
                # Set the time of the day of the current use period
                currentUsePeriodTimeOfTheDay = timeOfTheDay
                # Is the beginning of the time period today? Taking into account the fact we have the correct
                # start hour and end hour of the current use period
                if midnightIncluded:
                    # the startTime will be of 'higher value' than the 'end time' (numerically)
                    # and, most of all, will be in another day than the other
                    if localDateTimeNow.time() < currentUsePeriodStartTimeHM:
                        # We're in the day of end time, after midnight, thus today is the RHS boundary date
                        # and we need to subtract 1 day in order to get the start date's day (for date)
                        startPeriodDate = localDateTimeNow.date() - datetime.timedelta(days=1)
                    else:
                        # Otherwise, today is the day of the correct start date of the time period?
                        startPeriodDate = localDateTimeNow.date()
                #else: # kept for transparency while reading this code later
                else:
                    startPeriodDate = localDateTimeNow.date()
                # This date will be the same regardless of whethere there's an over-midnight period or not
                endPeriodDate = localDateTimeNow.date()
                
                currentPeriodStartDT = datetime.datetime.combine(startPeriodDate, currentUsePeriodStartTimeHM)
                currentPeriodEndDT = datetime.datetime.combine(endPeriodDate, currentUsePeriodEndTimeHM)

                currentPeriodStartdt_tzaware = amsTZ.localize( currentPeriodStartDT )
                currentPeriodEnddt_tzaware = amsTZ.localize( currentPeriodEndDT )
                currentUseTimePeriodTimeDelta = currentPeriodEnddt_tzaware - currentPeriodStartdt_tzaware
                # Check how many interactions were already executed during the current use period
                if self.allInteractionsWithTheDeviceDTs == []:
                    break # No interactions of the current user with the device were recorded at all
                for idx, dt in reversed(list(enumerate(self.allInteractionsWithTheDeviceDTs))):
                    # Start from the most recent one and continue until the end of the time period is reached
                    # Test if the dt is within the use time period
                    dt_tzaware = dt
                    isWithinCurrentUseTimePeriod = ( currentPeriodStartdt_tzaware <= dt_tzaware <= currentPeriodEnddt_tzaware )
                    if isWithinCurrentUseTimePeriod:
                        numberOfInteractionsExecutedInCurrentUseTimePeriod += 1
                        if datetimeOfLastInteractionOfThisUsePeriod == None:
                            datetimeOfLastInteractionOfThisUsePeriod = dt # we only do it once, as we browse the list from the end and there can be only one last / more recent use
                    ###else:
                    ###    # we reached the first interaction outside of the current use period,
                    ###    # so there is no use/point to continue browsing the `self.allInteractionsWithTheDeviceDTs`
                break # No point / need to iterature further since we've already found the current day and time
        # Establish the weekday of the current use period
        currentUsePeriodDayOfWeek = None
        for dayOfWeekEnumItem in cdefs.Weekdays:
            if dayOfWeekEnumItem.value == localDateTimeNow.weekday():
                currentUsePeriodDayOfWeek = dayOfWeekEnumItem
                break
        # Establish how many interactions are expected in this time frame
        expectedNoOfInteractions = SimulatedIoTuser.get_expected_no_of_interactions_with_device(deviceType, (currentUsePeriodStartTimeHM, currentUsePeriodEndTimeHM), currentUsePeriodDayOfWeek)
        # Is there a need for an interaction now?
        if numberOfInteractionsExecutedInCurrentUseTimePeriod >= expectedNoOfInteractions:
            if self.lastEnoughInteractionsRetDT == None:
                pass # Just to prevent/safeguard from error below; it should not happen though due to the above condition anyway though
            else:
                if datetime.datetime.now(amsTZ) - self.lastEnoughInteractionsRetDT > self.debugLoggingIntervalInSeconds:
                    ######logging.info("There was enough interactions within the current time period, skipping...")
                    self.lastEnoughInteractionsRetDT = datetime.datetime.now(amsTZ)
            return False # done with the interactions already, no need for more
        ## When was the last interaction? # -> it's kept in the variable `datetimeOfLastInteractionOfThisUsePeriod`
        ## Should we trigger one now? (it probably shouldn't happen right after the previous one, or should it? Randomize for now)
        # Compute the length of the interaction period and then divide the period into x time slices. Consider simulating
        # an interaction within each of these time slices only (to allow pauses in between)
        noOfTimeSlicesPerUsePeriod = cdefs.NO_OF_TIME_SLICES_PER_USE_PERIOD[currentUsePeriodTimeOfTheDay]
        noOfTimeSubSlices = cdefs.NO_OF_SUB_SLICES_PER_TIME_PERIOD
        currentUseTimePeriodLenInSeconds = currentUseTimePeriodTimeDelta.total_seconds()
        sliceLenInSeconds = currentUseTimePeriodLenInSeconds / noOfTimeSlicesPerUsePeriod
        subSliceLenInSeconds = sliceLenInSeconds / noOfTimeSubSlices
        # Which slice are we in? -> see else clause
        if datetimeOfLastInteractionOfThisUsePeriod == None:
            # There was no interactions between the simulated user and the simulated device yet
            tdelta_sinceLastInteraction = datetime.timedelta() # After documentation: all arguments are optional and default to 0
        else:
            tdelta_sinceLastInteraction = datetime.datetime.now(amsTZ) - datetimeOfLastInteractionOfThisUsePeriod
        tdelta_sinceLastInteraction_inSeconds = tdelta_sinceLastInteraction.total_seconds()
        
        tdelta_sinceBeginningOfCurrentTimePeriod = datetime.datetime.now(amsTZ) - currentPeriodStartdt_tzaware
        tdelta_sinceBeginningOfCurrentTimePeriod_inSeconds = tdelta_sinceBeginningOfCurrentTimePeriod.total_seconds()
        numberOfCurrentTimeSlice = int( math.ceil(tdelta_sinceBeginningOfCurrentTimePeriod_inSeconds / sliceLenInSeconds) )
        
        startOfTheCurrentTimeSlice = currentPeriodStartdt_tzaware + datetime.timedelta( seconds = sliceLenInSeconds * (numberOfCurrentTimeSlice - 1) )
        endOfTheCurrentTimeSlice = currentPeriodStartdt_tzaware + datetime.timedelta( seconds = sliceLenInSeconds * numberOfCurrentTimeSlice )
        tdelta_sinceBeginningOfCurrenTimeSlice = datetime.datetime.now(amsTZ) - startOfTheCurrentTimeSlice
        tdelta_sinceBeginningOfCurrenTimeSlice_inSeconds = tdelta_sinceBeginningOfCurrenTimeSlice.total_seconds()
        numberOfCurrentTimeSubSlice = int( math.ceil(tdelta_sinceBeginningOfCurrenTimeSlice_inSeconds / noOfTimeSubSlices) )
        # Were there any interactions recently?
        if datetimeOfLastInteractionOfThisUsePeriod == None:
            # We do not want to start simulated interactions right after the bootup period, let's then wait for 'cdefs.NUMBER_OF_TIME_SLICES_TO_WAIT_BEFORE_FIRST_USER_INTERACTION' time slices
            if numberOfCurrentTimeSlice <= cdefs.NUMBER_OF_TIME_SLICES_TO_WAIT_BEFORE_FIRST_USER_INTERACTION:
                if self.lastStillIn1stSliceDebugLogDT == None:
                    self.lastStillIn1stSliceDebugLogDT = datetime.datetime.now(amsTZ)
                numberOfSecondsSinceStillIn1stSliceDebugLog = int( ( datetime.datetime.now(amsTZ) - self.lastStillIn1stSliceDebugLogDT ).total_seconds() )
                if numberOfSecondsSinceStillIn1stSliceDebugLog > self.debugLoggingIntervalInSeconds:
                    ######logging.info("Still in the time slice(s) to be waited up, not considering user interaction here at the moment...")
                    self.lastStillIn1stSliceDebugLogDT = datetime.datetime.now(amsTZ)
                return False
            # there was no interaction with the device in this use period yet, toss a coin to decide whether to make it now
            # BUT FIRST: check whether it's been already done in the current sub-slice of the current time slice
            if numberOfCurrentTimeSubSlice == self.orderNumberOfPreviousTossTimeSubSlice: # this covers the case when 'self.orderNumberOfPreviousTossTimeSubSlice' == -1, i.e. there was not interaction yet at all
                if self.lastDebugLogOfAlreadyTossedACoinInThisSubSliceDT == None:
                    self.lastDebugLogOfAlreadyTossedACoinInThisSubSliceDT = datetime.datetime.now(amsTZ)
                numberOfSecondsSinceLastDebugLogOfAlreadyTosseInThisSubSlice = int( ( datetime.datetime.now(amsTZ) - self.lastDebugLogOfAlreadyTossedACoinInThisSubSliceDT).total_seconds() ) # by using int(...) here, we might cut some very short period test times, beware
                if numberOfSecondsSinceLastDebugLogOfAlreadyTosseInThisSubSlice > self.debugLoggingIntervalInSeconds_SHORT:
                    logging.info("Still in the same subslice #" + str(numberOfCurrentTimeSubSlice) + ", skipping the toss... [no interaction yet, shouldn't get here?]")
                    self.lastDebugLogOfAlreadyTossedACoinInThisSubSliceDT = datetime.datetime.now(amsTZ)
                return False
            # Otherwise, there was no interaction neither in the current time slice nor time subslice and it should be attempted, moving on to tossing the coin then
            if self.lastNoInteractionInThisPeriodDT == None:
                self.lastNoInteractionInThisPeriodDT = datetime.datetime.now(amsTZ)
            numberOfSecondsSinceLastNoInteractionInThisPeriodDebugLog = int( ( datetime.datetime.now(amsTZ) - self.lastNoInteractionInThisPeriodDT ).total_seconds() )
            if numberOfSecondsSinceLastNoInteractionInThisPeriodDebugLog > self.debugLoggingIntervalInSeconds:
                logging.info("No interactions within this period yet, tossing it...")
                self.lastNoInteractionInThisPeriodDT = datetime.datetime.now(amsTZ)
            # Now we can toss the coin
            logging.info("FIRST TOSS of time period: " + str(currentUsePeriodStartTimeHM)
                                 + " - " + str(currentUsePeriodEndTimeHM))
            coinFace = SimulatedIoTuser.toss_a_coin()
            self.orderNumberOfPreviousTossTimeSubSlice = numberOfCurrentTimeSubSlice
            if coinFace == 1:
                # YesOB
                self.orderNumberOfPreviousInteractionTimeSlice = numberOfCurrentTimeSlice
                return True # and thus 'datetimeOfLastInteractionOfThisUsePeriod' won't be None next time
            else:
                # We might be back here, since the 'self.allInteractionsWithTheDeviceDTs' won't get any DTs in 'SimulatedIoTdevice' and thus while empty, no 'datetimeOfLastInteractionOfThisUsePeriod' will be set
                pass
        else:
            if self.lastConsideringInteractionDT == None:
                self.lastConsideringInteractionDT = datetime.datetime.now(amsTZ) #time.time()
            numberOfSecondsSinceLastConsideringInteractionDebugLog = int( (datetime.datetime.now(amsTZ) - self.lastConsideringInteractionDT).total_seconds() )
            if numberOfSecondsSinceLastConsideringInteractionDebugLog > self.debugLoggingIntervalInSeconds:
                # Calculate the boundaries of the current time slice
                self.lastConsideringInteractionDT = datetime.datetime.now(amsTZ) #time.time()
            # Which time slice of the current use period are we in?
            # if the last interaction has been done within this use period, do not consider it during the same timeslice twice
            # i.e. toss the coin only if we're already in the next time slice and the time of lenght corresponding to the
            # slice length has elapsed by this point
            # If there was already an interaction within the current time slice, do not consider another one
            if self.orderNumberOfPreviousInteractionTimeSlice == numberOfCurrentTimeSlice:
                lastInteractionDT = self.allInteractionsWithTheDeviceDTs[len(self.allInteractionsWithTheDeviceDTs)-1]
                if self.lastWeHadInteractionInThisSliceAlreadyDT == None:
                    self.lastWeHadInteractionInThisSliceAlreadyDT = datetime.datetime.now(amsTZ)
                numberOfSecondsSinceLastWeHadInteractionInThisSliceDebugLog = int( (datetime.datetime.now(amsTZ) - self.lastWeHadInteractionInThisSliceAlreadyDT).total_seconds() )
                if numberOfSecondsSinceLastWeHadInteractionInThisSliceDebugLog > self.debugLoggingIntervalInSeconds:
                    ######logging.info("We had an interaction in this time slice (#" + str(numberOfCurrentTimeSlice) + ") already at " + str(lastInteractionDT) + ", skipping...")
                    self.lastWeHadInteractionInThisSliceAlreadyDT = datetime.datetime.now(amsTZ)
                return False
            # How about the subslice?
            if numberOfCurrentTimeSubSlice == self.orderNumberOfPreviousTossTimeSubSlice: # this covers the case when 'self.orderNumberOfPreviousTossTimeSubSlice' == -1, i.e. there was not interaction yet at all
                if self.lastDebugLogOfAlreadyTossedACoinInThisSubSliceDT == None:
                    self.lastDebugLogOfAlreadyTossedACoinInThisSubSliceDT = datetime.datetime.now(amsTZ)
                numberOfSecondsSinceLastDebugLogOfAlreadyTosseInThisSubSlice = int( ( datetime.datetime.now(amsTZ) - self.lastDebugLogOfAlreadyTossedACoinInThisSubSliceDT).total_seconds() ) # by using int(...) here, we might cut some very short period test times, beware
                if numberOfSecondsSinceLastDebugLogOfAlreadyTosseInThisSubSlice > self.debugLoggingIntervalInSeconds:
                    ######logging.info("Still in the same subslice #" + str(numberOfCurrentTimeSubSlice) + ", skipping the toss... [there was an interaction already]")
                    self.lastDebugLogOfAlreadyTossedACoinInThisSubSliceDT = datetime.datetime.now(amsTZ)
                return False

            if numberOfCurrentTimeSlice > cdefs.NUMBER_OF_TIME_SLICES_TO_WAIT_BEFORE_FIRST_USER_INTERACTION:
                # we're in at least 2nd time slice, i.e. we do not begin interactions immediately in the first one, when the boot connections occur
                ## Now, we slice the time slice into sub-slices, in order to space out the coin tosses in time within the time slice (as otherwise,
                ## we've been getting the winning coin face almost immediately)
                
                # toss a coin
                coinFace = SimulatedIoTuser.toss_a_coin()
                self.orderNumberOfPreviousTossTimeSubSlice = numberOfCurrentTimeSubSlice
                if coinFace == 1:
                    logging.info("Won this coin toss, updating 'self.orderNumberOfPreviousInteractionTimeSlice' to " + str(numberOfCurrentTimeSlice))
                    self.orderNumberOfPreviousInteractionTimeSlice = numberOfCurrentTimeSlice
                    return True
                else:
                    logging.info("Lost this coin toss...")
                    return False
        return False # Should be redundant, kept in case some wild and quick changes of code would miss something
        
    def get_expected_no_of_interactions_with_device(deviceType, deviceUsePeriodHoursRange, dayOfTheWeek):
        import math
        import random
        import datetime
        import cons_defs as cdefs
        #logging.info("Establishing the number of expected interaction this simulated user will have with the simulated device")
        amsTZ = pytz.timezone('Europe/Amsterdam')
        localDateTimeNow = datetime.datetime.now(amsTZ)
        usePeriodStartHM, usePeriodEndHM = deviceUsePeriodHoursRange
        usePeriodStartString = str(usePeriodStartHM.hour)
        if usePeriodStartHM.minute != 0:
            usePeriodStartString += '.' + str(usePeriodStartHM.minute)
        usePeriodEndString = str(usePeriodEndHM.hour)
        if usePeriodEndHM.minute != 0:
            usePeriodEndString += '.' + str(usePeriodEndHM.minute)
        minNoOfExpectedInteractions, maxNoOfExpectedInteractions = cdefs.EXPECTED_NO_OF_INTERACTIONS_WITH_DEVICE_MINS[(deviceType,
                                                                      (usePeriodStartString, usePeriodEndString), dayOfTheWeek)]
        avgNoOfInteractionsFloor = math.floor( (minNoOfExpectedInteractions + maxNoOfExpectedInteractions) / 2.0 )
        # no specific rationale attached to this choice of calculating the number of expected calculations for now
        return math.floor( avgNoOfInteractionsFloor * random.uniform(0.25, 1.75) )

    def toss_a_coin():
        """ Return the result of Python's equivalent of tossing a coin """
        tossResult = random.randint(0, 1)
        ######logging.info("Executed toss_a_coin, returning " + str(tossResult))
        return tossResult
        
    
